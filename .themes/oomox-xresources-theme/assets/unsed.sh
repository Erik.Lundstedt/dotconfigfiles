#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#242837/g' \
         -e 's/rgb(100%,100%,100%)/#8099ee/g' \
    -e 's/rgb(50%,0%,0%)/#242837/g' \
     -e 's/rgb(0%,50%,0%)/#7a7ab0/g' \
 -e 's/rgb(0%,50.196078%,0%)/#7a7ab0/g' \
     -e 's/rgb(50%,0%,50%)/#242837/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#242837/g' \
     -e 's/rgb(0%,0%,50%)/#8099ee/g' \
	"$@"
