#!/usr/bin/env fennel
(local erik
	   {
		:name
		(fn [group]
			(.. "Erik Lundstedt " "(" group ")")
		)
		:alias (fn [group]
			["Erik" "erik" "me" group]
		)
		})
(local addr
	   [
		{:name (erik.name "private")	:alias (erik.alias "private")				:mail "erik@lundstedt.it"}
		{:name (erik.name "work")		:alias (erik.alias "work")				:mail "erik.lundstedt@techiteasy.se"}
		{:name "Magnus Eriksson"		:alias ["magnus" "brorfar" "Magnus"]			:mail "magnuskeriksson@yahoo.se"}
		{:name "update (work)"			:alias ["update" "daily" "work"]			:mail "update@techiteasy.se"}
		{:name "Ludwig (work)"			:alias ["update" "daily" "work" "Ludwig"]		:mail "ludwig@retail-data.com"}
		{:name "Kenneth Lundstedt"		:alias ["kenneth" "Kenneth" "farfar" "Farfar"]		:mail "kenneth@lundstedt.it"}
		{:name "Magnus	Lundstedt"		:alias ["magnus" "Magnus" "pappa" "Pappa"]		:mail "kenneth@lundstedt.it"}
		]
	   )
{
 :addr addr
 }
