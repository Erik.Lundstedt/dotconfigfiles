#!/usr/bin/env fennel
;; {:alias "bash"			:path "$HOME/.bashrc"}
;; {:alias "ssh"			:path "$HOME/.ssh/config"}
;; {:alias "sxhkd"			:path "$XDG_CONFIG_DIR/sxhkd/sxhkdrc"}
;; {:alias "xbindkeys"		:path "$XDG_CONFIG_DIR/xbindkeys/xbindkeysrc.scm"}
;; {:alias "vim"			:path "$HOME/.vimrc"}
;; {:alias "xinitrc"		:path "$HOME/.xinitrc"}
;; {:alias "xresources"		:path "$HOME/.Xresources"}
;; {:alias "zsh"			:path "$HOME/.zshrc"}
;; {:alias "luakit"			:path "$XDG_CONFIG_DIR/luakit/userconfig.fnl"}
;; {:alias "picom"			:path "$XDG_CONFIG_DIR/picom/picom.conf"}
;; {:alias "ncmpcpp"		:path "$XDG_CONFIG_DIR/ncmpcpp/config"}
;; {:alias "neovim"			:path "$XDG_CONFIG_DIR/nvim/init.vim"}

{:configFiles
 [{:alias "awesome"		:path "$XDG_CONFIG_DIR/awesome/cfg.fnl"}
  {:alias "emacs"		:path "$XDG_CONFIG_DIR/crafted-emacs/config.el"}
  {:alias "emacs.d"		:path "$XDG_CONFIG_DIR/emacs/init.el"}
  {:alias "tym"			:path "$XDG_CONFIG_DIR/tym/config.fnl"}
  {:alias "xplr"		:path "$XDG_CONFIG_DIR/xplr/config.fnl"}]}
