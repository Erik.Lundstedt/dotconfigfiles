#!/usr/bin/env fennel

(local editors {})

(fn editors.nvim [file]
  "function to execute in order to open the editor"
  (table.concat ["tym" "--title" "tmp" "-e" " nvim" "-l" file ] " "))

(fn editors.emacs [file]
  "function to execute in order to open the editor"
  (table.concat ["emacsclient" "--alternate-editor=emacs" file] " "))

{
 :dmenuArgs []
 :editor editors.emacs
 :editorFn editors.emacs
 }
