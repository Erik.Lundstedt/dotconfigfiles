;;(add-to-list 'load-path (expand-file-name "modules/" user-emacs-directory))
;;(add-to-list 'load-path (expand-file-name "config.el" user-emacs-directory))

(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.config/emacs/")))


(defun load-user-file (file)
  "Load a file in current user's configuration directory,
subfolder modules"
  (interactive "f")
  (require (expand-file-name file (expand-file-name "modules" user-init-dir)))
  )



(require 'packages-conf)
(require 'early-conf)
(load "settings" nil t)


(defvar modules
	'(
	"evil-conf"
	"speedbar-conf"
	"skeleton-conf"
	"rememberall-conf"
	"ide-conf"
	"ivy-conf"
	"modeline-conf"
	"feedreader-conf"
	"dired-conf"
	"org-conf"
	"info-conf"
	"keybind-conf"
	"hydra-conf"
	))


(add-to-list 'modules "fennel-conf" 't)



(defvar jp-modules
  '("jp-icons"))

(defun load-module-list (mods)
  (dolist (mod mods)
	(message "%s" mod)
	;; (message-box "%s" module)
	(require (intern mod))))


(load-module-list jp-modules)
(load-module-list modules)
