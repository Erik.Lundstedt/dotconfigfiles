(defvar el/default-font-size 180)
(defvar el/default-variable-font-size el/default-font-size)
;; Make frame transparency overridable
(defvar el/frame-transparency '(90 . 90))
(defvar fontname "Ubuntu Mono Ligaturized")
;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

(defun el/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
	   (format "%.2f seconds" (float-time (time-subtract after-init-time before-init-time))) gcs-done))
;;(add-hook 'emacs-startup-hook #'el/display-startup-time)
;; Initialize package sources
(require 'package)
(setq package-archives
	'(("melpa" . "https://melpa.org/packages/")
	("org" . "https://orgmode.org/elpa/")
        ("elpa" . "https://elpa.gnu.org/packages/")))
;;(package-initialize)
;;(unless package-archive-contents (package-refresh-contents))

  ;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;;(use-package use-package-hydra  :after use-package :after hydra)

;;(use-package use-package-chords)



;; (use-package auto-package-update
;;   :custom
;;   (auto-package-update-interval 7)
;;   (auto-package-update-prompt-before-update t)
;;   (auto-package-update-hide-results t)
;;   :config
;;   (auto-package-update-maybe)
;;   (auto-package-update-at-time "11:50"))




;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
(use-package no-littering)


(use-package doom-themes
  :init		(load-theme 'doom-palenight t)
  :config	(load-theme 'doom-palenight t))



(defun el/display-line-numbers-relative ()
	(interactive)
	(setq display-line-numbers-type 'relative)
	(which-function-mode)
	;;(if global-display-line-numbers-mode (global-display-line-numbers-mode)  (global-display-line-numbers-mode))
	(display-line-numbers--turn-on))

(defun el/startup-hook-fun ()
  (el/display-line-numbers-relative)
  (message "linenumbers relative")
  ;;(toggle-truncate-lines t)
  ;;(outline-minor-mode t)
  (page-break-lines-mode t)
  (set-default 'truncate-lines t)
  (indent-tabs-mode t)
  ;;(rainbow-delimiters-mode)
  (delete-selection-mode 1)
  )


;; Set frame transparency
(set-frame-parameter (selected-frame) 'alpha el/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,el/frame-transparency))
;;(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
;;(add-to-list 'default-frame-alist '(fullscreen . maximized))
(setq frame-resize-pixelwise t
	  window-combination-resize t)            ; Resize windows proportionally

(set-face-attribute 'default nil :font fontname :height el/default-font-size)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font fontname :height el/default-font-size)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font fontname :height el/default-variable-font-size :weight 'regular)


(setq inhibit-startup-message t)

(scroll-bar-mode -1)				; Disable visible scrollbar
(tool-bar-mode -1)				; Disable the toolbar
(tooltip-mode -1)				; Disable tooltips
(set-fringe-mode 0)				; Give some breathing room


;;(menu-bar-mode -1)            ; Disable the menu bar

;; Set up the visible bell
(setq visible-bell 0)

;;(setq scroll-step 1) ;; keyboard scroll one line at a time

(column-number-mode)
(setq display-line-numbers 'relative)
(customize-set-variable 'display-line-numbers-type 'relative)
(global-display-line-numbers-mode t)
(electric-pair-mode 1)
(show-paren-mode 1)    ; turn on paren match highlighting

(use-package dashboard
  :config
  (dashboard-setup-startup-hook)
  (setq
   dashboard-startup-banner 'official
   ;;dashboard-icon-type 'all-the-icons
   ;;dashboard-set-heading-icons t
   ;;dashboard-set-file-icons t
   dashboard-center-content t))

(setq initial-buffer-choice
      (lambda ()
		(get-buffer-create "*dashboard*"))
	  )

(setq tab-bar-new-tab-choice
      (lambda ()
		(get-buffer-create "*dashboard*"))
	  )


(add-hook 'after-init-hook
	(lambda ()
		(add-to-list 'default-frame-alist '(alpha . 80))
		(el/startup-hook-fun)
		;;(speedbarsetup)
		  (server-start)
		))


;;(add-hook 'speedbar-mode-hook (lambda (display-line-numbers-mode nil)))
;; (defun myIdleTask ()
;;   "Temporary function."
;;   (if (evil-insert-state-p)
;;       (evil-normal-state)))
;; (run-with-idle-timer 2 t 'myIdleTask)


(add-hook 'after-init-hook
		  ;;(newsticker-start)
		  (el/startup-hook-fun))

(add-hook
 'prog-mode-hook
  (lambda ()
    (interactive)
    (setq tab-width 4)
    (hs-minor-mode)
    (el/startup-hook-fun)))

(add-hook 'evil-mode-hook
  (lambda ()
	(interactive)
	(el/startup-hook-fun)))

;;(add-hook 'org-mode-hook  (lambda ()	(interactive)	(el/startup-hook-fun)))


(provide 'settings)
