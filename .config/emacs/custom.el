(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(flycheck-aspell flymake-aspell yasnippet-snippets yasnippet-classic-snippets yasnippet-capf yaml-pro yaml-imenu xr xkcd which-key w3m vundo visual-regexp vimrc-mode use-package-hydra use-package-chords tree-sitter-langs tempel-collection telephone-line tab-jump-out system-packages symbols-outline symbol-overlay srefactor smartscan smartparens smart-jump smart-compile simple-bookmarks scrollable-quick-peek rg rainbow-mode rainbow-identifiers rainbow-delimiters rainbow-blocks project-abbrev pretty-symbols pretty-mode posframe platformio-mode php-quickhelp php-mode php-eldoc paredit-menu pair-tree pager-default-keybindings page-break-lines outline-minor-faces origami org-modern no-littering niceify-info narrowed-page-navigation mpdmacs mpdel-embark move-dup monroe math-symbols mastodon makefile-executor major-mode-hydra magit-todos magit-imerge luarocks lsp-ui logview keepass-mode jabber ivy-prescient ivy-mpdel ivy-hydra ivy-historian insert-shebang info-rename-buffer info-colors inf-clojure indent-bars impostman imenu-extra idle-highlight-in-visible-buffers-mode hl-prog-extra hl-block-mode hideshow-org helpful graphql go-snippets gitignore-snippets git-gutter+ ggtags general fontawesome flyparens flymake-fennel flylisp flycheck-clojure fireplace file-info fennel-mode fancy-dabbrev evil-paredit evil-owl evil-nerd-commenter evil-mc-extras evil-collection evil-args eshell-z eshell-up eshell-info-banner eshell-git-prompt emr emojify emms elfeed-goodies electric-case dumb-jump doom-themes discover dired-single denote declutter dashboard counsel-projectile config-general-mode company-try-hard company-shell company-lua company-go company-box company-arduino clojure-snippets clojure-mode-extra-font-locking casual-suite bool-flip bookmark-view bookmark-in-project bookmark-frecency bluetooth bicycle banner-comment autobookmarks auto-package-update arduino-cli-mode all-the-icons-ivy-rich all-the-icons-dired all achievements @)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
