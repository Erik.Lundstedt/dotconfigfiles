
(use-package xr)
(use-package emr)

(use-package declutter)

(use-package evil-nerd-commenter
  :bind ("M-§" . evilnc-comment-or-uncomment-lines))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))


(use-package rainbow-identifiers
  :hook (prog-mode . rainbow-identifiers-mode))



(use-package emojify)
(use-package discover)
;;(use-package mastodon-discover)
(use-package mastodon
  :config
  (mastodon-discover)
  (setq
    mastodon-instance-url "https://social.vivaldi.net"
    mastodon-active-user "erilun06"))

;; (use-package selectric-mode)
;; (selectric-mode t)

(use-package indent-bars)
(use-package smartscan)
(use-package rg)
(use-package smart-jump)
(setq smart-jump-bind-keys-for-evil t)
;;(smart-jump-register)


  
(use-package electric-case)
(use-package keepass-mode)
(use-package xkcd)

(use-package mpdmacs)




(use-package page-break-lines
  :init (global-page-break-lines-mode t))

(use-package hl-todo
  :init (global-hl-todo-mode))
(use-package magit-todos
  :after magit
  :config (magit-todos-mode 1))


(use-package vundo)






;; (use-package )
;; (use-package )
;; (use-package )
;; (use-package )



(provide 'packages-conf)
