;;(set-face-attribute (car face) nil :font fontname :weight 'regular :height (cdr face)))

(use-package org :custom (org-support-shift-select t))

;;(use-package org-bullets
;;  :hook (org-mode . org-bullets-mode)
;;  :custom
;;  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; (use-package org-superstar
;; ;;  :hook (org-mode . org-superstar-mode)
;;   :custom
;;   (org-superstar-headline-bullets-list '("✯" "✫" "✬" "✪" "❂" "✭" "✮" ))
;;   :config
;;   (setq org-superstar-item-bullet-alist '("❃" "❀" "❁"))
;;   )

(use-package org-modern
  :hook (org-mode . org-modern-mode)
  :custom
  (org-modern-star '("✯" "✫" "✬" "✪" "❂" "✭" "✮" ))
  (org-modern-list '("❃" "❀" "❁"))
  :custom-face
  (org-modern-tag ((t (:inherit (secondary-selection org-modern-label) :width normal))))
  )

(use-package denote
  :after org)

;; (defun el/org-mode-visual-fill ()
;;   (setq visual-fill-column-width 100
;;         visual-fill-column-center-text t)
;;   (visual-fill-column-mode 1))

;;(use-package visual-fill-column :hook (org-mode . el/org-mode-visual-fill))

(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
	 (fennel . t)
	 ))

  (push '("conf-unix" . conf-unix) org-src-lang-modes))






(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("fnl" . "src fennel"))
  )

(provide 'org-conf)
