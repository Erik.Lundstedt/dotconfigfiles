


(use-package tempo)
(use-package tempel)
(use-package tempel-collection :after tempel)


;;; shell
(add-hook
 'sh-mode-hook
 (lambda ()
   (define-skeleton sh-skel-fn
	 "a function in (ba)sh" nil
	 (skeleton-read "name: ") "()" \n
	 > "{" \n
	 > _ - \n
	 > "}" \n
	 )
   (define-abbrev sh-mode-abbrev-table "fn" "" 'sh-skel-fn)
   ))


(define-skeleton elisp-skel-abbrev "shortcut to define a abbreviation with abbrev" nil 
  > "(define-abbrev "
  (skeleton-read "-abbrev-table: " ) _
  "-abbrev-table " 
  "\"" (skeleton-read "abbrev: ") _ "!\" \""
  (skeleton-read "expansion: ") _ "\" "
  "'" (skeleton-read "skeleton fn: ") _ ")" \n > _ -
  )

(define-abbrev emacs-lisp-mode-abbrev-table "_add-abbrev!" "" 'elisp-skel-abbrev)

(define-skeleton elisp-skel-skel "shortcut to define a skeleton" nil
  "(define-skeleton " (skeleton-read "name: ") " \"" (skeleton-read "docstring: " ) "\" nil " \n
  > _
  ")" \n
  > "_add-abbrev!"
  )

;;; fennel
(add-hook 'abbrev-mode-hook
 (lambda ()
   
   (define-skeleton fnl-skel-do "do statement in fennel" nil
	 "(do " > _ - ")")

   (define-skeleton fnl-skel-while "while loop in fennel" nil
	 "(while "
	 \n >  (skeleton-read "logic: " "()")
	 \n > _ - ")" )
   
   (define-skeleton fnl-skel-when "(when (= a b)) in fennel" nil
	 "(when (= " (skeleton-read "a: ") _ (skeleton-read "b: ") _ ")" \n
	 > _ - ")")




   
   (define-skeleton fnl-skel-unless "(when (not= a b)) in fennel" nil
	 "(when (not= " (skeleton-read "a: ") _ (skeleton-read "b: ") _ ")" \n
	 > _ - ")")

   (define-skeleton fnl-skel-local "a local variable/constant in fennel" nil
	 "(local " (skeleton-read "name: ") " " (skeleton-read "value: ") _ - ")")

   (define-skeleton fnl-skel-let "a let expression in fennel" nil
	 "(let [" (skeleton-read "vars: ") "]" \n > _ - ")")

   (define-skeleton fnl-skel-lambda "a lambda in fennel" nil
	 "(lambda " (skeleton-read "name: ") " [" (skeleton-read "args: " ) "]" \n > _ - \n ")")

   (define-skeleton fnl-skel-function "a function in fennel" nil
	 "(fn " (skeleton-read "name: ") " [" (skeleton-read "args: " ) "]" \n > _ - \n ")")

   (define-skeleton fnl-skel-require "require a module in fennel" nil
	 "(require :" (skeleton-read "module: :") _ ")")

   (define-skeleton fnl-skel-req "require a module in fennel" nil
	 "(local " (skeleton-read "name: " ) _ "require! ")

   (define-skeleton fnl-skel-macro "a macro in fennel" nil
	 "(macro " (skeleton-read "name: ") " [" (skeleton-read "args: " ) "]" \n > _ - \n ")")

   (define-skeleton fnl-skel-for "for-loop in fennel" nil
	 "(for [i " (skeleton-read "min: ") " " (skeleton-read "max: ") "]" \n _ - \n ")")

   (define-skeleton fnl-skel-pairs "pair iterator function in fennel" nil
	 "(pairs " (skeleton-read "table: ") ")")

   (define-skeleton fnl-skel-ipairs "numeric pair iterator function in fennel" nil
	 "(ipairs " (skeleton-read "table: ") ")")

   (define-skeleton fnl-skel-each "each-loop in fennel" nil
	 "(each [key value pairs!]" \n _ - ")")

   (define-skeleton fnl-skel-ieach "alternate each-loop in fennel" nil
	 "(each [key value ipairs!]"  \n _ - ")")

   (define-skeleton fnl-skel-collect "table comprehension in fennel" nil
	 "(collect [key value pairs!]" \n _ - ")")

   (define-skeleton fnl-skel-icollect "sequential table comprehension in fennel" nil
	 "(icollect [key value ipairs!]"  \n _ - ")")



   (define-skeleton fnl-skel-new 
	 "boilerplate for doing OOP in fennel, (obj:new args)" nil
	 "(lambda " (skeleton-read "name: ") ".new [this props]"
	 "(let [obj props]"
	 \n > _ -
	 "(setmetatable obj this)"
	 "(set this.__index this)"
	 "obj))")

   (defun multi-define-abbrev (abbrevs abbrev-table)
	 ;;(dolist abbrev abbrevs )
	 (dolist-with-progress-reporter (x abbrevs) "multi-def-abbrev"
	   (define-abbrev abbrev-table (car x) (cdr x))))
   
   (setq fennel-simple-abbrev-list
		 '(("!tcat"		.	"(local tcat table.concat)")
		   ("!fnl"		.	"(local fennel (require :fennel))")
		   ("!view"		.	"(local view fennel.view)")
		   ;;---
		   ("!tprint"	.	"(fn tprint [...] \" table-printing function \" (print (table.concat ...)))")
		   ("!tprint1"	.	"(fn tprint [...] \" table-printing function \" (print (tcat ...)))")
		   ;;---
		   ("!pp"		.	"(fn pp [...] \"pretty-printing function\" (let [fennel (require :fennel)] (print (fennel.view ...))))")
		   ("!pp1"		.	"(fn pp [...] \"pretty-printing function\" (print (fennel.view ...)))")
		   ("!pp2"		.	"(fn pp [...] \"pretty-printing function\" (print (view ...)))")
		   ))
   (multi-define-abbrev fennel-simple-abbrev-list fennel-mode-abbrev-table)
   
   (define-skeleton fnl-skel-boiler
	 "boilerplate with nested abbrevs for fennel" nil
	 "!tcat"
	 "!fnl"
	 "!view"
	 "!tprint1"
	 "!pp2"
	 \n \n
	 "new!"
	 \n > _ -
	 )
   
   (define-abbrev fennel-mode-abbrev-table "do!" "" 'fnl-skel-do)
   (define-abbrev fennel-mode-abbrev-table "while!" "" 'fnl-skel-while)

   (define-abbrev fennel-mode-abbrev-table "lambda!" "" 'fnl-skel-lambda)
   (define-abbrev fennel-mode-abbrev-table "macro!" "" 'fnl-skel-macro)
   (define-abbrev fennel-mode-abbrev-table "fn!" "" 'fnl-skel-function)
   (define-abbrev fennel-mode-abbrev-table "lcoal" "" 'fnl-skel-local)
   (define-abbrev fennel-mode-abbrev-table "lcaol" "" 'fnl-skel-local)
   (define-abbrev fennel-mode-abbrev-table "local!" "" 'fnl-skel-local)
   (define-abbrev fennel-mode-abbrev-table "let!" "" 'fnl-skel-let)
   
   (define-abbrev fennel-mode-abbrev-table "when!" "" 'fnl-skel-when)
   (define-abbrev fennel-mode-abbrev-table "unless!" "" 'fnl-skel-unless)
   (define-abbrev fennel-mode-abbrev-table "req!" "" 'fnl-skel-req)
   (define-abbrev fennel-mode-abbrev-table "require!" "" 'fnl-skel-require)
   (define-abbrev fennel-mode-abbrev-table "pairs!" "" 'fnl-skel-pairs)
   (define-abbrev fennel-mode-abbrev-table "ipairs!" "" 'fnl-skel-ipairs)
   (define-abbrev fennel-mode-abbrev-table "each!" "" 'fnl-skel-each)
   (define-abbrev fennel-mode-abbrev-table "ieach!" "" 'fnl-skel-ieach)
   
   (define-abbrev fennel-mode-abbrev-table "collect!" "" 'fnl-skel-collect)
   (define-abbrev fennel-mode-abbrev-table "icollect!" "" 'fnl-skel-icollect)
   (define-abbrev fennel-mode-abbrev-table "for!" "" 'fnl-skel-for)

   (define-abbrev fennel-mode-abbrev-table "new!" "" 'fnl-skel-new)
   (define-abbrev fennel-mode-abbrev-table "boil!" "" 'fnl-skel-boiler)
   (define-abbrev fennel-mode-abbrev-table "boiler!" "" 'fnl-skel-boiler)
   (define-abbrev fennel-mode-abbrev-table "boilerplate!" "" 'fnl-skel-boiler)
   (define-abbrev fennel-mode-abbrev-table "head!" "" 'fnl-skel-boiler)
   

   (setq-local abbrev-suggest t)
   )
 
)



(provide 'skeleton-conf)

