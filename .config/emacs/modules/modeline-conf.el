;;;; uncrafted-modeline.el --- uncrafted module for setting up the modeline  -*- lexical-binding: t; -*-

;; Copyright (C) 2022
;; SPDX-License-Identifier: MIT

;; Authors: System Crafters Community, David "System Crafters" Wilson, Erik Lundstedt

;; Commentary

;

;;; Code:


(defun telephoneline-my-abs ()
  (setq telephone-line-lhs
	'((evil	. (telephone-line-evil-tag-segment))
	  (accent . (telephone-line-misc-info-segment))
	  (evil . (telephone-line-vc-segment))
	  (nil . (telephone-line-projectile-buffer-segment)))
	  ;;(accent . (telephone-line-simple-minor-mode-segment))
	;;telephone-line-center-lhs '()
	;;telephone-line-center-rhs '()
	telephone-line-rhs 
	'((nil . (telephone-line-flymake-segment telephone-line-flycheck-segment))
	  (evil . (telephone-line-major-mode-segment))
	  )))

(defun telephone-modeline-style ()
  (use-package telephone-line
    :config
    (telephoneline-my-abs)
    (telephone-line-mode t)))

(telephone-modeline-style)


(provide 'modeline-conf)
;;; uncrafted-modeline.el ends here
