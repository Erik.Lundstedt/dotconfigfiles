



(on-system-type
 'nil
 '(add-to-list 'Info-additional-directory-list "c:/Users/66140211/projects/info/" 'true)
 )


(use-package info-colors)
(add-hook 'Info-selection-hook 'info-colors-fontify-node)

(use-package niceify-info)
(add-hook 'Info-selection-hook #'niceify-info)

(use-package info-rename-buffer)

(info-initialize)

(pp Info-directory-list )

;;(setq Info-additional-directory-list nil)

;; (add-to-list Info-additional-directory-list '("C:/Users/66140211/projects/info"))

;; (message Info-directory-list)





(provide 'info-conf)
