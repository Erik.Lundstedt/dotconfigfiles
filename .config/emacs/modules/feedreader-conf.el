
(use-package elfeed
  :config
  (setq elfeed-feeds
		'(("https://www.reddit.com/r/linux.rss"			reddit	linux)
		  ("https://www.reddit.com/r/commandline.rss"	reddit	commandline)
		  ("https://www.reddit.com/r/emacs.rss"			reddit	emacs)
		  ("https://vivaldi.com/blog/feed"				blog	vivaldi)
		  ("https://blog.arduino.cc/feed"				blog	arduino)
		  ("https://archlinux.org/feeds/news/" arch linux news )
		  ("https://archlinux.org/feeds/packages/x86_64/" arch linux packages)
		  ("https://archlinux.org/feeds/packages/added/x86_64/" arch linux recent packages)
		  ("https://aur.archlinux.org/rss" arch linux recent packages aur)
		  )))

;;("name" "url")
(setq newsticker-url-list-defaults nil
	  newsticker-url-list
	  '(("vivaldi-blog" "https://vivaldi.com/blog/feed")
		("Arduino-Blog" "https://blog.arduino.cc/feed")
		(	"hass-blog" "https://www.home-assistant.io/atom.xml"))
	  ;;	  elfeed-feeds '("https://vivaldi.com/blog/feed")
	  )

(use-package elfeed-goodies
  :after elfeed
  :init
  (elfeed-goodies/setup)
  :config
  (setq elfeed-goodies/entry-pane-size 0.5)
  )

;;(setq elfeed-goodies/entry-pane-size 0.5)

(evil-define-key 'normal elfeed-show-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)






(add-hook
 'newsticker-treeview-item-mode-hook
 (lambda ()
   (centered-cursor-mode)
   (focus-mode)))







(provide 'feedreader-conf)
