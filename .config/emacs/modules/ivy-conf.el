(use-package all-the-icons)

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
		 :map evil-normal-state-map
		 ("/" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :custom ((ivy-initial-inputs-alist nil))
  :config
  (savehist-mode)
  (ivy-mode 1)
  )




(use-package counsel
  :bind (("C-x C-f" . 'counsel-find-file)
		 ("C-M-j " . 'counsel-switch-buffer)
		 ("M-x   " . 'counsel-M-x)
		 ("<f1> f" . 'counsel-describe-function)
		 ("<f1> v" . 'counsel-describe-variable)
		 ("<f1> o" . 'counsel-describe-symbol)
		 ("<f1> l" . 'counsel-find-library)
		 ("<f2> i" . 'counsel-info-lookup-symbol)
		 ("<f2> u" . 'counsel-unicode-char)
		 ("C-c  g" . 'counsel-git)
		 ("C-c  j" . 'counsel-git-grep)
		 ("C-x  l" . 'counsel-locate)
		 :map minibuffer-local-map
		 ("C-r" . 'counsel-minibuffer-history))
  :config
  (counsel-mode 1)
  )

(use-package ivy-hydra
  :after ivy
  :after hydra
  :bind (:map counsel-mode-map
		 ("C-e" . 'hydra-ivy/body)))



(use-package ivy-prescient
  :after counsel
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  ;; Uncomment the following line to have sorting remembered across sessions!
  (prescient-persist-mode 1)
  (ivy-prescient-mode 1))

(use-package historian)
(use-package ivy-historian)


(use-package ivy-rich
  :after ivy
  :init (ivy-rich-mode 1))


(use-package all-the-icons-ivy-rich
  :after ivy-rich
  :hook (ivy-rich-mode . all-the-icons-ivy-rich-mode)
  :init (all-the-icons-ivy-rich-mode 1)
  :config (all-the-icons-ivy-rich-mode 1))







(provide 'ivy-conf)
