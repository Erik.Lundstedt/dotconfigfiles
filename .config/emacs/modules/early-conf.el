(use-package browse-at-remote)
(use-package posframe)
(use-package move-dup :config (global-move-dup-mode))
(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

(use-package hydra)
(use-package fontawesome)
(use-package pretty-hydra :after hydra )
(use-package major-mode-hydra
  :after hydra 
  ;;:bind ("SPC b" . major-mode-hydra)
  )


;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-c d")		'server-edit)

(use-package general
  :after evil
  :config
  (general-create-definer el/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "M-SPC"))


(use-package file-info
  :config
  (setq hydra-hint-display-type 'posframe
	hydra-posframe-show-params
	`(:poshandler posframe-poshandler-frame-center
				  :internal-border-width 2
				  :internal-border-color "#61AFEF"
				  :left-fringe 16
				  :right-fringe 16)))

;;(defun on-system-type (nix win)
;;  (if
;;	(eq system-type 'windows-nt)
;;		((eval win))
;;	(eq system-type 'gnu/linux)
;;		(eval nix)))

(defmacro on-system-type (nix win)
  (if
	(eq system-type 'windows-nt)
		(list 'eval win)
	(eq system-type 'gnu/linux)
		(list 'eval nix)))


(use-package bind-key)
(use-package key-chord)
(use-package bind-chord)

(fset 'yes-or-no-p 'y-or-n-p)  ;; Ask for y/n instead of yes/no





(provide 'early-conf)
