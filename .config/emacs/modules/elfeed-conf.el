
(use-package elfeed
  :config
  (setq elfeed-curl-program-name "C:/Program Files/Git/mingw64/bin/curl.exe")
  (setq elfeed-feeds
		'(("https://www.reddit.com/r/linux.rss" reddit linux)
		  ("https://www.reddit.com/r/commandline.rss" reddit commandline)
		  ("https://www.reddit.com/r/emacs.rss" reddit emacs)
		  ("https://www.gamingonlinux.com/article_rss.php" gaming linux)
		  ("https://hackaday.com/blog/feed/" hackaday linux)
		  ("https://opensource.com/feed" opensource linux)
		  ("https://linux.softpedia.com/backend.xml" softpedia linux)
		  ("https://itsfoss.com/feed/" itsfoss linux)
		  ("https://www.phoronix.com/rss.php" phoronix linux)
		  ("https://www.computerworld.com/index.rss" computerworld linux)
		  ("https://www.networkworld.com/category/linux/index.rss" networkworld linux)
		  ("https://www.techrepublic.com/rssfeeds/topic/open-source/" techrepublic linux)
		  ("https://betanews.com/feed" betanews linux)
		  )))




(use-package elfeed-goodies
  :after elfeed
  :init
  (elfeed-goodies/setup)
  ;;:config
  ;;(setq elfeed-goodies/entry-pane-size 0.5)
  )

(setq elfeed-goodies/entry-pane-size 0.5)

(evil-define-key 'normal elfeed-show-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)


(provide 'elfeed-conf)
