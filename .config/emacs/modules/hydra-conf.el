
(defhydra dumb-jump-hydra (:colour blue :columns 3)
  "Dumb Jump"
  ("j" dumb-jump-go "Go")
  ("o" dumb-jump-go-other-window "Other window")
  ("e" dumb-jump-go-prefer-external "Go external")
  ("x" dumb-jump-go-prefer-external-other-window "Go external other window")
  ("i" dumb-jump-go-prompt "Prompt")
  ("l" dumb-jump-quick-look "Quick look")
  ("b" dumb-jump-back "Back"))


(defhydra bookmark-hydra (:colour blue)
  "bookmarks"
  ("m" bookmark-set "add mark")
  ("j" bookmark-jump "jump")
  ("l" list-bookmarks "list")
  ("d" bookmark-delete "delete a bookmark" )
  
  ("S" bookmark-save "save bookmarks")
  ("L" bookmark-load "load bookmarks")
  ("W" bookmark-write "save to other file")

  ("i" bookmark-insert "insert in the buffer the contents of the file BOOKMARK points to")
  ("I" bookmark-insert-location "insert the name of the file containing BOOKMARK")
  )



(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t)
  ("q" nil "finished" :exit t)
  ("x" nil "finished" :exit t))



(pretty-hydra-define mastodon-hydra (:title (with-material "groups" "mastodon" 1 -0.05))
  ("Follow"
   (("t" (mastodon-tl--follow-tag) (with-material "local-offer" "Tag" 1 -0.05))
	("u" (mastodon-tl--follow-user) (with-material "person" "Person" 1 -0.05))
	)))




(major-mode-hydra-define emacs-lisp-mode nil
  ("Eval"
   (("b" eval-buffer "buffer")
    ("e" eval-defun "defun")
    ("r" eval-region "region")
	("l" eval-last-sexp "last")
	)
;;	"REPL" (("I" ielm "ielm"))
;;	"Test" (("t" ert "prompt") ("T" (ert t) "all")  ("F" (ert :failed) "failed"))
   "Doc"
   (("d" describe-foo-at-point "thing-at-pt")
    ("f" describe-function "function")
    ("v" describe-variable "variable")
    ("i" info-lookup-symbol "info lookup"))))
(major-mode-hydra-define dired-mode (:title (with-material "folder_open" "files" 1 -0.05))
  ("Create"
   (("d" dired-create-directory  (with-material "create_new_folder" 1 -0.05))
	("f" dired-create-empty-file (with-material "note_add" "File" 1 -0.05))
	)))


(pretty-hydra-define
  newsticker-treeview-hydra (:title "newsticker-hydra" :quit-key "q" :quit-key "x" :quit-key "f")
  ("navigation"
   (("n" focus-next-thing "next paragraph")
	("p" focus-prev-thing "prev paragraph")
	("j" newsticker--next-message "next message")
	("k" newsticker--prev-message "prev message")
	)))



(setq major-mode-hydra-title-generator
	  '(lambda (mode)
         (s-concat
		  "\n" (s-repeat 10 " ")
          (all-the-icons-icon-for-mode mode :v-adjust 0.05)
          " " (symbol-name mode) " commands")))

(defvar jp-window--title (with-faicon "windows" "Window Management" 1 -0.05))

(pretty-hydra-define jp-window (:foreign-keys warn :title jp-window--title :quit-key "q")
  ("Actions"
   (("TAB" other-window "switch")
    ("x" ace-delete-window "delete")
    ("m" ace-delete-other-windows "maximize")
    ("s" ace-swap-window "swap")
    ("a" ace-select-window "select"))

   "Resize"
   (("h" move-border-left "←")
    ("j" move-border-down "↓")
    ("k" move-border-up "↑")
    ("l" move-border-right "→")
    ("n" balance-windows "balance")
    ("f" toggle-frame-fullscreen "toggle fullscreen"))

   "Split"
   (("b" split-window-right "horizontally")
    ("B" split-window-horizontally-instead "horizontally instead")
    ("v" split-window-below "vertically")
    ("V" split-window-vertically-instead "vertically instead"))

   "Zoom"
   (("+" zoom-in "in")
    ("=" zoom-in )
    ("-" zoom-out "out")
    ("0" jp-zoom-default "reset"))))



(use-package symbol-overlay)
(use-package smartparens)


(defvar jp-toggles--title (with-faicon "toggle-on" "Toggles" 1 -0.05))

(pretty-hydra-define jp-toggles
	(:colour amaranth :quit-key "q" :title jp-toggles--title)
	("Basic"
		(("n" display-line-numbers-mode "line number" :toggle t)
		( "w" whitespace-mode "whitespace" :toggle t)
		( "W" whitespace-cleanup-mode "whitespace cleanup" :toggle t)
		( "r" rainbow-mode "rainbow" :toggle t)
		( "L" page-break-lines-mode "page break lines" :toggle t))
	"Highlight"
		(("s" symbol-overlay-mode "symbol" :toggle t)
		( "l" hl-line-mode "line" :toggle t)
		( "t" hl-todo-mode "todo" :toggle t)
		( "x" highlight-sexp-mode "sexp" :toggle t))
		"Coding"
		(("p" smartparens-mode "smartparens" :toggle t)
		( "P" smartparens-strict-mode "smartparens strict" :toggle t)
		( "S" show-smartparens-mode "show smartparens" :toggle t)
		( "f" flycheck-mode "flycheck" :toggle t))
	 "Emacs"
		(("D" toggle-debug-on-error "debug on error" :toggle (default-value 'debug-on-error))
		 ( "X" toggle-debug-on-quit "debug on quit" :toggle (default-value 'debug-on-quit)))))





(provide 'hydra-conf)
