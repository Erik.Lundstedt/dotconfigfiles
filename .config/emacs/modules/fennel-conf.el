(use-package fennel-mode)
(require 'fennel-mode)
(require 'projectile)

(use-package rx)
(use-package imenu-extra)
;;(imenu-extra-auto-setup)
(use-package monroe)
(use-package tree-sitter)
(use-package tree-sitter-langs)

;;(prettify-symbols-mode)

(use-package
  flymake-fennel
  :after fennel
  :after flymake)

;; (defvar fennel-local-fn-pattern
;;   (rx (syntax open-parenthesis)
;;	  (or "fn" "lambda" "λ" "macro") (1+ space)
;;	  (group (1+ (or (syntax word) (syntax symbol) "-" "_")))))

;; (defvar fennel-local-var-pattern
;;   (rx (syntax open-parenthesis)
;;	  (or "global" "var" "local") (1+ space)
;;	  (group (1+ (or (syntax word) (syntax symbol))))))

;;(message fennel-local-fn-pattern)

(defun rexp ()
	(rx-to-string
	 '(seq
	   (group bol (= 3 (syntax <)))
	   space
	   (group (1+ printing) eol)
	   )t)
;;(rx-define ol-head)
	;;(rx)
	)



;;(rexp)
;;(insert ";; rexp=>" "\"" (rexp)"\"" )
;;(highlight-regexp (rexp) )
;;(add-to-list 'imenu-generic-expression `("var" ,fennel-local-var-pattern 1))


(setq fennel-imenu-generic-expression
 	  '(("variables" "\\s(\\(?:global\\|local\\|var\\|set\\)[[:space:]]+\\(\\(?:\\sw\\|\\s_\\)+\\)" 1)
		("functions" "\\s(\\(?:fn\\|lambda\\|macro\\|λ\\)[[:space:]]+\\(\\(?:\\sw\\|\\s_\\|-\\|_\\|\\.\\)+\\)" 1)
		("headers"	 "\\(^\\s<\\{3,\\}\\)[[:space:]]\\([[:print:]]+$\\)" 2)
		("headings"	 "\\(?:^;;;+\\)\\(?:[[:word:]]\\|[[:space:]]\\)+" 1)
		))

;;(add-to-list 'fennel-imenu-generic-expression `("functions" ,fennel-local-fn-pattern 1))
;;(add-to-list 'fennel-imenu-generic-expression `("variables" ,fennel-local-var-pattern 1))



;; (setq fennel-program "/usr/bin/env fennel")




(add-hook 'fennel-mode-hook
		  (lambda ()
			(with-eval-after-load 'insert-shebang
			  (add-to-list 'insert-shebang-file-types '("fnl" . "fennel")))
			(use-package rainbow-delimiters)
			(rainbow-delimiters-mode)
			(abbrev-mode t)
			;;(flymake-fennel-setup)
			;;(flymake-mode t)
			
			(setq prettify-symbols-alist
				  '(;;("lambda" . 955); λ
					("lambda"	. ?λ)
					("->"		. ?→)
					("not="		. ?≠)
					))
			
	 		(global-prettify-symbols-mode 1)
			

			(which-function-mode)
			(setq-local
			 evil-args-openers '("[" "{" "(")
			 evil-args-delimiters '("," ":" "\n" " ")
			 evil-args-closers '("]" "}" ")")
			 )
		
			;;(setq imenu-generic-expression lisp-outline-imenu-generic-expression)
			
			;; (setq outline-heading-alist
			;; 	  '(("\\(^\\s<\\{3,\\}\\)[[:space:]]\\([[:print:]]+$\\)" . 1)
			;; 		("\\(^\\s<\\{4,\\}\\)[[:space:]]\\([[:print:]]+$\\)" . 2)
			;; 		("\\(^\\s<\\{5,\\}\\)[[:space:]]\\([[:print:]]+$\\)" . 3)
			;; 		("\\(^\\s<\\{6,\\}\\)[[:space:]]\\([[:print:]]+$\\)" . 4)))
			
			(setq-local outline-regexp "\\(?:;;;+\\)\\(?:[[:word:]]\\|[[:space:]]\\)+")
			(outline-minor-mode t)
			(setq imenu-auto-rescan t)
			(yas-minor-mode t)
			(company-mode t)
			

			
			;;(hideshowvis-enable)
			;;(setq-local inferior-lisp-program fennel-program)
			(speedbar-add-expansion-list '(".fnl" ".lua" ".awm"))
			(setq-local compilation-environment
						'("PATH=/usr/bin:/usr/local/bin:/usr/local/sbin:/usr/pkg/sbin:/usr/pkg/bin:/home/erik/bin:/home/erik/go/bin:/home/erik/scripts:/home/erik/.luarocks/bin:/home/erik/botsdir/bin:"
						  "FENNEL_PATH=/home/erik/botsdir/?/?.fnl;/home/erik/botsdir/?.fnl;/home/erik/botsdir/lib/?.fnl;/home/erik/botsdir/lib/?/?.fnl;/home/erik/.config/fennelScripts/?.fnl;/home/erik/.config/fennel-scripts/?.fnl;/home/erik/.config/fennelScripts/?.rc.fnl;./?/?.fnl;./?.fnl;./lib/?/?.fnl;./lib/?.fnl;"))
			(indent-tabs-mode t)
			(setq speedbar-use-imenu-flag t)
			(setq-local imenu-generic-expression fennel-imenu-generic-expression)
			(add-hook 'after-save-hook (lambda () (check-parens)))
			;;(fennel-proto-repl "/usr/bin/fennel")
			)
		  )


(defun fennel/testing ()
  (interactive)
  (message-box projectile-project-root)
  (message-box projectile-project-type)
  )

(defun outline-imenu ()
  (interactive)
  (setq imenu-generic-expression fennel-imenu-generic-expression)
  (imenu-update-menubar)
  (speedbar-refresh)
  )



;;(customize-set-variable projectile-completion-system 'Ivy)







;;/home/erik/botsdir/bin/

	 
 (projectile-register-project-type
  'fennel '("bots.fnl")
  :project-file	"project.fnl"
  :configure	"botsbuildbots configure"
  :compile		"botsbuildbots build"
  :install		"botsbuildbots install"
  :run			"botsbuildbots run"
  :test			"botsbuildbots test"
  )

(setq compilation-window-height 10)


;;(projectile-register-project-type 'make '("Makefile")
;;                                  :project-file "Makefile"
;;                                  :compile "make"
;;                                  :test "make test"
;;                                  :install "make install")

;;(standard-display-ascii ?\t "|  |")
;;(standard-display-ascii ?\t "\t")

;;(fennel-format-region )




(defun fnl-align-table (alig)
  (interactive "sAlign using: ")
  (let ((pmin (point-min)) (pmax (point-max)))
	(setq align-to-tab-stop t)
	(indent-tabs-mode t)
	(evil-indent pmin pmax)
	(align-regexp
	 pmin pmax
	 (concat
	  "\\(\\s-*\\)"
	  "\:" alig ))))



;; (add-hook 'fennel-mode-hook
;; 		  (lambda ()
			;;(define-key fennel-mode-map [remap evil-indent] 'fnl-align-table)
			;;(define-key fennel-mode-map (kbd "C-<return>") 'auto-complete)
			;;(evil-define-key 'insert fennel-mode-map (kbd "C-<return>") 'auto-complete)
			;;(define-key fennel-mode-map (kbd "C-c f") 'fennel-format-buffer)
			;;(with-eval-after-load 'eglot	(add-to-list 'eglot-server-programs '(fennel-mode . ("fennel-ls")))) 
			;;(customize-set-variable speedbar-tag-hierarchy-method 6) 
			;;'(speedbar-prefix-group-tag-hierarchy speedbar-trim-words-tag-hierarchy)
			;;(message-box "added keybinds")
			;; ))

;;; fennel-speedbar stuff

;;(setq fennel-speedbar-mode-map (speedbar-make-specialized-keymap))
;;(defun fennel-install-speedbar-variables ())
;;(with-eval-after-load 'speedbar (fennel-install-speedbar-variables))

;;; botsmenu


(provide 'fennel-conf)
;;; fennel-conf.el ends here
