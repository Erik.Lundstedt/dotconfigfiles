
;;(evil-define-key 'normal  (kbd ))

(global-set-key (kbd "M-<tab>") 'speedbar-get-focus);;alt-tab as I want it
(global-set-key (kbd "C-M-<return>") 'company-complete)


(defun el/insert-c-l ()
  (interactive)
  (insert "\f\n"))


(use-package banner-comment
  :commands (banner-comment)
  :bind ("C-c h" . banner-comment)
  :config
  (add-hook 'lisp-mode-hook (lambda () (setq-local banner-comment-start ";;"))))

 
(el/leader-keys
  "<"	'(counsel-switch-buffer :which-key "switch buffer interactively")
  "TAB"	'(tab-bar-switch-to-tab :which-key "switch tab")
  "."	'(find-file :which-key "find-file")
  ","	'(find-file-other-tab :which-key "find-file-other-tab")
  "u"	'(vundo :which-key "visual undo, like undo-tree")
  ;;"e"	'(:ignore t :which-key "personal binding space")
  ;;"e s"	'(:ignore t :which-key "org-src-stuff")
  "s"	'(:ignore t :which-key "show")
  "j"	'(:ignore t :which-key "jump")
  "p"	'(:ignore t :which-key "page navigation")
  "g"	'(:ignore t :which-key "goto")
  "g d"	'(dumb-jump-hydra/body :which-key "goto definition using dumb-jump")
  )

(el/leader-keys
  "i"	'(:ignore t :which-key "show info")
  "i f"	'(file-info-show :which-key "show info about current file"))
;;(el/leader-keys "g ("	'() "g )" '()  )





(el/leader-keys
  'normal org-mode-map
  "RET"		'(el/eval-src-block		:which-key "eval sourceblock")
  "e s e"	'(el/org-edit-src-code	:which-key "edit src block"))
(el/leader-keys
  'normal org-src-mode-map
  "RET"		'(eval-buffer		:which-key "eval sourceblock")
  "e s s"	'(org-edit-src-save	:which-key "save src block")
  "e s w"	'(org-edit-src-exit	:which-key "save and exit src block"))

(el/leader-keys
  'normal emacs-lisp-mode-map
  "e"		'(:ignore t			:which-key "eval")
  "e s"		'(eval-last-sexp	:which-key "eval last sexp")
  "e b"		'(eval-buffer		:which-key "eval buffer")
  )
(el/leader-keys
  'visual emacs-lisp-mode-map
  "e r"		'(eval-region		:which-key "eval region")
  )


(el/leader-keys
  'normal projectile-mode-map
  "h"		'(:ignore t						:which-key "build/compile/run")
  "h c"		'(projectile-configure-project	:which-key "configure	project")
  "h i"		'(projectile-install-project	:which-key "install		project")
  "h r"		'(projectile-run-project		:which-key "run			project")
  "h t"		'(projectile-test-project		:which-key "test		project")
  )








;;(use-package casual-suite)
 (use-package casual-avy)
 (use-package casual-calc)
 (use-package casual-dired)
 (use-package casual-bookmarks)
 (use-package casual-re-builder)
 ;; (use-package casual-ibuffer)
 ;; (use-package casual-ibuffer)
 (use-package casual-ibuffer)
 (use-package casual-isearch)
;;(with-eval-after-load 'xkcd  (el/leader-keys))





(provide 'keybind-conf)
