(use-package insert-shebang)
;;(use-package flylisp)


(use-package yasnippet
  :bind (:map yas-minor-mode-map ("C-<return>" . hydra-yas/body))
  :hook (prog-mode . yas-minor-mode)
  )
;;   (hydra-yas (:color blue :hint nil)
;;           "
;;               ^YASnippets^
;; --------------------------------------------
;;   Modes:    Load/Visit:    Actions:

;;  _g_lobal  _d_irectory    _i_nsert
;;  _m_inor   _f_ile         _t_ryout
;;  _e_xtra   _l_ist         _n_ew
;;          _a_ll
;; "
;;           ("d" yas-load-directory)
;;           ("e" yas-activate-extra-mode)
;;           ("i" yas-insert-snippet)
;;           ("f" yas-visit-snippet-file :color blue)
;;           ("n" yas-new-snippet)
;;           ("t" yas-tryout-snippet)
;;           ("l" yas-describe-tables)
;;           ("g" yas/global-mode)
;;           ("m" yas/minor-mode)
;;           ("a" yas-reload-all))


(use-package yasnippet-snippets
  :after yasnippets)

(use-package yasnippet-capf
  :after yasnippets)

(use-package dumb-jump
  :config	(remove-hook 'xref-backend-functions #'etags--xref-backend)
  :hook		('xref-backend-functions . #'dumb-jump-xref-activate))


(use-package all)


(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
   '(setq projectile-project-search-path
          '(("~/git/projects-local/lisp/fennel" . 4 )
			("~/git/projects-local/www/" . 3 )
			("~/git/projects-local/arduinoPlatformio" . 2)
			("~/git/projects-local/" . 10))
		  projectile-switch-project-action #'projectile-dired
		  projectile-indexing-method 'git))

;;(Projectile-mode)



(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))



(use-package company
  :hook (prog-mode . company-mode)
  :bind
  (:map company-active-map ("<tab>" . company-complete-selection))
  :custom
  (company-minimum-prefix-length 0)
  (company-idle-delay 0.0)
  )

;;(use-package company-yasnippet
;;  :config (add-to-list 'company-yasnippet company-backends))


;;(global-set-key (kbd "<tab>") 'company-indent-or-complete-selection)

(use-package company-shell
  :after company)

(use-package company-box
  :hook (company-mode . company-box-mode)
  :config (add-to-list 'company-box-frame-parameters '(font . "Ubuntu Mono Ligaturized-20")))

(use-package company-lua :after company)

(use-package company-try-hard
  :after company-try-hard
  :config
  (global-set-key (kbd "S-<return>") #'company-try-hard)
  (define-key company-active-map (kbd "S-<return>") #'company-try-hard))

(setq compilation-window-height 20)

;;(progn (use-package platformio-mode) (use-package arduino-cli-mode) (use-package company-arduino) )


  
  (use-package lsp-mode
	:hook ((clojure-mode . lsp)
         (clojurec-mode . lsp)
         (clojurescript-mode . lsp))
  :config
  ;; add paths to your local installation of project mgmt tools, like lein
  (setenv "PATH" (concat "/usr/local/bin" path-separator (getenv "PATH")))
  (dolist (m '(clojure-mode
               clojurec-mode
               clojurescript-mode
               clojurex-mode))
    (add-to-list 'lsp-language-id-configuration `(,m . "clojure")))
  ;;(setq lsp-clojure-server-command '("/path/to/clojure-lsp"))
	; Optional: In case `clojure-lsp` is not in your $PATH
  )

;;(use-package clojure-mode :commands clojure-mode)

;;(use-package lsp-ui :commands lsp-ui-mode)


(with-eval-after-load 'tramp
  (add-to-list 'tramp-methods
			   '("yadm"
				 (tramp-login-program "yadm")
				 (tramp-login-args (("enter")))
				 (tramp-login-env (("SHELL") ("/bin/sh")))
				 (tramp-remote-shell "/bin/sh")
				 (tramp-remote-shell-args ("-c"))))
  )

  (defun yadm-status ()
	(interactive)
	(magit-status "/yadm::")
	)




(defun el/pretty-lambda ()
  "make some word or string show as pretty Unicode symbols"
  (interactive)
  (setq prettify-symbols-alist
		'(("lambda" . 955)))); λ

(add-hook 'lisp-mode-hook 'el/pretty-lambda)
;;(add-hook 'fennel-mode-hook 'el/pretty-lambda)
(add-hook 'emacs-lisp-mode-hook 'el/pretty-lambda)
(add-hook 'scheme-mode-hook 'el/pretty-lambda)
(global-prettify-symbols-mode 1)

(defun highlight-bool-bait ()
  (highlight-phrase "true" 'hi-green-b)
  (highlight-phrase "True" 'hi-green-b)
  (highlight-phrase "TRUE" 'hi-green-b)
  (highlight-phrase "false" 'hi-red-b)
  (highlight-phrase "False" 'hi-red-b)
  (highlight-phrase "FALSE" 'hi-red-b)
  )

(defun highlight-word-bait ()
  (highlight-phrase "function " 'hi-blue-b)
  (highlight-phrase "fn " 'hi-blue-b)
  (highlight-phrase "lambda " 'hi-blue-b)
  (highlight-phrase "defun " 'hi-blue-b) 
  )



;;(add-hook 'prog-mode-hook 'highlight-bool-bait)
;;(add-hook 'prog-mode-hook 'highlight-word-bait)
;;(add-hook 'conf-mode-hook 'highlight-bool-bait)
;;(add-hook 'json-mode-hook 'highlight-bool-bait)




;;(require 'semantic :hook (prog-mode . semantic-mode))

;;(with-eval-after-load 'speedbar (require 'semantic/sb))
(provide 'ide-conf)
