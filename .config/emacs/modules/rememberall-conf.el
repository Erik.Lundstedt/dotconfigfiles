;;yes, the filename a Harry Potter reference

(use-package outline)

;;(use-package outline-magic)
(use-package outline-minor-faces
  :after outline
  :hook (outline-minor-mode . outline-minor-faces-mode)
  )


(use-package bicycle
  :after outline
  :bind (:map outline-minor-mode-map
              ([C-tab] . bicycle-cycle)
              ([S-tab] . bicycle-cycle-global)))

;; (use-package prog-mode
;;   :ensure nil
;;   :config
;;   (add-hook 'prog-mode-hook #'outline-minor-mode)
;;   (add-hook 'prog-mode-hook #'hs-minor-mode)
;;   )





(use-package remember)
(setq remember-data-directory "~/notes/rememberall"
	  todo-directory "~/notes/todo/"
	  )
 
 (setq calendar-date-style "iso"
	   remember-notes-initial-major-mode 'outline-mode
	   todo-initial-file "todo")

(use-package narrowed-page-navigation)

(defun el/remember-with-filename ()
  "Use the filename"
  (interactive)
  (let ((remember-annotation-functions '(buffer-file-name)))
    (call-interactively 'remember)))
 

(defun el/remember-with-project-name ()
  "Use the project name"
  (interactive)
  (let ((remember-annotation-functions '(projectile-project-name)))
    (call-interactively 'remember)))

(el/leader-keys 'normal
  "r"			'(:ignore t :which-key "Remember")
  "r n"			'(remember-notes :which-key "Notes")
  "r N"			'(remember-other-frame :which-key "in new/other frame");;TODO make this a lambda or function,so the new frame is "drop-down" like the quake terminal
  "r a"			'(remember-append-to-file :which-key "append to file")
  "r c"			'(remember-clipboard :which-key "clipboard")
  "r r"			'(remember-region :which-key "region")
  )
(el/leader-keys 'normal remember-mode-map
  "r w"		'(remember-finalize :which-key "write(save)")
  "r q"		'(remember-destroy :which-key "destroy the buffer")
  )


(el/leader-keys 'normal
  "t"			'(:ignore t :which-key "todo")
  "t t"			'(todo-show :which-key "show,open,TODO")
  "t n"			'(todo-insert-item :which-key "new")
  ;;===================================================================
  "t a"			'(:ignore t :which-key "add")
  "t a f"		'(todo-add-file :which-key "file")
  "t a c"		'(todo-add-category :which-key "category")
  ;;===================================================================
  "t c"			'(:ignore t :which-key "check")
  "t c f"		'(todo-check-file :which-key "check file")
  "t c F"		'(todo-check-format :which-key "check formatting")
  )

(el/leader-keys 'normal
  "c"			'(:ignore t :which-key "calendar")
  "c c"			'(calendar :which-key "open calendar")
  ;; "c"			'( :which-key "")
  ;; "c"			'( :which-key "")
  ;; "c"			'( :which-key "")
  ;;====================================================================
  "d"			'(:ignore t :which-key "diary")
  "d d"			'(diary :which-key "open diary")
  ;; "d"			'( :which-key "")
  ;; "d"			'( :which-key "")
  ;; "d"			'( :which-key "")
  ;; "d"			'( :which-key "")
  )

  (use-package bookmark-view)
  (use-package autobookmarks)
  (use-package simple-bookmarks)
  (use-package bookmark-frecency)


(el/leader-keys 'normal
  "b"			'(:ignore t :which-key "bookmarks")
  "b a"			'(bookmark-set :which-key "create new bookmark")
  "b l"			'(bookmark-bmenu-list :which-key "list-bookmarks")
  )







(pretty-hydra-define hydra-page-nav (:title "page-navigation" :colour blue)
  ("evil/emacs" 
   (("k" backward-page "jump page  up ")
	("j" forward-page  "jump page down")
	("p" backward-page "jump page prev")
	("n" forward-page  "jump page next"))
   "page-up/down/arrows"
   (("<prior>"	backward-page	"prior page")
	("<next>"	forward-page	"next page")
	("<up>"		backward-page	"page <up>")
	("<down>"	forward-page	"page <down>"))
   "exit"
   (("f" nil	"finished" :exit t)
	("q" nil	"finished" :exit t)
	("x" nil	"finished" :exit t))))

(el/leader-keys 'normal
  "j J"			'(hydra-page-nav/body :which-key "navigate by page with hydra")
  "j j <prior>"	'(backward-page	:which-key "jump to previous page")
  "j j <next>"	'(forward-page	:which-key "jump to next page")
  "p j <prior>"	'(backward-page	:which-key "Page Jump up")
  "p j <next>"	'(forward-page	:which-key "Page Jump Down")
  "p j <up>"	'(backward-page	:which-key "Page Jump up")
  "p j <down>"	'(forward-page	:which-key "Page Jump Down")
  "p j k"		'(backward-page	:which-key "Page Jump up")
  "p j j"		'(forward-page	:which-key "Page Jump Down")
  "p j p"		'(backward-page	:which-key "Page Jump Previous")
  "p j n"		'(forward-page	:which-key "Page Jump Next")
  "p i"			'(insert-c-l	:which-key "insert pagebreak symbol")
  "p j i"		'(insert-c-l	:which-key "insert pagebreak symbol")
  )





(el/leader-keys 'normal
  "n"			'(:ignore t :which-key "narrow")
  "n d"			'(narrow-to-defun :which-key "narrow to defun")
  "n D"			'(narrow-to-defun-include-comments :which-key "narrow to defun and include comments")
  "n n"			'(narrow-to-region :which-key "narrow to region")
  "n p"			'(narrowed-page-navigation-mode :which-key "narrow to page")
  "n j"			'(narrowed-page-navigation-next :which-key "next page")
  "n k"			'(narrowed-page-navigation-previous :which-key "previous page")
  "n w"			'(widen :which-key "widen")
  )






(provide 'rememberall-conf)
