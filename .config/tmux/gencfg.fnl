#!/usr/bin/env fennel

(local config {:debug false :printComments true})

(fn note [...]
  "prints a comment if enabled"
  (if config.printComments
	  (print (.. "# " (table.concat [...] " ")))))

;;(local yes "yes")
;;(local no "no")
;; (local (on off) (values :on :off))
;; (local (yes no) (values on off))




;; # Base16 oomox-xresources-reverse
;; # Scheme author: oomox-xresources-reverse
;; # Template author: Tinted Theming: (https://github.com/tinted-theming)

;; # default statusbar colors
;; set-option -g status-style "fg=#383c4b,bg=#242837"

;; # default window title colors
;; set-window-option -g window-status-style "fg=#383c4b,bg=default"

;; # active window title colors
;; set-window-option -g window-status-current-style "fg=#fff796,bg=default"

;; # pane border
;; set-option -g pane-border-style "fg=#242837"
;; set-option -g pane-active-border-style "fg=#666666"

;; # message text
;; set-option -g message-style "fg=#8099ee,bg=#242837"

;; # pane number display
;; set-option -g display-panes-active-colour "#e5f779"
;; set-option -g display-panes-colour "#fff796"

;; # clock
;; set-window-option -g clock-mode-colour "#e5f779"

;; # copy mode highlight
;; set-window-option -g mode-style "fg=#383c4b,bg=#666666"

;; # bell
;; set-window-option -g window-status-bell-style "fg=#242837,bg=#ef8171"




(local mux {})
(fn list [tbl]
  (let [t []]
	(each [k v (pairs tbl)]
	  (note k v)
	  (table.insert t (table.concat [k "=" "'" v "'"])))
 (table.concat t ",")))

;;export CM_LAUNCHER=fzf

(local opts
	   {:base-index 1				;start counting at 1 so keybinding are better
		:renumber-windows true		;renumber windows to you dont have gaps in window numbers
		:mouse true				;turn on mouse support
		:status-style (list {:bg "#242837" :fg "#8099ee"})	;set statusbar colours to my theme
		:set-clipboard true		;enable using system clipboard
		})

(local wopts
	   {:mode-keys "vi"
		:automatic-rename true
		:monitor-activity true
		:monitor-silence "1"
		:pane-base-index 1
		:pane-border-indicators "both"
		:window-status-bell-style "reverse"
;;		:window-status-separator "||"
		:window-status-style "default"
		:remain-on-exit true
		})
(local keys [])
(fn bindkey [bind]
  (fn key [key action] {: key : action}))

(fn main []
	(fn setopt [scope opt]
	  (when (= (type opt.val) "boolean")
		(if opt.val
			(tset opt :val "on")
			(tset opt :val "off")))
	  (table.concat [scope :-g opt.key opt.val] " "))
	(local tables {:set-option opts :set-window-option wopts})
	(print "# SPDX-License-Identifier: SimPL-2.0")
	(print "# Written by Erik Lundstedt")
	(each [k v (pairs tables)]
	  (each [key val (pairs v)]
		(print (setopt k {: key : val})))))

(main)
{:main main}
