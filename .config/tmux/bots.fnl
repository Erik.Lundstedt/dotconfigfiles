#!/usr/bin/env fennel
(local bots {})
(local projname "tmuxconf")
(local tgt "tmux.conf")
;;(local tgtlicense (.. "~/fennelbin/licenses/" projname ".org"))


(fn exec [t sep]
  "This is a simple wrapper around os.execute,
but wrapping the argument in <nil-safe> (table.concat t sep).
if sep? is nil, it will use an empty string, so remember to add spaces where needed"
  (os.execute (table.concat t (or sep "")))
  (print (table.concat t (or sep ""))))



(fn ask [prompt]
  (print prompt "\t[y/n]")
  (let [answer (io.read 1)]
	(match answer
	  :y true
	  :Y true
	  :n false
	  :N false
	  _ true
	  )
	)
  )


(set bots.files
	  {:target tgt
	   :main "gencfg.fnl"
	   :tmptarget "tmux.tmp.conf"
	   }
	  )


(fn bots.build [this]
  (exec ["./" this.files.main " > " this.files.tmptarget])
  (print "done")
  )


(fn bots.install [this]
  (this:build)
  (exec ["diff" "-y" this.files.tmptarget this.files.target] " ")
  (when (ask "apply changes?")
	(exec ["install " "--compare " this.files.tmptarget this.files.target] " ")
	;;(os.execute (table.concat ["install " "--compare " "LICENSE.org" tgtlicense] " "))
	(print "*INSTALL* done")
	))

(fn bots.uninstall [this]
  (exec ["rm " "-f " this.files.target])
  ;;(os.execute (.. "rm " "-f " tgtlicense))
  (print "*UNINSTALL* done"))

(bots:build)
(bots:install)
