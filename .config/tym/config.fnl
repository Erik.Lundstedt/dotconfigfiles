#!/usr/bin/env fennel
(local tym (require :tym))
(local defaultFont "Ubuntu Mono Ligaturized 16")
(local uriSchemes [:https :gemini :file :mailto])
(local unclutter true)
(local cursorShapes {:block :block :ibeam :ibeam :underline :underline})

(fn bgAlpha [alpha]
  (local (r g b a) (tym.color_to_rgba (tym.get :color_background)))
  (tym.notify (table.concat [r g b a alpha] ",") "rgba")
  (tym.rgba_to_color r g b alpha)

  (fn rgbaToColour [r g b a]
	(.. "rgba(" (table.concat
				 [r g b ;;(.. "0." (* 10 a))
				  (string.format "0.%i" (math.floor (* a 100)))]
				 ",") ")"))
  (rgbaToColour r g b alpha)
  )

(local window {:width (+ 80 0)
			   :height (+ 22 2)
			   :paddingHori 10
			   :paddingVert 10})

(tym.set_config {:font defaultFont
				 :uri_schemes (table.concat uriSchemes " ")
				 :autohide unclutter
				 :cursor_shape cursorShapes.ibeam
				 ;;:color_background (bgAlpha 0.8)
				 ;;:color_background "rgba(36,40,55,0.8)";;(bgAlpha 0.8)
				 :shell "/usr/bin/zsh"
				 ;;:shell "/bin/zsh"
				 :width window.width
				 :height window.height
				 :padding_horizontal window.paddingHoriz
				 :padding_vertical window.paddingVerti})

(λ updatealpha [x]
  (var (r g b a) (tym.color_to_rgba (tym.get :color_background)))
  (set a (math.max (math.min 1.0 (+ a x)) 0.0))
  ;;(local bg (tym.rgba_to_color r g b a))
  ;;(os.setlocale "" "")
  (tym.set :color_background (bgAlpha a))
  (tym.notify (string.format "%s alpha to %f" (or (and (> x 0) :Inc) :Dec) a))
  ;;(tym.notify (string.format "%f" a))
  ;;(tym.put (string.format "#%s\n" (bgAlpha a)))
  )
(fn update-alpha [delta]
  (local (r g b a) (tym.color_to_rgba (tym.get :color_background)))
  (local alpha (math.max (math.min 1.0 (+ a delta)) 0.0))
  ;;(local bg (tym.rgba_to_color r g b (+ a delta)))
  ;;(tym.notify (.. r "," g "," b "," a))
  
  (fn rgbaToColour [r g b a]
	(.. "rgba(" (table.concat
				 [r g b ;;(.. "0." (* 10 a))
				  (string.gsub (string.format "%.2f" a) "," "." )]
				 ",") ")"))
  (tym.notify (rgbaToColour r g b a))
  ;;(tym.set :color_background (rgbaToColour r g b a))
  ;;(tym.set :color_background (tym.rgba_to_color r g b a))
  ;;(tym.notify (string.format "%s alpha to %f" (or (and (> delta 0) :Inc) :Dec) a))
  ;;(tym.notify (.. a ))

  )





(local alphaChangeAspect 0.1)

(tym.set_keymaps
 {:<Ctrl><Shift>Up
  (fn [] (update-alpha alphaChangeAspect))
  :<Ctrl><Shift>Down
  (fn [] (update-alpha (- alphaChangeAspect)))
  }
 )

(fn check-mod-state []
  (if
   (tym.check_mod_state "<Ctrl>")
   :ctrl
   (tym.check_mod_state "<Shift>")
   :shift
   ))



(tym.set_hook
 :scroll
 (fn [dx dy x y]
   (tym.notify (check-mod-state))
   (if (tym.check_mod_state :<Ctrl>)
	   (do
		 (if (> dy 0) (global s (- (tym.get :scale) 10))
			 (global s (+ (tym.get :scale) 10)))
		 (tym.set :scale s)
		 (tym.notify (.. "set scale: " s "%"))
		 true
		 )
	   (tym.check_mod_state :<Shift>)
	   (do (update-alpha
			(or (and (< dy 0) alphaChangeAspect) (- alphaChangeAspect)))
		 true
		 )
	 )))

;; (tym.set_hook
;;  :clicked
;;  (fn [button uri]
;;    (print "you pressed button:" button)
;;    (when (= button 2)
;; 	 (when uri
;; 	   (print "you clicked URI: " uri)
;; 	   (os.execute (.. "jaro "  uri  "&"))
;; 	   ;;(tym.open uri)
;; 	   true))))
 (tym.notify (tym.get :color_background))
