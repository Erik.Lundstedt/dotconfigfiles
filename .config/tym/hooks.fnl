#!/usr/bin/env fennel
(local default-hooks
	   {:title (fn [])
		:bell nil
		:clicked
		:scroll
		:drag
		:activated
		:deactivated
		:selected
		:unselected
		:signal
		}
	   )
