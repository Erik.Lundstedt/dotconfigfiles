(local xplr xplr)

;;; Config

(local defaultStyle
	   {
		:add_modifiers nil
		:bg nil
		:fg nil
		:sub_modifiers nil
		})

(lambda logstyle [{:add-modifiers ?add-modifiers
			  :sub-modifiers ?sub-modifiers
			  :bg ?bg
			  :fg ?fg}
			 {:add_modifiers (or ?add-modifiers nil)
			  :sub_modifiers (or ?sub-modifiers nil)
			  :bg (or ?bg nil)
			  :fg (or ?fg nil) }])

;;;; General

(local logs
		{:error
		 {:format "ERROR"
		  :style (logstyle {:fg "Red"})}
		 :info
		 {:format "INFO"
		  :style (logstyle {:fg "LightBlue"})}
		 :success
		 {:format "SUCCESS"
		  :style (logstyle {:fg "Green"})}
		 :warning
		 {:format "WARNING"
		  :style (logstyle {:fg "Yellow"})}})




(local generalConfig
	   {:show_hidden false
		:read_only false
		:enable_recover_mode true
		:start_fifo nil
		:hide_remaps_in_help_menu false
		:prompt	{:format "❯ " :style defaultStyle}
		:initial_layout "default"
		:initial_mode "default"
		:initial_sorting
		[{ :sorter "ByCanonicalIsDir" :reverse true }
		 { :sorter "ByIRelativePath" :reverse false }]
		:logs logs
		:default_ui
		{:prefix "  "
		 :suffix ""
		 :style defaultStyle}
		:focus_ui
		{:prefix "▸["
		 :suffix "]"
		 :style
		 {:add_modifiers [ "Bold" ]
		  :sub_modifiers nil
		  :bg nil
		  :fg "Blue"}}
		:selection_ui
		{:prefix " {"
		 :suffix "}"
		 :style
		 {:add_modifiers [ "Bold" ]
		  :sub_modifiers nil
		  :bg nil
		  :fg "LightGreen"}}
		:focus_selection_ui
		{:prefix "▸["
		 :suffix "]"
		 :style
		 {:add_modifiers ["Bold"]
		  :sub_modifiers nil
		  :bg nil
		  :fg "LightGreen"}}
		:sort_and_filter_ui
		{:separator
		 {:format " › "
		  :style
		  {:add_modifiers ["Dim"]
		   :bg nil
		   :fg nil
		   :sub_modifiers nil}}
		 :default_identifier
		 {:format nil
		  :style
		  {:add_modifiers ["Bold"]
		   :sub_modifiers  nil
		   :bg nil
		   :fg nil}}
		 :filter_identifiers
		 {:AbsolutePathDoesContain {:format "abs=~" :style defaultStyle}
		  :AbsolutePathDoesEndWith {:format "abs=$" :style defaultStyle}
		  :AbsolutePathDoesNotContain {:format "abs!~" :style defaultStyle}
		  :AbsolutePathDoesNotEndWith {:format "abs!$" :style defaultStyle}
		  :AbsolutePathDoesNotStartWith {:format "abs!^" :style defaultStyle}
		  :AbsolutePathDoesStartWith {:format "abs=^" :style defaultStyle}
		  :AbsolutePathIs {:format "abs==" :style defaultStyle}
		  :AbsolutePathIsNot {:format :abs!= :style defaultStyle}
		  :IAbsolutePathDoesContain {:format "[i]abs=~" :style defaultStyle}
		  :IAbsolutePathDoesEndWith {:format "[i]abs=$" :style defaultStyle}
		  :IAbsolutePathDoesNotContain {:format "[i]abs!~" :style defaultStyle}
		  :IAbsolutePathDoesNotEndWith {:format "[i]abs!$" :style defaultStyle}
		  :IAbsolutePathDoesNotStartWith {:format "[i]abs!^" :style defaultStyle}
		  :IAbsolutePathDoesStartWith {:format "[i]abs=^" :style defaultStyle}
		  :IAbsolutePathIs {:format "[i]abs==" :style defaultStyle}
		  :IAbsolutePathIsNot {:format "[i]abs!=" :style defaultStyle}
		  :IRelativePathDoesContain {:format "[i]rel=~" :style defaultStyle}
		  :IRelativePathDoesEndWith {:format "[i]rel=$" :style defaultStyle}
		  :IRelativePathDoesNotContain {:format "[i]rel!~" :style defaultStyle}
		  :IRelativePathDoesNotEndWith {:format "[i]rel!$" :style defaultStyle}
		  :IRelativePathDoesNotStartWith {:format "[i]rel!^" :style defaultStyle}
		  :IRelativePathDoesStartWith {:format "[i]rel=^" :style defaultStyle}
		  :IRelativePathIs {:format "[i]rel==" :style defaultStyle}
		  :IRelativePathIsNot {:format "[i]rel!=" :style defaultStyle}
		  :RelativePathDoesContain {:format "rel=~" :style defaultStyle}
		  :RelativePathDoesEndWith {:format :rel=$ :style defaultStyle}
		  :RelativePathDoesNotContain {:format "rel!~" :style defaultStyle}
		  :RelativePathDoesNotEndWith {:format "rel!$" :style defaultStyle}
		  :RelativePathDoesNotStartWith {:format "rel!^" :style defaultStyle}
		  :RelativePathDoesStartWith {:format "rel=^" :style defaultStyle}
		  :RelativePathIs {:format "rel==" :style defaultStyle}
		  :RelativePathIsNot {:format "rel!=" :style defaultStyle}}
		 :sort_direction_identifiers
		 {:forward
		  {:format "↓"
		   :style defaultStyle}
		  :reverse
		  {:format "↑"
		   :style defaultStyle}}
		 :sorter_identifiers
		 {:ByCanonicalAbsolutePath {:format "[c]abs" :style defaultStyle}
		  :ByCanonicalExtension {:format "[c]ext" :style defaultStyle}
		  :ByCanonicalIsDir {:format "[c]dir" :style defaultStyle}
		  :ByCanonicalIsFile {:format "[c]file" :style defaultStyle}
		  :ByCanonicalIsReadonly {:format "[c]ro" :style defaultStyle}
		  :ByCanonicalMimeEssence {:format "[c]mime" :style defaultStyle}
		  :ByCanonicalSize {:format "[c]size" :style defaultStyle}
		  :ByExtension {:format "ext" :style defaultStyle}
		  :ByICanonicalAbsolutePath {:format "[ci]abs" :style defaultStyle}
		  :ByIRelativePath {:format "[i]rel" :style defaultStyle}
		  :ByISymlinkAbsolutePath {:format "[si]abs" :style defaultStyle}
		  :ByIsBroken {:format "⨯" :style defaultStyle}
		  :ByIsDir {:format "dir" :style defaultStyle}
		  :ByIsFile {:format "file" :style defaultStyle}
		  :ByIsReadonly {:format "ro" :style defaultStyle}
		  :ByIsSymlink {:format "sym" :style defaultStyle}
		  :ByMimeEssence {:format "mime" :style defaultStyle}
		  :ByRelativePath {:format "rel" :style defaultStyle}
		  :BySize {:format "size" :style defaultStyle}
		  :BySymlinkAbsolutePath {:format "[s]abs" :style defaultStyle}
		  :BySymlinkExtension {:format "[s]ext" :style defaultStyle}
		  :BySymlinkIsDir {:format "[s]dir" :style defaultStyle}
		  :BySymlinkIsFile {:format "[s]file" :style defaultStyle}
		  :BySymlinkIsReadonly {:format "[s]ro" :style defaultStyle}
		  :BySymlinkMimeEssence {:format "[s]mime" :style defaultStyle}
		  :BySymlinkSize {:format "[s]size" :style defaultStyle}
		  }}
		:panel_ui
		{:default
		 {:borders ["Top" "Right" "Bottom" "Left" ]
		  :style defaultStyle
		  :title
		  {:format nil
		   :style defaultStyle}}
		 :help_menu
		 {:borders nil
		  :style defaultStyle
		  :title
		  {:format nil
		   :style defaultStyle}}
		 :input_and_logs
		 {:borders nil
		  :style defaultStyle
		  :title
		  {:format nil
		   :style defaultStyle}}
		 :selection
		 {:borders nil
		  :style defaultStyle
		  :title
		  {:format nil
		   :style defaultStyle}}
		 :sort_and_filter
		 {:borders nil :style defaultStyle
		  :title
		  {:format nil :style defaultStyle}}
		 :table
		 {:borders nil
		  :style defaultStyle
		  :title
		  {:format nil
		   :style defaultStyle }}}
		:table
		{:style defaultStyle
		 :col_spacing  1
		 :col_widths
		 [{:Percentage 10}
		  {:Percentage 50}
		  {:Percentage 10}
		  {:Percentage 10}
		  {:Percentage 20}]
		 :header
		 {:cols
		  [{:format " index"
			:style defaultStyle}
		   {:format "╭──── path"
			:style defaultStyle}
		   {:format "permissions"
			:style defaultStyle}
		   {:format "size"
			:style defaultStyle}
		   {:format "type"
			:style defaultStyle}]
		  :height 1
		  :style
		  {:add_modifiers ["Bold"]
		   :sub_modifiers nil
		   :bg nil
		   :fg nil}}
		 :row
		 {:cols
		  [{:format "builtin.fmt_general_table_row_cols_0" :style defaultStyle}
		   {:format "builtin.fmt_general_table_row_cols_1" :style defaultStyle}
		   {:format "builtin.fmt_general_table_row_cols_2" :style defaultStyle}
		   {:format "builtin.fmt_general_table_row_cols_3" :style defaultStyle}
		   {:format "builtin.fmt_general_table_row_cols_4" :style defaultStyle}]
		  :height 0
		  :style defaultStyle }
		 :tree
		 [{:format "├─" :style defaultStyle}
		  {:format "├─" :style defaultStyle}
		  {:format "╰─" :style defaultStyle}]}})

(set xplr.config.general generalConfig)
