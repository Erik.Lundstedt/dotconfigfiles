(local xplr xplr)


(local go-back
	   {:help "back"
		:messages
		["PopMode"
		 {:SwitchModeBuiltin "default"}
		 ]})

(fn term [class title cmd args ]
  ;;(.. "kitty --class float --title " cmd cmd (table.concat args))
  (.. "tym " "--role " class  " --title " title " -e " cmd (table.concat args " "))
  ;;(.. "kitty "  "--class " class "
  ;;--title " title " -1 " " -e " cmd (table.concat args) "&")
  )

(local cfg
	   {
		;;-------------general---------------------
		:term      term
		:editor    "nvim "
		:visual    "emacsclient "
		:imageCat  "imv "
		:mediaplay "mpv "
		:pager     "less "
		:top       "btop "
		;;-------------music-----------------------
;;		{:name "open terminal texteditor" :cmd (gencmd cfg.editor     ["-l"])}
;;		{:name "resource monitor"         :cmd (gencmd cfg.top        [""])}
		}
	   )
;;; :
(fn run-builder [helptext cmd args interm]
  (local command (if (= cmd nil) "echo error" cmd))
  (table.insert args "\"$XPLR_FOCUS_PATH\"");;"${\"XPLR_FOCUS_PATH\"}")
  (table.insert args " &");;"${\"XPLR_FOCUS_PATH\"}")
  (fn gencmd [cmd args]
	(cfg.term "float" cmd cmd args)
	)
  {
   :help helptext
   :messages
   ["PopMode"
	;;{:BashExec "nvim $XPLR_FOCUS_PATH"};;Silently}
	{
	 :BashExec ;;Silently
	 (if interm
		 (gencmd command args)
		 (table.concat [command (table.concat args " ")] " ")
		 )
	 }
	]
   }
  )


;;; :

(local open-in
	   {
		:editor     (run-builder "tui texteditor"  cfg.editor [""] true)
		:visual     (run-builder "gui texteditor " cfg.visual [""] false)
		:imageCat   (run-builder "image viever"    cfg.imageCat [""] false)
		:mediaplay  (run-builder "play media"      cfg.mediaPlay [""] false)
		:pager      (run-builder "show in pager"   cfg.pager [""] true)
		:universal	(run-builder "open with jaro"	"jaro " [""] false)
		}
	   )

(set xplr.config.modes.custom.open
	 {:name "open with"
	  :key_bindings
	  {:on_key
	   {:e open-in.editor
		:E open-in.visual
		:v open-in.mediaplay
		:p open-in.pager
		;;:x (. open-in :xdg)
		:i open-in.imageCat
		:q go-back
		:escape go-back
		:ctrl-c go-back}}})






(tset xplr.config.modes.builtin.default.key_bindings.on_key "o"
	  {:help "open_with-mode"
	   :messages
	   [;;{:SwitchModeCustom "open"}
		{:BashExec (table.concat ["jaro" "\"$XPLR_FOCUS_PATH\""] " ")}
		]})




(set xplr.config.modes.custom.mypreview
	 {:name "preview"
	  :key_bindings
	  {:on_key
	   {;;		:i (. open-in :imageCat)
		:j {:help "down"    :messages [:FocusNext {:BashExecSilently (.. "imv-open " "$XPLR_FOCUS_PATH" "&")}]}
		:k {:help "up"      :messages [:FocusPrevious {:BashExecSilently (.. "imv-open " "$XPLR_FOCUS_PATH" "&")}]}
		:down {:help "down" :messages [:FocusNext {:BashExecSilently (.. "imv-open " "$XPLR_FOCUS_PATH" "&")}]}
		:up {:help "up"     :messages [:FocusPrevious {:BashExecSilently (.. "imv-open " "$XPLR_FOCUS_PATH" "&")}]}
		:space {:help "toggle selection" :messages [:ToggleSelection	:FocusNext	{:BashExecSilently (.. "imv-open " "$XPLR_FOCUS_PATH" "&")}]}
		:q go-back
		:escape go-back
		:ctrl-c go-back}
	   }
	  }
	 )





(tset xplr.config.modes.builtin.default.key_bindings.on_key "f2"
	  {:help "previewImage"
		:messages
	   [{:BashExecSilently (.. "imv-open " "$XPLR_FOCUS_PATH")}
		{:SwitchModeCustom "mypreview"}
		]
	   }
	  )
