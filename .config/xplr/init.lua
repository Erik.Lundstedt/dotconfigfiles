-- You need to define the script version for compatibility check.
-- See https://github.com/sayanarijit/xplr/wiki/Upgrade-Guide.
--
-- version = "1.0.0"


--require("cfg")


local xplr = xplr

local home = os.getenv("HOME")
package.path = home
 --  .. '/.config/xplr/?.fnl;'
 --  .. home
   .. '/.config/xplr/?.lua;'
--   .. home
--   .. '/.config/xplr/luafiles/?.lua;'
--    .. home
--    .. "/.config/xplr/plugins/?/src/init.lua;"
--    .. home
--    .. "/.config/xplr/plugins/?/init.lua;"
--    .. home
--    .. "/.config/xplr/plugins/?/lib/?.lua;"
-- --   .. home
--   .. "/.config/xplr/plugins/luamark/?.lua;"
--   .. home
--   .. "/.config/xplr/plugins/luamark/lib/?.lua;"
   .. package.path

local fennel = require("./fennel")
local oldpath = fennel.path

fennel.path = fennel.path
  .. ";"
  .. home
  .. "/.config/xplr/plugins/?/init.fnl;"
  .. home
  .. "/.config/xplr/plugins/?.fnl;"
  .. home
  .. "/.config/xplr/?.fnl;"

table.insert(package.loaders or package.searchers, fennel.searcher)

local var = require("var")
version = var.version

require("xplr-rc")
fennel.path=oldpath
