(local xplr xplr)

(set xplr.config.modes.builtin.default.key_bindings.on_key.ctrl-s xplr.config.modes.builtin.default.key_bindings.on_key.ctrl-f)

(set
 xplr.config.modes.builtin.default.key_bindings.on_key.m
 {:help "tydra"
  :messages [{:BashExec "tydrashow xplrActions"}]})
