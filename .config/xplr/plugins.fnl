(local vars (require :var))

(local xpm (require "xpm"))

(local home (os.getenv "HOME"))
(local use-plugin xpm.use_plugin)

  ;; (use-plugin
  ;;  {
  ;;   :name "gitlab:/Erik.Lundstedt/luamark.xplr"
  ;;   ;;:setup
  ;;   ;; (fn []
  ;;   ;;   (local luamark (require "luamark"))
  ;;   ;;   (luamark.setup
  ;;   ;;    {
  ;;   ;;     :directory (.. home "/.config" "/bookmarks")
  ;;   ;;     }
  ;;   ;;    )
  ;;   ;;   ;;(luamark.setup)
  ;;   ;;   )
  ;;    }
  ;;  )


(use-plugin {:name "prncss-xyz/icons.xplr"})
(use-plugin {:name "dtomvan/extra-icons.xplr"})
  ;; (use-plugin
  ;;  {
  ;;   :name "sayanarijit/zoxide.xplr"
  ;;   }
  ;;  )

  (use-plugin
   {:name "dtomvan/paste-rs.xplr"
	:setup
	(fn []
	  (local pasteRS (require "paste-rs"))
	  (pasteRS.setup)
	  )})

;;(use-plugin "igorepost/context-switch")
(use-plugin
 {:name "igorepst/context-switch.xplr"
  :setup
  (fn []
	(local ct (require "context-switch"))
	(ct.setup {:key "ctrl-x" }))})




(use-plugin "sayanarijit/scp.xplr")


  ;; (use-plugin
  ;;  {
  ;;   :name "sayanarijit/fzf.xplr"
  ;;   :setup
  ;;   (fn []
  ;;     (local fzf (require :fzf))
  ;;     (fzf.setup {:mode "default" :key "ctrl-s"})
  ;;     )
  ;;   }
  ;;  )



  (use-plugin {
   :name "sayanarijit/dragon.xplr"
   :setup
   (fn []
	 (local dragon (require :dragon))
	 (dragon.setup
	  {:bin vars.dragon.bin
	   :drag_args "--all"
	   :drop_args "--all"
	   :keep_selection true
	   :key "D"}))})

(use-plugin
 {:name "junker/nuke.xplr"
  :setup
  (fn []
	(local nuke (require :nuke))
	(nuke.setup
	 {:open
	  {:run_executables false
	   :custom
	   [{:mime_regex "^image/.*" :command "imv-open {}"}
		{:mime_regex ".*" :command "jaro {}"}]
	   } :view {:show_line_numbers true}
	  }))})

(local key xplr.config.modes.builtin.default.key_bindings.on_key)
(set key.v
	 {:help "nuke"
	  :messages
	  [:PopMode
	   {:SwitchModeCustom "nuke"}]})
(tset key :f3 xplr.config.modes.custom.nuke.key_bindings.on_key.v)
;(tset key :enter xplr.config.modes.custom.nuke.key_bindings.on_key.o)
;;(use-plugin {:name ""})

;; :prncss-xyz/type-to-nav.xplr
;; :sayanarijit/completion.xplr
;; :sayanarijit/dual-pane.xplr
;; :sayanarijit/map.xplr
;; :sayanarijit/xclip.xplr



;;(xpm.update_plugins)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (xpm.setup                                     ;;
;;  [                                             ;;
;;   {1 :sayanarijit/command-mode.xplr            ;;
;;    :after (fn []                               ;;
;;             (require :commands)                ;;
;;             )                                  ;;
;;    }                                           ;;
;;   :dtomvan/icons.xplr                          ;;
;;   :dtomvan/ouch.xplr                           ;;
;;   :igorepst/context-switch.xplr                ;;
;;   {1                                           ;;
;;    :igorepst/term.xplr                         ;;
;;    :setup (fn []                               ;;
;;             (local term (require :term))       ;;
;;             (local k-hsplit                    ;;
;;                    (term.profile_tmux_hsplit)) ;;
;;             (set k-hsplit.key :ctrl-h)         ;;
;;             (term.setup                        ;;
;;              [                                 ;;
;;               (term.profile_tmux_vsplit)       ;;
;;               k-hsplit]                        ;;
;;              )                                 ;;
;;             )                                  ;;
;;    }                                           ;;
;;   :prncss-xyz/icons.xplr                       ;;
;;   :prncss-xyz/type-to-nav.xplr                 ;;
;;   :sayanarijit/completion.xplr                 ;;
;;   :sayanarijit/dragon.xplr                     ;;
;;   :sayanarijit/dual-pane.xplr                  ;;
;;   :sayanarijit/fzf.xplr                        ;;
;;   :sayanarijit/map.xplr                        ;;
;;   :sayanarijit/xclip.xplr                      ;;
;;   :sayanarijit/zoxide.xplr                     ;;
;;   ]                                            ;;
;;  )                                             ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
