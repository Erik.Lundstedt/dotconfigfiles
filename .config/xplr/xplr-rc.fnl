;;(global version :0.19.0)
;;(local version "0.17.6")
(local xplr xplr)
(local home (os.getenv :HOME))
(local xpm-path (.. home :/.local/share/xplr/xpm.xplr))
(local xpm-url "https://github.com/dtomvan/xpm.xplr")
(set package.path
	 (.. package.path ";" xpm-path "/?.lua;" xpm-path :/?/init.lua))
(os.execute (string.format "[ -e '%s' ] || git clone '%s' '%s'" xpm-path xpm-url xpm-path))


 (require :layouts)
 ;; (require :config)
 ;; (require :nodetypes)
 ;; (require :functions)
 ;; (require :modes)
(require :keybindings)
;;(require :openIn)
;;(require :imgprev)
(require :plugins)

(fn shExecSilent0 [script]
  {;;:BashExec0
   :BashExecSilently0
   (table.concat script "\n")
   }
  )
(local bookmarksDirectory (.. (os.getenv "HOME") "/.config" "/bookmarks"))
(local builtin xplr.config.modes.builtin)




(tset builtin.default.key_bindings.on_key :i
	 {:help "image preview"
	  :messages
	  [
	   (shExecSilent0
		["FIFO_PATH=\"/tmp/xplr.fifo\""
		"if [ -e \"$FIFO_PATH\" ]; then"
		"\"$XPLR\" -m StopFifo"
		"rm -f -- \"$FIFO_PATH\""
		"else"
		"mkfifo \"$FIFO_PATH\""
		"\"$HOME/.config/xplr/scripts/imv-open-pipe\" \"$FIFO_PATH\" \"$XPLR_FOCUS_PATH\" &"
		"\"$XPLR\" -m 'StartFifo: %q' \"$FIFO_PATH\""
		"fi"]
		)
	   ]})


(set builtin.default.key_bindings.on_key.x
	 {:help "chmod a+x"
	  :messages [{:BashExec"chmod a+x $(echo \"$XPLR_FOCUS_PATH\"|cat $XPLR_PIPE_SELECTION_OUT - |fzf)"}]
	  }
	 )

(set builtin.default.key_bindings.on_key.y
	 {:help "show image"
	  :messages [{:BashExecSilently (string.format "imv-open %q" "$XPLR_FOCUS_PATH")}]
	  }
	 )

(set builtin.default.key_bindings.on_key.ä
	  {:help "xpm"
	   :messages
	   [:PopMode
		{:SwitchModeCustom "xpm"}]})

(tset builtin.default.key_bindings.on_key :n
	  {:help "toggleFIFO"
	   :messages
	   [
		{:ToggleFifo "/home/erik/Desktop/XPLR-fifo.fifo"}
		]
	   }
	 )


;; (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-å"
;;       {:help "mount ftp"
;;        :messages
;;        [
;;         {:BashExec "curlftpfs 192.168.100.111 netdev -o user=dietpi"}
;;         ]
;;        }
;;       )

;;  (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-d"
;;        {:help "removespaces"
;;         :messages [{:BashExecSilently "for file in *; do mv \"$file\" `echo $file|tr '-' ''`;done"}]})

;;  (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-D"
;;        {:help "removespaces -r"
;;         :messages [{:BashExecSilently "for file in */*; do mv \"$file\" `echo $file|tr '-' ''`;done"}]})


;; (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-d"
;;	   {:help "diredit in editor"
;;		:messages [{:BashExecSilently "vidir"}]})

(set xplr.config.modes.builtin.default.key_bindings.on_key.ctrl-d
	 {:help "'dired'"
	  :messages
	  [
	   {:BashExec
		(table.concat
		 [
		  "SELECTION=$(cat \"${XPLR_PIPE_SELECTION_OUT:?}\")"
		  "NODES=${SELECTION:-$(cat \"${XPLR_PIPE_DIRECTORY_NODES_OUT:?}\")}"
		  "if [ \"$NODES\" ]; then"
		  "echo -e \"$NODES\" | renamer"
		  "\"$XPLR\" -m ExplorePwdAsync"
		  "fi"
		  ] "\n")
		}]})


;; (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-p"
;;	  {
;;	   :help "preview"
;;	   :messages
;;	   [{:StartFifo "/tmp/previuwu.fifo"}]
;;	   }
;;	  )
;; (local cmd-mode (require :command-mode))
;; (cmd-mode.setup {:mode "default" :key ";"})

;; (local luamark (require :luamark))
;; (luamark.setup {:mode :default :directory (.. home :/.config :/bookmarks)})
