(local shellFuncs
       {
        :rename
        (.." echo SetInputBuffer:"
           " \"'\"$(basename \"${XPLR_FOCUS_PATH}\")\""
           " '\" >> \"${XPLR_PIPE_MSG_IN:?}\" ")
       :go-home (.. "echo ChangeDirectory:"
                   "\"'\"${HOME:?}\"'\" >>"
                   "\"${XPLR_PIPE_MSG_IN:?}\" ")
        :fzf-search (..
         "SELECTED=$(cat \"${XPLR_PIPE_DIRECTORY_NODES_OUT:?}\" |"
         " awk -F / '{print $NF}' | fzf --no-sort )"
         " if [ \"$SELECTED\" ]; then"
         "echo FocusPath: '\"'$PWD/$SELECTED'\"' >> \"${XPLR_PIPE_MSG_IN:?}\" fi"
         "if [ -d \"$SELECTED\" ]; then"
         "echo Enter >> \"${XPLR_PIPE_MSG_IN:?}\" fi")
        :help (.. "[ -z \"$PAGER\" ] && PAGER=\"less -+F\""
                  "cat -- \"${XPLR_PIPE_GLOBAL_HELP_MENU_OUT}\" | ${PAGER:?}")
        :copy-here (..
                "  (while IFS= read -r line; do"
                "if cp -vr -- \"${line:?}\" ./; then"
                "echo LogSuccess: $line copied to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\""
                "  else"
                "echo LogError: Failed to copy $line to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\""
                "  fi"
                "  done < \"${XPLR_PIPE_SELECTION_OUT:?}\")"
                "  echo ExplorePwdAsync >> \"${XPLR_PIPE_MSG_IN:?}\""
                "  echo ClearSelection >> \"${XPLR_PIPE_MSG_IN:?}\""
                "  read -p \"[enter to continue]\" ")
        :move-here
        (.. "(while IFS= read -r line; do "
            "if mv -v -- \"${line:?}\" ./; then "
            "echo LogSuccess: $line moved to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\" "
            "else "
            "echo LogError: Failed to move $line to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\" "
            "fi "
            "done < \"${XPLR_PIPE_SELECTION_OUT:?}\") "
            "echo ExplorePwdAsync >> \"${XPLR_PIPE_MSG_IN:?}\" "
            "read -p \"[enter to continue]\" ")
       :mkdir
       (.. "  PTH=\"$XPLR_INPUT_BUFFER\""
           "  if [ \"${PTH}\" ]; then"
           "\tmkdir -p -- \"${PTH:?}\" \\"
           "\t&& echo \"SetInputBuffer: ''\" >> \"${XPLR_PIPE_MSG_IN:?}\" \\"
           "\t&& echo ExplorePwd >> \"${XPLR_PIPE_MSG_IN:?}\" \\"
           "\t&& echo LogSuccess: $PTH created >> \"${XPLR_PIPE_MSG_IN:?}\" \\"
           "\t&& echo FocusByFileName: \"'\"$PTH\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\""
           "  else"
           "\techo PopMode >> \"${XPLR_PIPE_MSG_IN:?}\""
           "  fi")
        :touch
" PTH=\"$XPLR_INPUT_BUFFER\"
  if [ \"${PTH}\" ]; then
\ttouch -- \"${PTH:?}\" \\
\t&& echo \"SetInputBuffer: ''\" >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t&& echo LogSuccess: $PTH created >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t&& echo ExplorePwd >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t&& echo FocusByFileName: \"'\"$PTH\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\"
  else
\techo PopMode >> \"${XPLR_PIPE_MSG_IN:?}\"
  fi
"
        }
       )




shellFuncs
