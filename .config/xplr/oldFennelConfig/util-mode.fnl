(local version "0.17.1")

(local xplr xplr)


(local go-back
       {:help "back"
        :messages
        ["PopMode"
         {:SwitchModeBuiltin "default"}
         ]
        }
       )

;;; :
(fn run-builder [helptext cmd args]
  (local command (if (= cmd nil) "echo error" cmd))
;;(table.insert args "$XPLR_FOCUS_PATH");;"${\"XPLR_FOCUS_PATH\"}")
  {
   :help helptext
   :messages
   ["PopMode"
    ;;{:BashExec "nvim $XPLR_FOCUS_PATH"};;Silently}
    {
     :BashExecSilently (table.concat [command (table.concat args " ")] " ")
     }
    "ExplorePwd"
    ]
   }
  )


;;; :

(local actions
       {
        :sortFiles (run-builder "sort into subdirs\n by filetype"  "fileSort" [""])
        }
       )

(set xplr.config.modes.custom.open
     {:name "open with"
      :key_bindings
      {:on_key
       {
        :s actions.sort
        :q go-back
        :escape go-back
        :ctrl-c go-back}}})


(tset xplr.config.modes.builtin.default.key_bindings.on_key "o"
      {:help "open_with-mode"
       :messages
       [
        {:SwitchModeCustom "open"}]})
