;(local version "0.17.6")
(local xplr xplr)
(local vars (require :var))
(set xplr.config.modes.builtin.default
     {:name :default
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {"#"
        {:help nil
         :messages {1 :PrintAppStateAndQuit}}
        :. {:help "show hidden"
            :messages {1 {:ToggleNodeFilter {:filter :RelativePathDoesNotStartWith
                                             :input "."}}
                       2 :ExplorePwdAsync}}
        ":" {:help :action
             :messages {1 :PopMode
                        2 {:SwitchModeBuiltin :action}}}
        :? {:help "global help menu"
            :messages {1 {:BashExec "\t\t\t  [ -z \"$PAGER\" ] && PAGER=\"less -+F\"
\t\t\t  cat -- \"${XPLR_PIPE_GLOBAL_HELP_MENU_OUT}\" | ${PAGER:?}
\t\t\t"}}}
        :G {:help "go to bottom"
            :messages {1 :PopMode 2 :FocusLast}}
        :ctrl-a {:help "select/unselect all"
                 :messages {1 :ToggleSelectAll}}
        :ctrl-c {:help :terminate
                 :messages {1 :Terminate}}
        :ctrl-f {:help :search
                 :messages [ :PopMode
                             {:SwitchModeBuiltin "search"}
                             {:SetInputBuffer ""}
                             :ExplorePwdAsync]}
        :ctrl-i {:help "next visited path"
                 :messages {1 :NextVisitedPath}}
        :ctrl-o {:help "last visited path"
                 :messages {1 :LastVisitedPath}}
        :ctrl-r {:help "refresh screen"
                 :messages {1 :ClearScreen}}
        :ctrl-u {:help "clear selection"
                 :messages {1 :ClearSelection}}
        :ctrl-w {:help "switch layout"
                 :messages {1 {:SwitchModeBuiltin :switch_layout}}}
        :d {:help :delete
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :delete}}}
        :down {:help :down :messages {1 :FocusNext}}
        :enter {:help "quit with result"
                :messages {1 :PrintResultAndQuit}}
        :esc {:help nil :messages {}}
        :f {:help :filter
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :filter}}}
        :g {:help "go to"
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :go_to}}}
        :left {:help :back :messages {1 :Back}}
        :q {:help :quit :messages {1 :Quit}}
        :r {:help :rename
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :rename}
                       3 {:BashExecSilently " echo SetInputBuffer: \"'\"$(basename \"${XPLR_FOCUS_PATH}\")\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\" "}}}
        :right {:help :enter :messages {1 :Enter}}
        :s {:help :sort
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :sort}}}
        :space {:help "toggle selection"
                :messages {1 :ToggleSelection
                           2 :FocusNext}}
        :up {:help :up :messages {1 :FocusPrevious}}
        "~" {:help "go home"
             :messages {1 {:BashExecSilently " echo ChangeDirectory: \"'\"${HOME:?}\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t"}}}}
       :on_number
       {:help "input"
        :messages [ :PopMode
                    {:SwitchModeBuiltin :number}
                    :BufferInputFromKey]
        }
       }
      }
     )



(tset xplr.config.modes.builtin.default.key_bindings.on_key :tab
      (. xplr.config.modes.builtin.default.key_bindings.on_key :ctrl-i))
(tset xplr.config.modes.builtin.default.key_bindings.on_key :v
      xplr.config.modes.builtin.default.key_bindings.on_key.space)
(tset xplr.config.modes.builtin.default.key_bindings.on_key :V
      (. xplr.config.modes.builtin.default.key_bindings.on_key :ctrl-a))
(tset xplr.config.modes.builtin.default.key_bindings.on_key "/"
      (. xplr.config.modes.builtin.default.key_bindings.on_key :ctrl-f))
(tset xplr.config.modes.builtin.default.key_bindings.on_key :h
      xplr.config.modes.builtin.default.key_bindings.on_key.left)
(tset xplr.config.modes.builtin.default.key_bindings.on_key :j
      xplr.config.modes.builtin.default.key_bindings.on_key.down)
(tset xplr.config.modes.builtin.default.key_bindings.on_key :k
      xplr.config.modes.builtin.default.key_bindings.on_key.up)
(tset xplr.config.modes.builtin.default.key_bindings.on_key :l
      xplr.config.modes.builtin.default.key_bindings.on_key.right)


(set xplr.config.modes.builtin.recover
     {:name :recover
      :layout {:CustomContent {:title " recover "
                               :body {:StaticParagraph {:render "
  You pressed an invalid key and went into \"recover\" mode.
  This mode saves you from performing unwanted actions.

  Let's calm down, press `escape`, and try again.
\t\t  "}}}}
      :key_bindings {:on_key {:ctrl-c {:help :terminate
                                       :messages {1 :Terminate}}
                              :esc {:help :escape :messages {1 :PopMode}}}
                     :default {:messages {}}}})
(set xplr.config.modes.builtin.selection_ops
     {:name "selection ops"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:c
        {:help "copy here"
         :messages
         {1 {:BashExec "\t\t\t  (while IFS= read -r line; do
\t\t\t  if cp -vr -- \"${line:?}\" ./; then
\t\t\t\techo LogSuccess: $line copied to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  else
\t\t\t\techo LogError: Failed to copy $line to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  fi
\t\t\t  done < \"${XPLR_PIPE_SELECTION_OUT:?}\")
\t\t\t  echo ExplorePwdAsync >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  echo ClearSelection >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  read -p \"[enter to continue]\"
\t\t\t"}
          2 :PopMode}}
        :ctrl-c {:help :terminate
                 :messages {1 :Terminate}}
        :esc {:help :cancel :messages {1 :PopMode}}
        :m {:help "move here"
            :messages {1 {:BashExec "\t\t\t  (while IFS= read -r line; do
\t\t\t  if mv -v -- \"${line:?}\" ./; then
\t\t\t\techo LogSuccess: $line moved to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  else
\t\t\t\techo LogError: Failed to move $line to $PWD >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  fi
\t\t\t  done < \"${XPLR_PIPE_SELECTION_OUT:?}\")
\t\t\t  echo ExplorePwdAsync >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  read -p \"[enter to continue]\"
\t\t\t"}
                       2 :PopMode}}
        :x {:help "open in gui"
            :messages {1 {:BashExecSilently "\t\t\t  if [ -z \"$OPENER\" ]; then
\t\t\t\tif command -v xdg-open; then
\t\t\t\t  OPENER=xdg-open
\t\t\t\t  elif command -v open; then
\t\t\t\t  OPENER=open
\t\t\t\telse
\t\t\t\t  echo 'LogError: $OPENER not found' >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t\t  exit 1
\t\t\t\tfi
\t\t\t  fi
\t\t\t  (while IFS= read -r line; do
\t\t\t  $OPENER \"${line:?}\" > /dev/null 2>&1
\t\t\t  done < \"${XPLR_PIPE_RESULT_OUT:?}\")
\t\t\t"}
                       2 :ClearScreen
                       3 :PopMode}}}}})
(set xplr.config.modes.builtin.create
     {:name :create
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :d {:help "create directory"
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin "create directory"}
                       3 {:SetInputBuffer ""}}}
        :esc {:help :cancel :messages {1 :PopMode}}
        :f {:help "create file"
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin "create file"}
                       3 {:SetInputBuffer ""}}}}}})
(set xplr.config.modes.builtin.create_directory
     {:name "create directory"
      :help nil
      :extra_help nil
      :key_bindings {:on_key {:ctrl-c {:help :terminate
                                       :messages {1 :Terminate}}
                              :enter {:help "create directory"
                                      :messages {1 {:BashExecSilently "\t\t\t  PTH=\"$XPLR_INPUT_BUFFER\"
\t\t\t  if [ \"${PTH}\" ]; then
\t\t\t\tmkdir -p -- \"${PTH:?}\" \\
\t\t\t\t&& echo \"SetInputBuffer: ''\" >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo ExplorePwd >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo LogSuccess: $PTH created >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo FocusByFileName: \"'\"$PTH\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  else
\t\t\t\techo PopMode >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  fi
\t\t\t"}}}
                              :esc {:help :cancel :messages {1 :PopMode}}}
                     :default {:messages {1 :UpdateInputBufferFromKey}}}})
(set xplr.config.modes.builtin.create_file
     {:name "create file"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :enter {:help "create file"
                :messages {1 {:BashExecSilently "\t\t\t  PTH=\"$XPLR_INPUT_BUFFER\"
\t\t\t  if [ \"${PTH}\" ]; then
\t\t\t\ttouch -- \"${PTH:?}\" \\
\t\t\t\t&& echo \"SetInputBuffer: ''\" >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo LogSuccess: $PTH created >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo ExplorePwd >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo FocusByFileName: \"'\"$PTH\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  else
\t\t\t\techo PopMode >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  fi
\t\t\t"}}}
        :esc {:help :cancel :messages {1 :PopMode}}}
       :on_special_character nil
       :default {:messages {1 :UpdateInputBufferFromKey}}}})
(set xplr.config.modes.builtin.number
     {:name :number
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :down {:help "to down"
               :messages {1 :FocusNextByRelativeIndexFromInput
                          2 :PopMode}}
        :enter {:help "to index"
                :messages {1 :FocusByIndexFromInput
                           2 :PopMode}}
        :esc {:help :cancel :messages {1 :PopMode}}
        :up {:help "to up"
             :messages {1 :FocusPreviousByRelativeIndexFromInput
                        2 :PopMode}}}
       :on_navigation {:messages {1 :UpdateInputBufferFromKey}}
       :on_number {:help :input
                   :messages {1 :UpdateInputBufferFromKey}}}})
(tset xplr.config.modes.builtin.number.key_bindings.on_key :j
      xplr.config.modes.builtin.number.key_bindings.on_key.down)
(tset xplr.config.modes.builtin.number.key_bindings.on_key :k
      xplr.config.modes.builtin.number.key_bindings.on_key.up)
(set xplr.config.modes.builtin.go_to
     {:name "go to"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :esc {:help :cancel :messages {1 :PopMode}}
        :f {:help "follow symlink"
            :messages {1 :FollowSymlink 2 :PopMode}}
        :g {:help :top
            :messages {1 :FocusFirst 2 :PopMode}}
        :x {:help "open in gui"
            :messages {1 {:BashExecSilently "\t\t\t  if [ -z \"$OPENER\" ]; then
\t\t\t\tif command -v xdg-open; then
\t\t\t\t  OPENER=xdg-open
\t\t\t\t  elif command -v open; then
\t\t\t\t  OPENER=open
\t\t\t\telse
\t\t\t\t  echo 'LogError: $OPENER not found' >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t\t  exit 1
\t\t\t\tfi
\t\t\t  fi
\t\t\t  $OPENER \"${XPLR_FOCUS_PATH:?}\" > /dev/null 2>&1
\t\t\t"}
                       2 :ClearScreen
                       3 :PopMode}}}}})
(set xplr.config.modes.builtin.rename
     {:name :rename
      :help nil
      :extra_help nil
      :key_bindings {:on_key {:ctrl-c {:help :terminate
                                       :messages {1 :Terminate}}
                              :enter {:help :rename
                                      :messages {1 {:BashExecSilently "\t\t\t  SRC=\"${XPLR_FOCUS_PATH:?}\"
\t\t\t  TARGET=\"${XPLR_INPUT_BUFFER:?}\"
\t\t\t  mv -- \"${SRC:?}\" \"${TARGET:?}\" \\
\t\t\t\t&& echo ExplorePwd >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo FocusByFileName: \"'\"$TARGET\"'\" >> \"${XPLR_PIPE_MSG_IN:?}\" \\
\t\t\t\t&& echo LogSuccess: $SRC renamed to $TARGET >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t"}
                                                 2 :PopMode}}
                              :esc {:help :cancel :messages {1 :PopMode}}}
                     :default {:help nil
                               :messages {1 :UpdateInputBufferFromKey}}}})
(set xplr.config.modes.builtin.delete
     {:name :delete
      :help nil
      :extra_help nil
      :key_bindings {:on_key {:D {:help "force delete"
                                  :messages {1 {:BashExec "\t\t\t  (while IFS= read -r line; do
\t\t\t  if rm -rfv -- \"${line:?}\"; then
\t\t\t\techo LogSuccess: $line deleted >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  else
\t\t\t\techo LogError: Failed to delete $line >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  fi
\t\t\t  done < \"${XPLR_PIPE_RESULT_OUT:?}\")
\t\t\t  echo ExplorePwdAsync >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  read -p \"[enter to continue]\"
\t\t\t"}
                                             2 :PopMode}}
                              :ctrl-c {:help :terminate
                                       :messages {1 :Terminate}}
                              :d {:help :delete
                                  :messages {1 {:BashExec "\t\t\t  (while IFS= read -r line; do
\t\t\t  if [ -d \"$line\" ] && [ ! -L \"$line\" ]; then
\t\t\t\tif rmdir -v -- \"${line:?}\"; then
\t\t\t\t  echo LogSuccess: $line deleted >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t\telse
\t\t\t\t  echo LogError: Failed to delete $line >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t\tfi
\t\t\t  else
\t\t\t\tif rm -v -- \"${line:?}\"; then
\t\t\t\t  echo LogSuccess: $line deleted >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t\telse
\t\t\t\t  echo LogError: Failed to delete $line >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t\tfi
\t\t\t  fi
\t\t\t  done < \"${XPLR_PIPE_RESULT_OUT:?}\")
\t\t\t  echo ExplorePwdAsync >> \"${XPLR_PIPE_MSG_IN:?}\"
\t\t\t  read -p \"[enter to continue]\"
\t\t\t"}
                                             2 :PopMode}}
                              :esc {:help :cancel :messages {1 :PopMode}}}}})
(set xplr.config.modes.builtin.action
     {:name "action to"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:!
        {:help "shell"
         :messages {1 {:Call {:command vars.shell
                              :args {1 :-i}}}
                    2 :ExplorePwdAsync
                    3 :PopMode}}
        :c {:help :create
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :create}}}
        :ctrl-c {:help :terminate
                 :messages {1 :Terminate}}
        :e {:help "open in editor"
            :messages {1 {:BashExec "\t\t\t  ${EDITOR:-vi} \"${XPLR_FOCUS_PATH:?}\"
\t\t\t"}
                       2 :PopMode}}
        :esc {:help :cancel :messages {1 :PopMode}}
        :l {:help :logs
            :messages {1 {:BashExec "\t\t\t  [ -z \"$PAGER\" ] && PAGER=\"less -+F\"
\t\t\t  cat -- \"${XPLR_PIPE_LOGS_OUT}\" | ${PAGER:?}
\t\t\t"}
                       2 :PopMode}}
        :s {:help "selection operations"
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :selection_ops}}}
        :m {:help "toggle mouse"
            :messages {1 :PopMode 2 :ToggleMouse}}
        :q {:help "quit options"
            :messages {1 :PopMode
                       2 {:SwitchModeBuiltin :quit}}}}
       :on_number {:help "go to index"
                   :messages {1 :PopMode
                              2 {:SwitchModeBuiltin :number}
                              3 :BufferInputFromKey}}}})
(set xplr.config.modes.builtin.quit
     {:name "quit"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:enter
        {:help "just quit"
         :messages {1 :Quit}}
        :p {:help "quit printing pwd"
            :messages {1 :PrintPwdAndQuit}}
        :f {:help "quit printing focus"
            :messages {1 :PrintFocusPathAndQuit}}
        :s {:help "quit printing selection"
            :messages {1 :PrintSelectionAndQuit}}
        :r {:help "quit printing result"
            :messages {1 :PrintResultAndQuit}}
        :esc {:help :cancel :messages {1 :PopMode}}
        :ctrl-c {:help :terminate
                 :messages {1 :Terminate}}}}})
(set xplr.config.modes.builtin.search
     {:name :search
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :down {:help :down :messages {1 :FocusNext}}
        :enter {:help :focus
                :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesContain}
                           2 :PopMode
                           3 :ExplorePwdAsync}}
        :right {:help :enter
                :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesContain}
                           2 :Enter
                           3 {:SetInputBuffer ""}
                           4 :ExplorePwdAsync}}
        :left {:help :back
               :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesContain}
                          2 :Back
                          3 {:SetInputBuffer ""}
                          4 :ExplorePwdAsync}}
        :tab {:help "toggle selection"
              :messages {1 :ToggleSelection 2 :FocusNext}}
        :up {:help :up :messages {1 :FocusPrevious}}}
       :default {:messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesContain}
                            2 :UpdateInputBufferFromKey
                            3 {:AddNodeFilterFromInput :IRelativePathDoesContain}
                            4 :ExplorePwdAsync}}}})
(tset xplr.config.modes.builtin.search.key_bindings.on_key :esc
      xplr.config.modes.builtin.search.key_bindings.on_key.enter)
(tset xplr.config.modes.builtin.search.key_bindings.on_key :ctrl-n
      xplr.config.modes.builtin.search.key_bindings.on_key.down)
(tset xplr.config.modes.builtin.search.key_bindings.on_key :ctrl-p
      xplr.config.modes.builtin.search.key_bindings.on_key.up)


(set xplr.config.modes.builtin.filter
     {:name :filter
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:R
        {:help "relative does not contain"
         :messages {1 {:SwitchModeBuiltin :relative_path_does_not_contain}
                    2 {:SetInputBuffer ""}
                    3 {:AddNodeFilterFromInput :IRelativePathDoesNotContain}
                    4 :ExplorePwdAsync}}
        :ctrl-c {:help :terminate
                 :messages {1 :Terminate}}
        :ctrl-r {:help "reset filters"
                 :messages {1 :ResetNodeFilters
                            2 :ExplorePwdAsync}}
        :ctrl-u {:help "clear filters"
                 :messages {1 :ClearNodeFilters
                            2 :ExplorePwdAsync}}
        :enter {:help :done :messages {1 :PopMode}}
        :r {:help "relative does contain"
            :messages {1 {:SwitchModeBuiltin :relative_path_does_contain}
                       2 {:SetInputBuffer ""}
                       3 {:AddNodeFilterFromInput :IRelativePathDoesContain}
                       4 :ExplorePwdAsync}}}}})
(tset xplr.config.modes.builtin.filter.key_bindings.on_key :esc
      xplr.config.modes.builtin.filter.key_bindings.on_key.enter)
(set xplr.config.modes.builtin.relative_path_does_contain
     {:name "relative path does contain"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :enter {:help "apply filter"
                :messages {1 :PopMode}}
        :esc {:help :cancel
              :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesContain}
                         2 :PopMode
                         3 :ExplorePwdAsync}}}
       :default {:help nil
                 :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesContain}
                            2 :UpdateInputBufferFromKey
                            3 {:AddNodeFilterFromInput :IRelativePathDoesContain}
                            4 :ExplorePwdAsync}}}})
(set xplr.config.modes.builtin.relative_path_does_not_contain
     {:name "relative path does not contain"
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:ctrl-c
        {:help "terminate"
         :messages {1 :Terminate}}
        :enter {:help "apply filter"
                :messages {1 :PopMode}}
        :esc {:help :cancel
              :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesNotContain}
                         2 :PopMode
                         3 :ExplorePwdAsync}}}
       :default {:help nil
                 :messages {1 {:RemoveNodeFilterFromInput :IRelativePathDoesNotContain}
                            2 :UpdateInputBufferFromKey
                            3 {:AddNodeFilterFromInput :IRelativePathDoesNotContain}
                            4 :ExplorePwdAsync}}}})
(set xplr.config.modes.builtin.sort
     {:name :sort
      :help nil
      :extra_help nil
      :key_bindings
      {:on_key
       {:!
        {:help "reverse sorters"
         :messages {1 :ReverseNodeSorters
                    2 :ExplorePwdAsync}}
        :E {:help "by canonical extension reverse"
            :messages {1 {:AddNodeSorter {:sorter :ByCanonicalExtension
                                          :reverse true}}
                       2 :ExplorePwdAsync}}
        :M {:help "by canonical mime essence reverse"
            :messages {1 {:AddNodeSorter {:sorter :ByCanonicalMimeEssence
                                          :reverse true}}
                       2 :ExplorePwdAsync}}
        :N {:help "by node type reverse"
            :messages {1 {:AddNodeSorter {:sorter :ByCanonicalIsDir
                                          :reverse true}}
                       2 {:AddNodeSorter {:sorter :ByCanonicalIsFile
                                          :reverse true}}
                       3 {:AddNodeSorter {:sorter :ByIsSymlink
                                          :reverse true}}
                       4 :ExplorePwdAsync}}
        :R {:help "by relative path reverse"
            :messages {1 {:AddNodeSorter {:sorter :ByIRelativePath
                                          :reverse true}}
                       2 :ExplorePwdAsync}}
        :S {:help "by size reverse"
            :messages {1 {:AddNodeSorter {:sorter :BySize
                                          :reverse true}}
                       2 :ExplorePwdAsync}}
        :backspace {:help "remove last sorter"
                    :messages {1 :RemoveLastNodeSorter
                               2 :ExplorePwdAsync}}
        :ctrl-c {:help :terminate
                 :messages {1 :Terminate}}
        :ctrl-r {:help "reset sorters"
                 :messages {1 :ResetNodeSorters
                            2 :ExplorePwdAsync}}
        :ctrl-u {:help "clear sorters"
                 :messages {1 :ClearNodeSorters
                            2 :ExplorePwdAsync}}
        :e {:help "by canonical extension"
            :messages {1 {:AddNodeSorter {:sorter :ByCanonicalExtension
                                          :reverse false}}
                       2 :ExplorePwdAsync}}
        :enter {:help :done :messages {1 :PopMode}}
        :m {:help "by canonical mime essence"
            :messages {1 {:AddNodeSorter {:sorter :ByCanonicalMimeEssence
                                          :reverse false}}
                       2 :ExplorePwdAsync}}
        :n {:help "by node type"
            :messages {1 {:AddNodeSorter {:sorter :ByCanonicalIsDir
                                          :reverse false}}
                       2 {:AddNodeSorter {:sorter :ByCanonicalIsFile
                                          :reverse false}}
                       3 {:AddNodeSorter {:sorter :ByIsSymlink
                                          :reverse false}}
                       4 :ExplorePwdAsync}}
        :r {:help "by relative path"
            :messages {1 {:AddNodeSorter {:sorter :ByIRelativePath
                                          :reverse false}}
                       2 :ExplorePwdAsync}}
        :s {:help "by size"
            :messages {1 {:AddNodeSorter {:sorter :BySize
                                          :reverse false}}
                       2 :ExplorePwdAsync}}}}})

(tset xplr.config.modes.builtin.sort.key_bindings.on_key :esc
      xplr.config.modes.builtin.sort.key_bindings.on_key.enter)

(set xplr.config.modes.builtin.switch_layout
     {:name "switch layout"
      :help nil
      :extra_help nil
      :key_bindings {:on_key {:1 {:help :default
                                  :messages {1 {:SwitchLayoutBuiltin :default}
                                             2 :PopMode}}
                              :2 {:help "no help menu"
                                  :messages {1 {:SwitchLayoutBuiltin :no_help}
                                             2 :PopMode}}
                              :3 {:help "no selection panel"
                                  :messages {1 {:SwitchLayoutBuiltin :no_selection}
                                             2 :PopMode}}
                              :4 {:help "no help or selection"
                                  :messages {1 {:SwitchLayoutBuiltin :no_help_no_selection}
                                             2 :PopMode}}
                              :ctrl-c {:help :terminate
                                       :messages {1 :Terminate}}
                              :esc {:help :cancel :messages {1 :PopMode}}}}})
(set xplr.config.modes.custom {})
