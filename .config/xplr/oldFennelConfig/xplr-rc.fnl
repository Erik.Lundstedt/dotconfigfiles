;;(global version :0.19.0)
;;(local version "0.17.6")
(local xplr xplr)
(local home (os.getenv :HOME))




(local xpm-path (.. home :/.local/share/xplr/xpm.xplr))
(local xpm-url "https://github.com/dtomvan/xpm.xplr")
(set package.path
     (.. package.path ";" xpm-path "/?.lua;" xpm-path :/?/init.lua))
(os.execute (string.format "[ -e '%s' ] || git clone '%s' '%s'" xpm-path xpm-url xpm-path))


(require :layouts)
(require :config)
(require :nodetypes)
(require :functions)
(require :modes)
(require :openIn)
(require :imgprev)
(require :plugins)

(local bookmarksDirectory (.. (os.getenv "HOME") "/.config" "/bookmarks"))
(tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-b"
      {:help "luamark"
       :messages [{:ChangeDirectory bookmarksDirectory}]}
      )


;; (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-å"
;;       {:help "mount ftp"
;;        :messages
;;        [
;;         {:BashExec "curlftpfs 192.168.100.111 netdev -o user=dietpi"}
;;         ]
;;        }
;;       )

;;  (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-d"
;;        {:help "removespaces"
;;         :messages [{:BashExecSilently "for file in *; do mv \"$file\" `echo $file|tr '-' ''`;done"}]})

;;  (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-D"
;;        {:help "removespaces -r"
;;         :messages [{:BashExecSilently "for file in */*; do mv \"$file\" `echo $file|tr '-' ''`;done"}]})


 (tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-d"
       {:help "diredit in editor"
        :messages [{:BashExecSilently "vidir"}]})


(tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-p"
      {:help "preview"
       :messages [{:BashExecSilently "
FIFO_PATH=/tmp/xplr.fifo
  if [ -e \"$FIFO_PATH\" ]; then
    echo StopFifo >> \"$XPLR_PIPE_MSG_IN\"
    rm \"$FIFO_PATH\"
  else
    mkfifo \"$FIFO_PATH\"
    \"/usr/bin/imv-open-fifo\" \"$FIFO_PATH\" \"$XPLR_FOCUS_PATH\" &
    echo \"StartFifo: '$FIFO_PATH'\" >> \"$XPLR_PIPE_MSG_IN\"
  fi
"
                   }
                  ]
       }
      )


(tset (. (. (. xplr.config.modes.builtin "default") :key_bindings) :on_key) "ctrl-p"
      {
       :help "preview"
       :messages
       [{:StartFifo "/tmp/previuwu.fifo"}]
       }
      )
;; (local cmd-mode (require :command-mode))
;; (cmd-mode.setup {:mode "default" :key ";"})

;; (local luamark (require :luamark))
;; (luamark.setup {:mode :default :directory (.. home :/.config :/bookmarks)})
