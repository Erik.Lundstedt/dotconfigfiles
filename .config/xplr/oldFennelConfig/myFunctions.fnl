;;-*-  mode: whitespace; mode: fennel; tab-width: 4; indent-tabs-mode: t; lua-indent-level: 4; -*-
;; You need to define the script version for compatibility check.
;; See https://github.com/sayanarijit/xplr/wiki/Upgrade-Guide.

;;(local version "0.19.0")

(local xplr xplr)

(local go-back
       {:help "back"
        :messages
        ["PopMode"
         {:SwitchModeBuiltin "default"}
         ]
        }
       )

(fn run-builder [helptext commands]
  {:help helptext
   :messages
    [:PopMode
     {
      "BashExecSilently"
      (table.concat commands " ")}]})

(local f-path "${\"XPLR_FOCUS_PATH\"}")

(local open-in
       {
        :nvim (run-builder    :nvim    [ "wezterm start" "nvim"         f-path  "&"])
        :xdg (run-builder     :xdg     [ "wezterm start" "xdg-open"     f-path  "&"])
        :pager (run-builder   :pager   [ "st" "-e"    "less"  f-path  "&"])
        :emacs (run-builder   :emacs   [ "emacsclient"  f-path  "&"])
        :mpv (run-builder     :mpv     [ "mpv"          f-path  "&"])
        :kodi (run-builder    :kodi    [ "kodi"         f-path  "&"])
        :dmenufm (run-builder :dmenufm [ "dmenufm"      f-path  "&"])
        :sxiv (run-builder    :six4    [ "nsxiv"        f-path  "&"])
        :feh (run-builder     :feh     [ "feh"          f-path  "&"])
        }
       )

(set xplr.config.modes.custom.open
     {:name "open with"
      :key_bindings
      {:on_key
       {
        :e (. open-in :emacs)
        :v (. open-in :mpv)
        :p (. open-in :pager)
        :x (. open-in :xdg)
        :i (. open-in :feh)
        :q go-back
        :escape go-back
        :ctrl-c go-back}}})

(tset xplr.config.modes.builtin.default.key_bindings.on_key "o"
      {:help "open_with-mode"
       :messages
       [
        {:SwitchModeCustom "open"}]})
