;; You need to define the script version for compatibility check.
;; See https://github.com/sayanarijit/xplr/wiki/Upgrade-Guide.
;;
;;(local version "0.17.6")

(local xplr xplr)

;;; Config

(local defaultStyle
       {
        :add_modifiers nil
        :bg nil
        :fg nil
        :sub_modifiers nil
        }
       )

;;;; General
;;;;; Show hidden

(local generalConfig
       {
        :show_hidden false
;;;;; Read only
        :read_only false
;;;;; Recover mode
        :enable_recover_mode false
;;;;; Start FIFO
        :start_fifo nil
;;;;; Hide remaps in help menu
        :hide_remaps_in_help_menu false
;;;;; Prompt
        :prompt
        {
         :format "❯ "
         :style
         {
          :add_modifiers nil
          :sub_modifiers nil
          :bg nil
          :fg nil
          }
         }
;;;;; Initial layout
        :initial_layout "default"
;;;;; Initial mode
        :initial_mode "default"
;;;;; Initial sorting
        :initial_sorting
        [
         { :sorter "ByCanonicalIsDir" :reverse true }
         { :sorter "ByIRelativePath" :reverse false }
         ]
;;;;; Logs
        :logs
        {
;;;;;;; Error
         :error
{
 :format "ERROR"
 :style
 {
  :add_modifiers nil
  :sub_modifiers nil
  :bg nil
  :fg "Red"
  }
 }
;;;;;;; Info
:info
{
 :format "INFO"
 :style
 {
  :add_modifiers nil
  :sub_modifiers nil
  :bg nil
  :fg "LightBlue"
  }
 }
;;;;;;; Success
:success
{
 :format "SUCCESS"
 :style
 {
  :add_modifiers nil
  :bg nil
  :fg "Green"
  :sub_modifiers nil
  }
 }
;;;;;;; Warning
:warning
{
 :format "WARNING"
 :style
 {
  :add_modifiers nil
  :bg nil
  :fg "Yellow"
  :sub_modifiers nil
  }
 }
}
;;;;; Default UI
        :default_ui
        {
         :prefix "  "
         :suffix ""
         :style defaultStyle
         }
;;;;; Focus UI
        :focus_ui
        {
         :prefix "▸["
         :suffix "]"
         :style
         {
          :add_modifiers [ "Bold" ]
          :sub_modifiers nil
          :bg nil
          :fg "Blue"
          }
         }
;;;;; Selection UI

        :selection_ui
        {
         :prefix " {"
         :suffix "}"
         :style
         {
          :add_modifiers [ "Bold" ]
          :sub_modifiers nil
          :bg nil
          :fg "LightGreen"
          }
         }
;;;;; Focus UI
        :focus_selection_ui
        {
         :prefix "▸["
         :suffix "]"
         :style
         {
          :add_modifiers ["Bold"]
          :sub_modifiers nil
          :bg nil
          :fg "LightGreen"
          }
         }
;;;;; Sort & filter UI
        :sort_and_filter_ui
        {
;;;;;;; Separator
         :separator
{
 :format " › "
 :style {
         :add_modifiers ["Dim"]
         :bg nil
         :fg nil
         :sub_modifiers nil
         }
 }
;;;;;;; Default identidier
:default_identifier
{
 :format nil
 :style
 {
  :add_modifiers ["Bold"]
  :bg nil
  :fg nil
  :sub_modifiers  nil
  }
 }

;;;;;;; Filter identifiers
:filter_identifiers
{
 :AbsolutePathDoesContain {:format "abs=~" :style defaultStyle}
 :AbsolutePathDoesEndWith {:format "abs=$" :style defaultStyle}
 :AbsolutePathDoesNotContain {:format "abs!~" :style defaultStyle}
 :AbsolutePathDoesNotEndWith {:format "abs!$" :style defaultStyle}
 :AbsolutePathDoesNotStartWith {:format "abs!^" :style defaultStyle}
 :AbsolutePathDoesStartWith {:format "abs=^" :style defaultStyle}
 :AbsolutePathIs {:format "abs==" :style defaultStyle}
 :AbsolutePathIsNot {:format :abs!= :style defaultStyle}
 :IAbsolutePathDoesContain {:format "[i]abs=~" :style defaultStyle}
 :IAbsolutePathDoesEndWith {:format "[i]abs=$" :style defaultStyle}
 :IAbsolutePathDoesNotContain {:format "[i]abs!~" :style defaultStyle}
 :IAbsolutePathDoesNotEndWith {:format "[i]abs!$" :style defaultStyle}
 :IAbsolutePathDoesNotStartWith {:format "[i]abs!^" :style defaultStyle}
 :IAbsolutePathDoesStartWith {:format "[i]abs=^" :style defaultStyle}
 :IAbsolutePathIs {:format "[i]abs==" :style defaultStyle}
 :IAbsolutePathIsNot {:format "[i]abs!=" :style defaultStyle}
 :IRelativePathDoesContain {:format "[i]rel=~" :style defaultStyle}
 :IRelativePathDoesEndWith {:format "[i]rel=$" :style defaultStyle}
 :IRelativePathDoesNotContain {:format "[i]rel!~" :style defaultStyle}
 :IRelativePathDoesNotEndWith {:format "[i]rel!$" :style defaultStyle}
 :IRelativePathDoesNotStartWith {:format "[i]rel!^" :style defaultStyle}
 :IRelativePathDoesStartWith {:format "[i]rel=^" :style defaultStyle}
 :IRelativePathIs {:format "[i]rel==" :style defaultStyle}
 :IRelativePathIsNot {:format "[i]rel!=" :style defaultStyle}
 :RelativePathDoesContain {:format "rel=~" :style defaultStyle}
 :RelativePathDoesEndWith {:format :rel=$ :style defaultStyle}
 :RelativePathDoesNotContain {:format "rel!~" :style defaultStyle}
 :RelativePathDoesNotEndWith {:format "rel!$" :style defaultStyle}
 :RelativePathDoesNotStartWith {:format "rel!^" :style defaultStyle}
 :RelativePathDoesStartWith {:format "rel=^" :style defaultStyle}
 :RelativePathIs {:format "rel==" :style defaultStyle}
 :RelativePathIsNot {:format "rel!=" :style defaultStyle}}


;;;;;;; Sort direction identifiers
:sort_direction_identifiers
{
 :forward
 {:format "↓"
  :style defaultStyle
  }
 :reverse
 {:format "↑"
  :style defaultStyle
  }
 }

;;;;;;; Sorter identifiers
:sorter_identifiers
{:ByCanonicalAbsolutePath {:format "[c]abs" :style defaultStyle}
 :ByCanonicalExtension {:format "[c]ext" :style defaultStyle}
 :ByCanonicalIsDir {:format "[c]dir" :style defaultStyle}
 :ByCanonicalIsFile {:format "[c]file" :style defaultStyle}
 :ByCanonicalIsReadonly {:format "[c]ro" :style defaultStyle}
 :ByCanonicalMimeEssence {:format "[c]mime" :style defaultStyle}
 :ByCanonicalSize {:format "[c]size" :style defaultStyle}
 :ByExtension {:format "ext" :style defaultStyle}
 :ByICanonicalAbsolutePath {:format "[ci]abs" :style defaultStyle}
 :ByIRelativePath {:format "[i]rel" :style defaultStyle}
 :ByISymlinkAbsolutePath {:format "[si]abs" :style defaultStyle}
 :ByIsBroken {:format "⨯" :style defaultStyle}
 :ByIsDir {:format "dir" :style defaultStyle}
 :ByIsFile {:format "file" :style defaultStyle}
 :ByIsReadonly {:format "ro" :style defaultStyle}
 :ByIsSymlink {:format "sym" :style defaultStyle}
 :ByMimeEssence {:format "mime" :style defaultStyle}
 :ByRelativePath {:format "rel" :style defaultStyle}
 :BySize {:format "size" :style defaultStyle}
 :BySymlinkAbsolutePath {:format "[s]abs" :style defaultStyle}
 :BySymlinkExtension {:format "[s]ext" :style defaultStyle}
 :BySymlinkIsDir {:format "[s]dir" :style defaultStyle}
 :BySymlinkIsFile {:format "[s]file" :style defaultStyle}
 :BySymlinkIsReadonly {:format "[s]ro" :style defaultStyle}
 :BySymlinkMimeEssence {:format "[s]mime" :style defaultStyle}
 :BySymlinkSize {:format "[s]size" :style defaultStyle}
 }
};;end sort_and_fileter_ui
;;;;; Panel UI
        :panel_ui
        {
;;;;;;; Default
         :default
{
 :borders [:Top :Right :Bottom :Left ]
 :style defaultStyle
 :title
 {
  :format nil
  :style defaultStyle
  }
 }
;;;;;;; Help menu

:help_menu
{
 :borders nil
 :style defaultStyle
 :title
 { :format nil
   :style defaultStyle
   }
 }
;;;;;;; Input & log

:input_and_logs
{
 :borders nil
 :style defaultStyle
 :title
 { :format nil
   :style defaultStyle
   }
 }

;;;;;;; Selection
:selection
{
 :borders nil
 :style defaultStyle
 :title
 { :format nil
   :style defaultStyle
   }
 }
;;;;;;; Sort and filter
:sort_and_filter
{
 :borders nil
 :style defaultStyle
 :title
 { :format nil
   :style defaultStyle
   }
 }

;;;;;;; Table
:table
{
 :borders nil
 :style defaultStyle
 :title
 { :format nil
   :style defaultStyle
   }
 }
}
;;;;; Table
        :table {
                :style defaultStyle
                :col_spacing  1
                :col_widths
                [ {:Percentage 10}
                  {:Percentage 50}
                  {:Percentage 10}
                  {:Percentage 10}
                  {:Percentage 20}
                  ]
;;;;;;; Header
                :header
                {:cols
                 [
                  {:format " index"
                   :style defaultStyle}
                  {:format "╭──── path"
                   :style defaultStyle}
                  {:format "permissions"
                   :style defaultStyle}
                  {:format "size"
                   :style defaultStyle}
                  {:format "type"
                   :style defaultStyle}
                  ]
                 :height 1
                 :style
                 {
                  :add_modifiers ["Bold"]
                  :sub_modifiers nil
                  :bg nil
                  :fg nil}
                 }

;;;;;;; Row
                :row
                {
                 :cols
                 [
                  {:format "builtin.fmt_general_table_row_cols_0" :style defaultStyle}
                  {:format "builtin.fmt_general_table_row_cols_1" :style defaultStyle}
                  {:format "builtin.fmt_general_table_row_cols_2" :style defaultStyle}
                  {:format "builtin.fmt_general_table_row_cols_3" :style defaultStyle}
                  {:format "builtin.fmt_general_table_row_cols_4" :style defaultStyle}
                  ]
                 :height 0
                 :style defaultStyle
                 }
;;;;;;; Tree
                :tree
                [
                 {:format "├─" :style defaultStyle}
                 {:format "├─" :style defaultStyle}
                 {:format "╰─" :style defaultStyle}
                 ]
                }})

(set xplr.config.general generalConfig)
