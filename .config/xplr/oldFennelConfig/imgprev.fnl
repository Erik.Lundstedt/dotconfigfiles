;;(local version "0.17.6")

(local xplr xplr)


(local fifopath "/tmp/xplr.fifo")
(local fifo
       {
        :start      {:StartFifo fifopath}
        :stop       "StopFifo"
        :create     {:BashExecSilently (.. "mkfifo " fifopath)}
        :destroy    {:BashExecSilently (.. "rm -f "  fifopath)}
        })

(local go-back
       {:help "back"
        :messages
        [
         fifo.stop
         fifo.destroy
         "PopMode"
         {:SwitchModeBuiltin "default"}
         ]
        }
       )

;;; :

(set xplr.config.modes.custom.imgprev
     {:name "open with"
      :key_bindings ;;xplr.config.modes.builtin.default.key_bindings
      {:on_key
       {
        :s
        {:help "send" :messages {:BashExecSilently (.. "/usr/bin/imv-open \"" fifopath "\" \"$XPLR_FOCUS_PATH\"&")}}
        :q go-back
        :escape go-back
        :ctrl-c go-back}
       }
      }
     )




(tset xplr.config.modes.builtin.default.key_bindings.on_key "i"
      {:help "preview images"
       :messages
       [
        {:SwitchModeCustom "imgprev"}
        fifo.create
        fifo.start
        ]
       }
      )
