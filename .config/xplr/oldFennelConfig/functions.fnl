;; You need to define the script version for compatibility check.
;; See https://github.com/sayanarijit/xplr/wiki/Upgrade-Guide.
;;
;; version = "1.0.0"
(local xplr xplr)
;;(local version "0.17.6")


;;; Function
;;;; Builtin
;;;;; Formaters
;;;;;; Format index column
(fn fmtIndexCol [m]
  "Format index column"
    (var r "")
    (if m.is_before_focus (set r (.. r " -"))
        (set r (.. r "  ")))
    (set r (.. r m.relative_index "│" m.index))
    r)
;;;;;; Format path column
(fn fmtPthCol [m]
  "Format path column"
    (var r (.. m.tree m.prefix))
    (if (= m.meta.icon nil) (set r (.. r ""))
        (set r (.. r m.meta.icon " ")))
    (set r (.. r m.relative_path))
    (when m.is_dir
      (set r (.. r "/")))
    (set r (.. r m.suffix " "))
    (when m.is_symlink
      (set r (.. r "-> "))
      (if m.is_broken (set r (.. r "×"))
          (do
            (set r (.. r m.symlink.absolute_path))
            (when m.symlink.is_dir
              (set r (.. r "/"))))))
    r)

;;;;;; Format permissions column
(fn fmtPermsCol [m]
"Format permissions column"
(let [no-color (os.getenv :NO_COLOR)]
    (fn green [x]
      (if (= no-color nil)
          (.. "\027[32m" x "\027[0m") x))

    (fn yellow [x]
      (if (= no-color nil)
          (.. "\027[33m" x "\027[0m") x))

    (fn red [x]
      (if (= no-color nil)
          (.. "\027[31m" x "\027[0m") x))

    (fn bit [x color cond]
      (if cond (color x) (color "-")))

    (local p m.permissions)
    (var r "")
    (set r (.. r (bit :r green p.user_read)))
    (set r (.. r (bit :w yellow p.user_write)))
    (if (and (= p.user_execute false)
             (= p.setuid false))
        (set r (.. r (bit "-" red p.user_execute)))
        (and (= p.user_execute true)
             (= p.setuid false))
        (set r (.. r (bit :x red p.user_execute)))
        (and (= p.user_execute false)
             (= p.setuid true))
        (set r (.. r (bit :S red p.user_execute)))
        (set r (.. r (bit :s red p.user_execute))))
    (set r (.. r (bit :r green p.group_read)))
    (set r (.. r (bit :w yellow p.group_write)))
    (if (and (= p.group_execute false)
             (= p.setuid false))
        (set r
             (.. r (bit "-" red p.group_execute)))
        (and (= p.group_execute true)
             (= p.setuid false))
        (set r (.. r (bit :x red p.group_execute)))
        (and (= p.group_execute false)
             (= p.setuid true))
        (set r (.. r (bit :S red p.group_execute)))
        (set r (.. r (bit :s red p.group_execute))))
    (set r (.. r (bit :r green p.other_read)))
    (set r (.. r (bit :w yellow p.other_write)))
    (if (and (= p.other_execute false)
             (= p.setuid false))
        (set r
             (.. r (bit "-" red p.other_execute)))
        (and (= p.other_execute true)
             (= p.setuid false))
        (set r (.. r (bit :x red p.other_execute)))
        (and (= p.other_execute false)
             (= p.setuid true))
        (set r (.. r (bit :T red p.other_execute)))
        (set r (.. r (bit :t red p.other_execute))))
    r))

;;;;;; Format size column
(fn fmtSizeCol [m]
 "Format size column"
  (if
   (not m.is_dir)
   m.human_size ""
   )
  )
;;;;;; Format mime column
(fn fmtMimeCol [m]
"Format mime column"
  (if
   (and m.is_symlink
        (not m.is_broken)
        )
   m.symlink.mime_essence
   m.mime_essence
   )
  )
;;; put the functions in the table
(set xplr.fn.builtin
     {
      :fmt_general_table_row_cols_0 fmtIndexCol
      :fmt_general_table_row_cols_1 fmtPthCol
      :fmt_general_table_row_cols_2 fmtPermsCol
      :fmt_general_table_row_cols_3 fmtSizeCol
      :fmt_general_table_row_cols_4 fmtMimeCol
      }
     )



;;;; Custom
(set xplr.fn.custom {})
