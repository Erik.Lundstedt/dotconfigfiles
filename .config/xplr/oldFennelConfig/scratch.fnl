local nuke = require("nuke")
nuke.setup(
   {
	  open = {
		 run_executables = true,
		 custom = {
			{mime_regex = "^image/.*", command = "imv-x11 {}"},
			{mime = "video/mp4", command = "mpv {}"},
			{mime_regex = ".*", command = "jaro {}"}
		 }
	  },
	  view = {
		 show_line_numbers = true
	  }
})
