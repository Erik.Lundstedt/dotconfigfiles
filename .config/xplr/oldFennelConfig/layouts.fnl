;;; You need to define the script version for compatibility check.
;;; See https://github.com/sayanarijit/xplr/wiki/Upgrade-Guide.
;;;
;;; version = "1.0.0"

;;(local version  "0.19.0")
;;(local version  "0.17.6")
(local xplr  xplr)

;;; Layouts
;;;; Builtin
(local builtinLayouts
       {
;;;;; Default
        :default
{
 :Horizontal
 {
  :config
  {:margin nil
   :horizontal_margin 0
   :vertical_margin 0
   :constraints
   [ {:Percentage 70} {:Percentage 30} ]
   }
  :splits
  [
   {:Vertical
    {:config
     {:margin 0
      :horizontal_margin nil
      :vertical_margin nil
      :constraints
      [ {:Length 3}
        {:Min 1}
        {:Length 3}]
      }
     :splits
     [
      :SortAndFilter
      :Table
      :InputAndLogs
      ]
     }
    }
   {:Vertical
    {:config
     {:margin 0
      :horizontal_margin nil
      :vertical_margin nil
      :constraints
      [
       {:Percentage 50}
       {:Percentage 50}
       ]}
     :splits
     [
      :Selection
      :HelpMenu
      ]
     }
    }
   ]
  }
 }
;;;;; No help
:no_help
{:Horizontal
 {:config
  {:margin nil
   :horizontal_margin nil
   :vertical_margin nil
   :constraints
   [
    {:Percentage 70}
    {:Percentage 30}]}
  :splits
  [
   {:Vertical
    {:config {:margin nil
              :horizontal_margin nil
              :vertical_margin nil
              :constraints
              [
               {:Length 3}
               {:Min 1}
               {:Length 3}]}
     :splits
     [
      :SortAndFilter
      :Table
      :InputAndLogs
      ]}}
   :Selection]}}
;;;;; No selection
:no_selection
{:Horizontal
 {:config {:margin nil
           :horizontal_margin nil
           :vertical_margin nil
           :constraints
           [ {:Percentage 70}
             {:Percentage 30}]}
  :splits
  [
   {:Vertical
    {:config
     {:margin nil
      :horizontal_margin nil
      :vertical_margin nil
      :constraints
      [
       {:Length 3}
       {:Min 1}
       {:Length 3}
       ]
      }
     :splits
     [
      :SortAndFilter
      :Table
      :InputAndLogs]}}
   :HelpMenu]}}
;;;;; No help, no selection
:no_help_no_selection
{:Vertical
 {:config
  {:margin nil
   :horizontal_margin nil
   :vertical_margin nil
   :constraints
   [ {:Length 3}
     {:Min 1}
     {:Length 3}
     ]
   }
  :splits
  [
   :SortAndFilter
   :Table
   :InputAndLogs
   ]
  }
 }

})
(set xplr.config.layouts.builtin builtinLayouts)
;;;; Custom

(fn makePercent [first]
  [
   {:Percentage first}
   {:Percentage (- 100 first)}
   ]
  )



(local customLayouts
       {
        :personal
        {
         :Horizontal
         {
          :config
          {:margin nil
           :horizontal_margin 0
           :vertical_margin 0
           :constraints (makePercent 70)
           }
          :splits
          [
           {:Vertical
            {:config
             {:margin 0
              :horizontal_margin nil
              :vertical_margin nil
              :constraints
              [ {:Length 2}
                {:Min 1}
                {:Length 3}]
              }
             :splits
             [
              :SortAndFilter
              :Table
              :InputAndLogs
              ]
             }
            }
           {:Vertical
            {:config
             {:margin 0
              :horizontal_margin nil
              :vertical_margin nil
              :constraints (makePercent 30)
              }
             :splits
             [
              :Selection
              :HelpMenu
              ]
             }
            }
           ]
          }}
        })


(set xplr.config.layouts.builtin.default (. customLayouts :personal))
