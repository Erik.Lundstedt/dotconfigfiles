;; You need to define the script version for compatibility check.
;; See https://github.com/sayanarijit/xplr/wiki/Upgrade-Guide.
;;; setup

;;(local version "0.19.0")

;;(local version "0.17.6")
(local xplr xplr)
;;; maybe I should try to use this, but its completely un-neded
(fn style [...]
  (local args ...)
  {
   :add_modifiers (or args.modifiers nil)
   :sub_modifiers (or args.removeModifiers nil)
   :fg (or args.fg nil)
   :bg (or args.bg nil)
   }
  )


(fn node [icon style]
  {
   :meta icon
   :style style
   }
  )



;;; Node types
;;;; Directory
(local nodetypes
       {
        :directory
        {
         :meta {:icon "ð"}
         :style {
         :add_modifiers [ "Bold" ]
         :sub_modifiers nil
         :bg nil
         :fg "Cyan"
         }
         }
;;;; File
        :file
        {
         :meta {:icon "ƒ"}
         :style {
         :add_modifiers nil
         :sub_modifiers nil
         :bg nil
         :fg nil
         }
         }
;;;; Symlink
        :symlink
        {
         :meta {:icon "§"}
         :style {
         :add_modifiers [ "Italic" ]
         :sub_modifiers nil
         :bg nil
         :fg "Magenta"
         }
}
;;;; Mime essence
:mime_essence {}

;;;; Extension
:extension {}

;;;; Special
:special {}
;;; finish
})
(set xplr.config.node_types nodetypes)
