local wezterm = require("wezterm")
local theme = require("theme")
local colours = theme.colours
local myTheme = theme.myTheme
local mycolors = theme.mycolors
local act = wezterm.action
local wezsuper = "CTRL|SHIFT"
local progs = {sshmenu = {label = "ssh-menu", args = {"sshmenu"}}, mount = {label = "mount", args = {"bashmount"}}, fennel = {label = "fennel", args = {"fennel"}}, xplr = {label = "xplr", args = {"xplr"}}, htop = {label = "htop", args = {"htop"}}}
local launchMenu = {progs.sshmenu, progs.mount, progs.fennel, progs.xplr, progs.htop}
local fontUsed = wezterm.font_with_fallback({"UbuntuMono Nerd Font", "Ubuntu Mono Ligaturized", "FiraCode Nerd Font", "JetBrains Mono"})
local visual_bell = {fade_in_function = "EaseIn", fade_in_duration_ms = 150, fade_out_function = "EaseOut", fade_out_duration_ms = 150, target = "CursorColor"}
local function ssh_domain(args)
  return {name = (args.name or args.hostname), remote_address = (args.hostname or args.ip or nil), username = (args.username or args.user or "erik")}
end
local function ip(last)
  return ("192.168.0." .. last)
end
local ssh_domains = {ssh_domain({name = "home-server", hostname = "server.local", ip = ip("101")})}
local unix_domains = {{name = "default"}, {name = "scratch"}, {name = "music"}, {name = "explorer"}, {name = "compilation"}, {name = "status"}}
local ncpamixer = {label = "volumectrl", args = {"ncpamixer"}}
local mopidy = {label = "music daemon", args = {"mopidy"}}
local musicplayer = {label = "musicplayer", args = {"ncmpcpp"}}
local cava = {label = "audio visualiser", args = {"cava"}}
local musicAction = {Multiple = {wezterm.action({SpawnCommandInNewTab = mopidy}), wezterm.action({SpawnCommandInNewTab = musicplayer}), wezterm.action({SpawnCommandInNewTab = cava})}}
local function keybindings()
  return {{mods = wezsuper, key = "PageUp", action = act.IncreaseFontSize}, {mods = wezsuper, key = "PageDown", action = act.DecreaseFontSize}, {mods = wezsuper, key = "LeftArrow", action = act.ActivateTabRelative(( - 1))}, {mods = wezsuper, key = "RightArrow", action = act.ActivateTabRelative(1)}, {mods = wezsuper, key = ">", action = act.ShowLauncherArgs({flags = "FUZZY|WORKSPACES"})}, {mods = "CTRL", key = "n", action = act.SwitchWorkspaceRelative(1)}, {mods = "CTRL", key = "p", action = act.SwitchWorkspaceRelative(( - 1))}, {key = "BrowserBack", action = act.ActivateTabRelative(( - 1))}, {key = "BrowserForward", action = act.ActivateTabRelative(1)}}
end
local bar = {}
bar.right = function(window, pane)
  local powerline = {arrow = {left = {solid = utf8.char(57522), normal = utf8.char(57523)}}}
  local cells = {}
  local info = {}
  local time = {date = wezterm.strftime(" %Y-%m-%d "), time = wezterm.strftime(" %H:%M ")}
  local function addcell(text, _1_)
    local _arg_2_ = _1_
    local bg = _arg_2_["bg"]
    local fg = _arg_2_["fg"]
    local index = 1
    table.insert(cells, {Foreground = {Color = (fg or mycolors.colour3)}})
    table.insert(cells, {Background = {Color = (bg or mycolors.colour2)}})
    return table.insert(cells, {Text = text})
  end
  local c = mycolors
  local t = myTheme
  local text_fg = c.colour4
  local palette = {{text = {fg = text_fg, bg = t.red}, arrow = {fg = t.red, bg = t.bg}}, {text = {fg = t.red, bg = t.green}, arrow = {fg = t.green, bg = t.red}}, {text = {fg = t.red, bg = t.yellow}, arrow = {fg = t.yellow, bg = t.green}}, {text = {fg = text_fg, bg = t.red}, arrow = {fg = t.red, bg = t.text}}, {text = {fg = text_fg, bg = t.green}, arrow = {fg = t.red, bg = t.green}}}
  local steps = {(" [" .. (window:active_workspace() or "!") .. "]"), (" [" .. (window:window_id() or "!") .. "] "), time.time, time.date}
  for k, value in ipairs(steps) do
    local v = palette[k]
    addcell(powerline.arrow.left.solid, v.arrow)
    addcell(value, v.text)
  end
  return window:set_right_status(wezterm.format(cells))
end
wezterm.on("update-right-status", bar.right)
local opacity = 0.8
local scrollbar = false
local exit = {close = "Close", hold = "Hold", closeOnClean = "CloseOnCleanExit"}
local keybinds = require("wezkeybinds")
local keys = keybinds.keys
for key, binding in ipairs(keybindings()) do
  table.insert(keys, binding)
end
local sessionizer = wezterm.plugin.require("https://github.com/mikkasendke/sessionizer.wezterm")
local config = wezterm.config_builder()
config.disable_default_key_bindings = true
config.keys = keys
config.launch_menu = launchMenu
config.visual_bell = visual_bell
config.audible_bell = "Disabled"
config.font = fontUsed
config.font_size = 16
config.adjust_window_size_when_changing_font_size = false
config.ssh_domains = ssh_domains
config.unix_domains = unix_domains
config.default_prog = {"/usr/bin/zsh"}
config.colors = colours
config.window_background_opacity = opacity
config.enable_scroll_bar = scrollbar
config.use_fancy_tab_bar = false
config.exit_behavior = exit.close
sessionizer.apply_to_config(config)
local function proj(name)
  return ("/home/erik/git/projects/lisp/fennel/" .. name)
end
sessionizer.config = {paths = {proj("term"), proj("fydra")}}
return config
