#!/usr/bin/env lua
pcall(require, "luarocks.loader")
local naughty=require("naughty")
local awful=require("awful")
local fennel = require("fennel")
fennel.path = fennel.path .. ";.config/awesome/?.fnl;.config/awesome/?/?.fnl"
--fennel.path = fennel.path .. ";/home/erik/botsdir/?/?.fnl"
table.insert(package.loaders or package.searchers, fennel.searcher)
--require("rc")
--local term = require("term")
	
--naughty.notification ({ title = "info", message = fennel.view(term)})


--awful.spawn.with_shell("~/.config/screenlayout/main.sh")



naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
        message = message
    }
end)



require("cfg")

