(local beautiful (require :beautiful))
(local awful (require :awful))
(local keybinds (require "keybinds"))
(local ruled (require :ruled))
(local ruler ruled.client)
(local naughty (require :naughty))

;;; default properties
(local default
	   {:rule {}
		:properties
		{:border_width beautiful.border_width
		 :border_color beautiful.border_normal
		 :focus awful.client.focus.filter
		 :titlebars_enabled false
		 :raise true
		 :keys keybinds.clientKeys
		 :buttons keybinds.clientButtons
		 :screen awful.screen.preferred
		 :placement (+ awful.placement.no_overlap
					   awful.placement.no_offscreen)
		 :marked false
		 :hungry false
		 :swallowed false
		 }})

;;; floating clients
(local floating
	   {:rule_any
		{:instance
		 ["DTA"
		  "copyq"
		  "bashrun2-run-dialog"
		  "pinentry"]
		 :class
		 ["bashrun2-run-dialog"
		  "Arandr"
		  "MessageWin"
		  "scratch"
		  "Wpa_gui"
		  "veromix"
		  "xtightvncviewer"
		  "Tor Browser"
		  "Xbindkeys_show"]
		 :name ["event tester"]
		 :role ["AlarmWindow"
				"ConfigManager"
				"bashrun2-run-dialog"
		  "pop-up"]}
		:properties
		{:floating true}})
;;; :
(local tmpFloat
	   {:rule_any {:class ["tmp"] :role ["tmp"] :name ["tmp"]}
        :properties
		{:floating true
         :titlebars_enabled false
         :skip_taskbar false
         :placement (+ awful.placement.no_overlap
                       awful.placement.no_offscreen
                       awful.placement.centered
                       ;;  awful.placement.top
                       )}})

(local fzf-menu
	   {:rule_any
		{:class ["fzf-menu"]
		 :instance ["fzf-menu"]}
		:properties
		{:floating true
		 :skip_taskbar false
		 :ontop true
		 :placement
		 (+
		  awful.placement.centered
		  awful.placement.top
		  ;;awful.placement.no_overlap
		  awful.placement.no_offscreen
		  )}})

(local startmenu
	   {:rule_any
		{:class ["startmenu"]
		 :role ["startmenu" ]
		 :name ["startmenu" ]}
		:properties
		{:floating true
		 :titlebars_enabled false
		 :skip_taskbar true
		 :placement
		  (+
		   awful.placement.no_overlap
		   ;;  awful.placement.centered
		   awful.placement.top_left
		   awful.placement.next_to_mouse
		   awful.placement.no_offscreen
		   )}})


(local logoutFrame
	   {:rule_any
		{:class ["logout"]
		 :role ["logout"]
		 :name ["logout"]}
		:properties
		{:floating true
		 :above true
		 :switch_to_tags true
		 :new_tag {:name "logout" :volatile true}
		 :placement
		 (+
;;		  awful.placement.no_overlap
		  awful.placement.no_offscreen
		  awful.placement.centered
		  ;;  awful.placement.top
		  )}})

(local kodi
	   {:rule_any {:class ["Kodi" "kodi"]}
		:properties
		{:floating false
		 :titlebars_enabled false
		 :above true
		 :switch_to_tags true
		 :fullscreen true
		 }})

(local fullscreen
	   {:rule_any {:class ["fullscreen" "Fullscreen"] :instance ["fullscreen" "Fullscreen"]}
		:properties
		{:floating false
		 :fullscreen true}})

;;:class neon-logout

(local games
	   {:rule_any
		{	:class	["farmingsimulator2022game.exe" "farmingsimulator2022game.exe"
					 "steam_app_526870" "steam_app_526870"
					 "steam_app_1248130"]
			:role	["farmingsimulator2022game.exe" "farmingsimulator2022game.exe"
					 "steam_app_526870" "steam_app_526870"
					 "steam_app_1248130"]}
		:properties
		{	:floating false
			:fullscreen true
			:ontop false
			:above false
			}})


;;; rules that makes a window/frame floating,ontop and above
(local floatRules
	   {:rule_any
		{:instance
		 ["float"
		  "qweb"
		  "batman"]
		 :class
		 ["float"
		  "tabbed"
		  "qweb"
		  "qtws"
		  "batman"]
		 :role
		 ["float"
		  "batman"]}
		:properties
		{:floating true
		 :ontop true
		 :above true
		 :titlebars_enabled false
		 :placement
		  (+
		   awful.placement.no_overlap
		   awful.placement.no_offscreen
		   awful.placement.centered
		 ;;  awful.placement.top
		   )}})


;;; scratchpad rules are different from simply floating
;;as they have placement set to the top of the screen instead of centered

(local scratchpadRules
	   {:rule
		{:class "scratch"}
		:properties
		{:floating true
		 :ontop true
		 :above true
		 :titlebars_enabled false
		 :placement
		 (+
		  awful.placement.no_overlap
		  awful.placement.no_offscreen
		  awful.placement.top
		  )}})
;;; no titlebars
(local noTitlebars
	   {:rule_any
		{:name
		 ["speedbar"
		  "bar"]
		 :class
		 ["emacs"
		  "steam"
		  "Steam"
		  "noDec"]}
		:properties
		{:titlebars_enabled false}})

(local center
	   {:rule_any
		{:class ["centered" "center"]
		 :role  ["centered" "center"]
		 :name	["centered" "center"]
		 }
		:properties
		{:placement
		 (+ awful.placement.centered
			awful.placement.no_offscreen)}})

(local devourer {:rule_any {:hungry true :swallowed true}})


(local hungry
	   {:rule_any {:class ["hungry"] :name ["hungry"]}
		:properties
		{:floating true
		 :hungry true
		 :titlebars_enabled true}
		}
	   
	   ;;(c:emit_signal "request::titlebars")
	   )
(local swallowed  {:rule_any {:class ["swallowed"]}})





;;; :
(ruler.connect_signal
 "request::rules"
 (fn []
   (local ruled (require :ruled))
   (local add-rule ruled.client.append_rule)
   (set default.id default)
   (add-rule default)
   (local windowrules
		  {:scratch scratchpadRules
		   :logout logoutFrame
		   : games
		   : kodi
		   : floating 
		   : floatRules
		   ;;: noTitlebars
		   : fullscreen 
		   : fzf-menu
		   : startmenu
		   : center
		   ;;: devourer
		   : swallowed
		   : hungry
		   :tmp tmpFloat
		   })
   ;;(set awful.rules.rules rules)
   (each [name rule (pairs windowrules)]
	 (set rule.id name)
	 (add-rule rule)
	 )

   ))
