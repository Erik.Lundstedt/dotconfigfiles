#!/usr/bin/env fennel
(local awful (require :awful))
(local tcat table.concat)
(local keys {:super "Mod4" :shift "Shift" :ctrl "Control"})
(local wallsDir "/home/erik/Desktop/wallpapers/")


(fn rnd [...]
  (math.randomseed (* (os.time) (math.random 1 1000)))
  (math.random ...))



;;(local randomimg (. favWalls.arch (rnd (# favWalls.arch))))

(local groups
	   {:cutegurl
		[(tcat [wallsDir "wp5568745-archlinux-anime-wallpapers.png"]);;cute
		 (tcat [wallsDir "wp2867758-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp5568831-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp11235342-arch-anime-girl-wallpapers.png"])
		 (tcat [wallsDir "wp5568744-arch-anime-girl-wallpapers.png"])
		 ]
		:archMultihead
		[;;randomimg
		 (tcat [wallsDir "wp2946681-wallpaper-arch-linux.jpg"]);;arch-right
		 (tcat [wallsDir "wp3195590-arch-wallpaper.png"])];;arch-left
		:dva
		[(tcat [wallsDir "dva-overwatch-sketch-artwork-ey-dark-1920x1080.jpg"])
		 (tcat [wallsDir "125157-dark.jpg"])
		 (tcat [wallsDir "919725-dark.jpg"])]
		:arch
		[
		 (tcat [wallsDir "wp11235342-arch-anime-girl-wallpapers.png"])
		 (tcat [wallsDir "wp2618254-arch-wallpaper.png"])
		 (tcat [wallsDir "wp5568745-archlinux-anime-wallpapers.png"]);;cute
		 (tcat [wallsDir "wp2867758-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp2867758-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp43363-arch-wallpaper.jpg "])
		 (tcat [wallsDir "wp5568743-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp5568744-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp5568831-archlinux-anime-wallpapers.png"])
		 (tcat [wallsDir "wp5568847-archlinux-anime-wallpapers.jpg"])
		 (tcat [wallsDir "wp6080159-anime-linux-wallpapers.jpg"])]})



;; ;;penguin-gurl


(local wallpapers
	   groups.cutegurl
	   ;;(require :alt)
	   ;;groups.alt
	   ;;groups.dva
	   ;;groups.archMultihead
	   )
;;        [
;; ;;        (. favWalls.arch (rnd (# favWalls.arch)))
;;         (. favWalls.arch 1)
;;         (. favWalls.arch 12)
;;         (. favWalls.arch 13)
;;         (. favWalls.dva 1)
;;         (. favWalls.dva 2)
;;         (. favWalls.dva 3)
;;         ]
;;        )


(fn term [{: role : title : cmd : args}]
  (local wezterm "wezterm start ")
  {:wezterm
   (.. wezterm		"--class "	role					" -- "  cmd (table.concat args " "))
   :kitty
   (.. "kitty "		"--class "	role " --title " title	" -1 "  cmd (table.concat args " "))
   :tym
   (.. "tym "		"--role "	role " --title " title	" -e '" cmd (table.concat args " ") "'")
   })
  ;;  (.. "kdialog --msgbox " cmd  "--title \"" (table.concat args " ") "\"" )





(local cfg
	   {;;-------------general---------------------
		:terminal "wezterm start"
		:term       (fn [role title cmd args]
					  (. (term {: role : title : cmd : args}) :tym));;for onscreen.fnl
		:editor     "nvim -l "
		:fm         "xplr "
		:irc        "catgirl "
		:news       "snownews "
		:nm         "nmtui "
		:getEvents  "xev "
		;;------------system management------------
		:pm         "parui "
		:top        "htop "
		;;--------------music----------------------
		:volume     "ncpamixer "
		:music      "ncmpcpp "
		:musicCtrl  "playerctl "
		:visualiser "cava "
		;;--------------utils----------------------
		:locker     "i3lock "
		})

(local defaults
		{:terminal		cfg.terminal
		 :terminal-alt	"tym "
		 :editor		(or (os.getenv :EDITOR) cfg.editor)
		 :cmdEditor		(.. "wezterm" " -e " "nvim")})


(local settings {})

(set settings.wallpapers wallpapers)
;;(set settings.favoriteWallpapers favWalls)
;;(set settings.favWalls favWalls)
(set settings.keys keys)
(set settings.defaults cfg)
(set settings.cfg cfg)



settings
