#!/home/erik/bin/awm
;;#!/usr/bin/env fennel
(local fennel (require :fennel))
(local awful (require :awful))
(local settings (require :settings))
(local gears (require :gears))
(local wibox (require :wibox))
(local defaults settings.defaults)
(local keys settings.keys)
(local modkey keys.super)
(local super keys.super)
(local shift keys.shift)
(local ctrl keys.ctrl)
(local naughty (require :naughty))


;;(or defaults.term "tym "))
(local editor defaults.editor)
(local append-global-mousebinds awful.mouse.append_global_mousebindings)
(local append-global-keybindings awful.keyboard.append_global_keybindings)
(local append-client-keybindings awful.keyboard.append_client_keybindings)
(local terminal (require :lib.terminal))
;;(when (not modal) (var modal nil) )
(set modal nil)
;;(naughty.notification {:timeout 200  :title  })

(local term {})
(lambda term.spawn [t {:title ?title :prog ?prog
				  :class ?class :role ?role
				  :width ?width :height ?height}]
  (let [trm
		(t.create
		 {:title ?title :prog ?prog
		  :class (or ?class ?role nil)
		  :width ?width :height ?height})]
	;;(print trm)
	(awful.spawn.with_shell trm)
	))

(local spawn
		{:simple awful.spawn
		 :single awful.spawn.single_instance
		 :raise awful.spawn.raise_or_spawn
		 :async awful.spawn.easy_async
		 :shell awful.spawn.with_shell
		 :once awful.spawn.once
		 }
	   )


(fn spawnAndLog [cmd name]
  (awful.spawn.easy_async
   cmd
   (fn [stdout stderr reason exit-code]
     (naughty.notify {:text (.. stdout)}))))

;;(local helpful (hotkeys-popup.widget.new))
;;(help:show_help)
;;(fn helpful.show-help []
;;  (helpful:show_help))

;; (local menu awful.menu)
;; (local menuitems
;;	   [
;;		["help" (fn [] helpful.show-help)]
;;		 ])
;; (each [key value (ipairs menuitems)]
;;   (menu:add value key)
;;   )


;;(fn keygrabber [{:stop-event stop_event :stop-key stop_key : }])
(local great {});;the oposite of, yet extending, awful
(fn great.key
  [{:mods modifiers
	:kbd key ;;:key key
	:on-press on_press :function on_press1 :fun on_press2 :func on_press3
	: desc : description : group : name} gr]
  (awful.key
   (or modifiers [modkey])
   key
   (or on_press on_press1 on_press2 on_press3)
   {:description  (or description desc "not finished yet") :group (or gr group "user")}
   )
  )
(local lay wibox.layout)
(local popup {})

(fn popup.widget [this title]


  (local widgets
		 {:title (wibox.widget.textbox title)
		  :keys (lay.fixed.vertical)})
  (set widgets.keys.spacing 3)

  (local akeys [[{:key "esc" :desc "quit"}
				 {:key "w" :desc ""
				  :key "n" :desc "toggle popup"}]])
  
  (each [k v (ipairs akeys)]
	(let [somelayout (lay.fixed.horizontal)]
	  (each [i j (ipairs v)]
		(let [tbox (wibox.widget.textbox (.. (. j :key) ":" (. j :desc) ))]
		  ;;(set tbox.forced_width 10)
		  ;;(set tbox.spacing 5)
		  (somelayout:add tbox)))
	  (set somelayout.spacing 3)
	  ;;(set somelayout.forced_width 10)
	  (widgets.keys:add somelayout)))

  (local wgt
		 {:border_width 1
		  :placement  awful.placement.centered 
		  :visible false
		  :ontop true
		  :opacity 0.8
		  :widget
		   (lay.fixed.vertical widgets.keys)})
  (set wgt.widget.forced_width 300)
  (set wgt.widget.forced_height 200)
  (set this.wgt (awful.popup wgt)))



(fn popup.show		[this] (set this.wgt.visible true))
(fn popup.hide		[this] (set this.wgt.visible false))
(fn popup.toggle	[this] (set this.wgt.visible (not this.wgt.visible)))


(set modal
	   (awful.keygrabber
		{:stop_event "press"
		 :stop_key "Escape"
		 :timeout 20 ;;(mins->secs 2)
		 :timeout_callback	(fn [] (naughty.notification {:text "keygrabber expired"}) (popup:hide))
		 :stop_callback		(fn [] (naughty.notification {:text "keygrabber stopped"}) (popup:hide))
		 :root_keybindings
		 [(great.key
		   {:mods [modkey] :kbd "a"
			:func (fn []
					(naughty.notification
					 {:title "started modal!"
					  :text "press escape to quit"})
					(popup:widget "modal")
					(popup:show)
					)})]
		 :keybindings
		 [(great.key
		   {:mods [] :kbd "w"
			:func
			(fn []
			  (naughty.notification
			   {:text "w was pressed"}))
			} "modal")
		  (great.key
		   {:mods [] :kbd "n"
			:func (fn [] (popup:toggle))
			:desc "toggle popup"
			} "modal")
		  (great.key
		   {:mods [] :kbd "d"
			:func (fn []
					(terminal.minimal:open {:class "float" :title "htop" :prog "htop "} awful.spawn.with_shell)
					;;(naughty.notification {:text "w was pressed"})
					(popup:hide)
				 )
			;;:desc ""
			} "modal")
		  ]}))

(each [k v (ipairs (. modal.keybindings))]
  (naughty.notification {:timeout 20 :text (.. (. v 1 :key) "\n" (or (?. v 1 :description) ""))}))







