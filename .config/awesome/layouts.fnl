#!/usr/bin/env fennel
(local layout (require "awful.layout.suit"))
;;(local sidestack (require "sidestack"))

(local layouts
[
 layout.tile
;; sidestack.left
;; sidestack
 layout.floating
 layout.tile.left
 layout.tile.bottom
 layout.tile.top
 layout.fair
 layout.fair.horizontal
 layout.spiral
 layout.spiral.dwindle
 layout.max
 layout.max.fullscreen
 layout.magnifier
 layout.corner.nw
 ]
)

layouts
