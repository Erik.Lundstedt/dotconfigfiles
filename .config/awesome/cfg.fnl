;;; cfg.fnl

(local gears (require :gears))
(local awful (require :awful))
(local wibox (require :wibox))
(local beautiful (require :beautiful))
(local naughty (require :naughty))
(local menubar (require :menubar))
(local hotkeys-popup (require :awful.hotkeys_popup))
(require :awful.hotkeys_popup.keys)
(require :awful.autofocus)
(local ruled (require :ruled))
(local rules (require :windowrules))
(local keybinds (require :keybinds))
(local settings (require :settings))
(local wallpaper (require :wallpaper))
(local titlebar (require :titlebar))
(local statusbar (require :statusbar))
(local bottombar (require :bottombar))
(local workspaces (require :workspaces))
(local layouts (require :layouts))
(local tbl (require :tableutils))
(require :keys)

((require :autostart))


(λ unless [check & body]
  (when (not check)
	(each [i v (ipairs body)]
	  (v))))


(when awesome.startup_errors
  (naughty.notification
   {:preset naughty.config.presets.critical
	:title "Oops, there were errors during startup!"
	:text awesome.startup_errors}))

;;(fn default-error [err] (when in-error (lua "return "))  (set in-error true) (naughty.notification {:preset naughty.config.presets.critical :title "Oops, an error happened!" :text (tostring err)})  (set in-error false))

(λ notify [{: title : text :preset ?preset}]
  (naughty.notification
   {
	:preset (or ?preset naughty.config.presets.critical)
	: title
	: text }))

(fn error-test [err]
  (notify {:title "an error happened"
		   :text (tostring err)}))

(do
  (var in-error false)
  (awesome.connect_signal
   "debug::error"
   ;;default-error
   error-test ))

;;(awesome.signal)
;;(beautiful.init "test.fnl")

(local theme (require :mytheme))
  (when (not (beautiful.init theme))
	(beautiful.init (.. (gears.filesystem.get_themes_dir) "default/theme.lua")))

;;(naughty.notify { :title "preset test" :text (tbl.dump naughty.config.presets.critical)  })

(local defaults settings.defaults)

(local keys settings.keys)
(local terminal defaults.terminal)
(local editor defaults.editor)
(local editor-cmd defaults.cmdEditor)

(local modkey keys.super)
(local super keys.super)
(local shiftkey keys.shift)
(local shift keys.shift)
(local ctrl  keys.ctrl)


;;(set awful.layout.layouts layouts)



(set menubar.utils.terminal terminal)


(local taglist-buttons keybinds.taglistButtons)
(local tasklist-buttons keybinds.tasklistButtons)



;;; signals


(awful.screen.connect_for_each_screen
 (fn [s]
   (workspaces.tags s)
   (set s.mypromptbox
		(awful.widget.prompt))
   
   (set s.mode-indicator
		(wibox.widget
		 {:markup "<b>[normal]</b>"
		  :halign "center"
		  :valign "center"
		  :widget awful.widget.textbox}))

   (set s.mylayoutbox
		(awful.widget.layoutbox s))
   (s.mylayoutbox:buttons keybinds.layoutBoxButtons)
   ;;(set s.userdata {:modes ["normal" "drop"] :mode 1})
   
   (set s.mytaglist (statusbar.tagList s ))

   ;; (set s.mytasklist
   ;;      (statusbar.taskList s tasklist-buttons)
   ;;      )

   (local spacing-widget
		  {:widget wibox.container.place
		   :valign "center" :halign "center"})
   (table.insert spacing-widget
				 {:widget wibox.widget.separator
				  :forced_width 10
				  :shape gears.shape.powerline
				  :shape_color theme.general_color4})
   (set s.mytasklist
		(awful.widget.tasklist
		 {:screen s
		  :filter awful.widget.tasklist.filter.currenttags
		  :buttons keybinds.tasklistButtons
		  :style {:shape_border_width 1
				  :shape_border_color theme.general_color6
				  :shape gears.shape.powerline}
		  :layout {:spacing 10
				   :spacing_widget spacing-widget
				   :layout wibox.layout.flex.horizontal}
		  :widget_template statusbar.tasklist }))

   (set s.mywibox (awful.wibar {:position "top" :screen s}))

   (set s.bottombar	(awful.wibar
					 {:position
					  "bottom"
					  :height 30
					  :opacity 75
					  :visible false
					  :screen s}))
   (s.bottombar:setup (bottombar.bottombar bottombar.sections))
   (s.mywibox:setup
	(statusbar.statusbar (statusbar.sections s))
	)

   ;;(set-wallpaper s)
   )
 )

;; (client.connect_signal
;;  "property::name"
;;  (fn  [c]
;;    ;;(naughty.notification {:timeout 20  :title (.. (if c.titlebars_enabled "true" "false")) })
;; 	 ;;(c:emit_signal "request::titlebars"))
;;    ))


(ruled.notification.connect_signal
 "request::rules"
 (fn []
   (ruled.notification.append_rule
	{:rule_any {:has_class ["vivaldi" "vivaldi-stable" "Vivaldi-Stable"]
				:has_instance ["vivaldi" "vivaldi-stable" "Vivaldi-Stable"]}
	 :properties {:append_actions [(naughty.action {:name "dismiss"})]
				  :bg "ff0000"}})
   (ruled.notification.append_rule {:properties {:timeout 15} :rule {}})))



(client.connect_signal
 :manage
 (fn [c]
   (when (and
		  (and awesome.startup (not c.size_hints.user_position))
		  (not c.size_hints.program_position))
	 (awful.placement.no_offscreen c))))





(client.connect_signal
 "request::titlebars"
 (fn [c]
   (let [tb (awful.titlebar c)
		 titleButtons
		 (gears.table.join
		  (awful.button {} 1 (fn [] (c:emit_signal "request::activate" :titlebar {:raise true}) (awful.mouse.client.move c)))
		  (awful.button {} 3 (fn [] (c:emit_signal "request::activate" :titlebar {:raise true}) (awful.mouse.client.resize c))))
		 ]
	 (tb:setup
	  (titlebar.widgetbuilder
	   {:inputTable {:layout wibox.layout.align.horizontal}
		:widgets
		[(titlebar.widgetbuilder (. (titlebar.sections titleButtons c) :left))
		 (titlebar.widgetbuilder (. (titlebar.sections titleButtons c) :center))
		 (titlebar.widgetbuilder (. (titlebar.sections titleButtons c) :right))
		 ]})))))



(awful.screen.connect_for_each_screen (fn [s] (wallpaper.auto s)))


(client.connect_signal
 "mouse::enter"
 (fn [c]
   (c:emit_signal
	"request::activate"
	:mouse_enter {:raise false})))

(client.connect_signal
 "focus"
 (fn [c] (set c.border_color beautiful.border_focus)))

(client.connect_signal
 "unfocus"
 (fn [c] (set c.border_color beautiful.border_normal)))
