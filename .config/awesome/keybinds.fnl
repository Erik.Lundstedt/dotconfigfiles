(local awful (require :awful))
(local naughty (require :naughty))
(local gears (require :gears))
(local hotkeys-popup (require "awful.hotkeys_popup"))
(require :awful.hotkeys_popup.keys)
;;(local onscreen (require :onscreen))

(local startmenu (require :startmenu))

(local settings (require :settings))
(local defaults settings.defaults)
(local terminal defaults.terminal)
(local editor defaults.editor)
(local editor-cmd defaults.cmdEditor)
(local keys settings.keys)
(local modkey keys.super)
(local super keys.super)
(local shiftkey keys.shift)
(local ctrl keys.ctrl)

(local categories {})


;; (awful.key [super] "f"
;;		   (fn [c]
;;			 (set c.fullscreen (not c.fullscreen))
;;			 (c:raise))
;;		   {:description "toggle fullscreen"
;;			:group "client"})




(local taglistButtons
       (gears.table.join
		(awful.button {} 1 (fn [t] (t:view_only)))
		(awful.button [super] 1 (fn [t] (when client.focus (client.focus:move_to_tag t))))
        (awful.button [super] 3 (fn [t] (when client.focus (client.focus:toggle_tag t))))
        (awful.button {} 3 awful.tag.viewtoggle)
        (awful.button {} 4 (fn [t] (awful.tag.viewnext t.screen)))
        (awful.button {} 5 (fn [t] (awful.tag.viewprev t.screen)))))


(local tasklistButtons
	   (gears.table.join
		(awful.button
		 {} 1
		 (fn [c]
		   (if (= c client.focus)
			   (set c.minimized true)
			   (c:emit_signal "request::activate"
							  :tasklist
							  {:raise true}))))
		(awful.button {} 3
					  (fn []
						(awful.menu.client_list {:theme {:width 256}})))
		(awful.button {} 4
					  (fn []
						(awful.client.focus.byidx 1)))
		(awful.button {} 5
					  (fn []
						(awful.client.focus.byidx (- 1))))))

;;; clientButtons
(fn clientRaise [c] (c:emit_signal "request::activate" "mouse_click" {:raise true}))

(local clientButtons
	   (gears.table.join
		(awful.button []      1 (fn [c] (clientRaise c)))
		(awful.button [super] 1 (fn [c] (clientRaise c) (awful.mouse.client.move c)))
		(awful.button [super] 3 (fn [c] (clientRaise c) (awful.mouse.client.resize c)))))

;;; layoutbox-button-keybinds-thingy
;; runs for each screen, has to do with the statusbar
(local layoutboxButtons
	(gears.table.join
	 (awful.button {} 1 (fn [] (awful.layout.inc 1)))
	 (awful.button {} 3 (fn [] (awful.layout.inc (- 1))))
	 (awful.button {} 4 (fn [] (awful.layout.inc 1)))
	 (awful.button {} 5 (fn [] (awful.layout.inc (- 1))))))






;;music.player
;;music.vis







;;; return
{
 :taglistButtons	taglistButtons
 :tasklistButtons	tasklistButtons
 :clientButtons		clientButtons
 :layoutBoxButtons	layoutboxButtons
 }
