#!/usr/bin/env fennel
(local awful (require "awful"))


(fn icon [iconName]
  (.. "/home/erik/.config/icons/" iconName ".svg")
  )

(local defaultLayout awful.layout.suit.tile)

(fn add-tag [name tag] (awful.tag.add name tag))

(fn tags [screen]
  (add-tag
   :home
   {:icon (icon :aperture)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :games
   {:icon (icon :bookmark)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :terminal
   {:icon (icon :terminal)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :filemanager
   {:icon (icon :folder)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :webbrowser
   {:icon (icon :chrome)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :media
   {:icon (icon :music)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :editor
   {:icon (icon :file-text)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :programming
   {:icon (icon :code)
	:layout defaultLayout
	:screen screen
	:selected false})

  (add-tag
   :ide
   	{:icon (icon :sidebar)
	:layout defaultLayout
	:master_width_factor (/ 1.0 5.0)
	:screen screen
	:selected true}))



{:tags tags}
