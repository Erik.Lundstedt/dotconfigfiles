#!/usr/bin/env fennel
(local awful (require :awful))
(local gears (require :gears))
(local naughty (require :naughty))
(local settings (require :settings))
