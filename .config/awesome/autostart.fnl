#!/usr/bin/env fennel
(local gears (require :gears))
(local awful (require :awful))
(local naughty (require :naughty))
(local utils (require :utils))



(local spawn
	   ;;utils.spawn
	   
		{:simple awful.spawn
		 :single awful.spawn.single_instance
		 :raise awful.spawn.raise_or_spawn
		 :async awful.spawn.easy_async
		 :shell awful.spawn.with_shell
		 :once awful.spawn.once
		 }
	   )


(fn spawnAndLog [cmd name]
  (awful.spawn.easy_async
   cmd
   (fn [stdout stderr reason exit-code]
     (naughty.notify {:text (.. stdout)}))))


(fn autostart []
  (local autorun true)
  (local autorun-apps
		 [;;"abduco -An 'xbindkeys -fg ~/.config/xbindkeys/xbindkeysrc.scm'"
		  "keepassxc"
		  "mopidy"
		  "ckb-next -b -p base"
		  "antimicrox --tray"
		  ])

  (when autorun
	(awful.spawn.with_shell (.. (gears.filesystem.get_configuration_dir)
								"autostart.sh"))

	(each [k app (ipairs autorun-apps)]
	  (spawn.simple app)))

  ;;(fn startapps []


  (local tagOrder utils.tagOrder)
  

  (fn getHostname []
	"gets system hostname by running the
hostname program and parsing its output
https://gist.github.com/h1k3r/089d43771bdf811eefe8
"
	(local prog (io.popen "/bin/hostname"))
	(local tmphostname (or (prog:read "*a") "err"))
	(prog:close)
	(local hostname (string.gsub tmphostname "\n$" ""))
	hostname)

  ;; (local apps
  ;; 		 [
		  ;;(spawn.single "tym --role float "          {:tag tagOrder.term})
		  ;;(spawn.single "tym --role float -e fennel" {:tag tagOrder.games})
		  ;;(spawn.single "tym --role float -e xplr"   {:tag tagOrder.fm})
		  ;;(spawn.once  "luakit"                      {:tag tagOrder.www})
		  (spawn.shell "~/.config/screenlayout/main.sh")
		  (spawn.once "tym --daemon")
		  ;;(spawn.shell "xbindkeys -fg ~/.config/xbindkeys/xbindkeysrc.scm" )
		  (spawn.shell "xbindkeys -fg ~/.config/xbindkeys/xbindkeysrc.scm -n")
		  (spawn.shell "picom")
		  ;; (spawn.once "wezterm start --domain=scratch")
		  ;;(spawn.once "emacsclient --alternate-editor=emacs" {:tag tagOrder.ide})
		  ;;(spawn.once "rofi-polkit-agent" )
		  ;;])
;;(each [key value (ipairs apps)] (value))
)
