(local awful (require "awful"))
(local wibox (require "wibox"))
(local gears (require "gears"))
;;(local widgets (require "widgets"))
(local theme (require "mytheme"))
(local tbl (require "tableutils"))
(local naughty (require "naughty"))
(local keybinds (require "keybinds"))
(local beautiful (require "beautiful"))
(local terminal (require :lib.terminal))

(local hotkeys-popup (require "awful.hotkeys_popup"))
(require :awful.hotkeys_popup.keys)
(local tcat table.concat)


;;(naughty.notification {:timeout 200  :title  })

(local term {})
(lambda term.spawn [t {:title ?title :prog ?prog
				  :class ?class :role ?role
				  :width ?width :height ?height}]
  (let [trm
		(t.create
		 {:title ?title :prog ?prog
		  :class (or ?class ?role nil)
		  :width ?width :height ?height})]
	;;(print trm)
	(awful.spawn.with_shell trm)
	))
;;; utility functions

(fn wiboxBuilder [layout items]
  (local retval {: layout})
  (each [key value (ipairs items)]
    (table.insert retval value))
  retval)


;;; set of "parent" widget-tables

;;;; tag-widgets


(local tagWidgets
       {:icon
		(tbl.combine
		 [{:id "icon_role" :widget wibox.widget.imagebox}]
         {:margins 4 :widget wibox.container.margin})
        :index
		(tbl.combine
		 [(tbl.combine 
           [{:id :index_role
             :widget wibox.widget.textbox
             ;;:visible false
             }]
           {:margins 2
            :widget wibox.container.margin})]
         {:bg theme.general_background
          :shape gears.shape.circle
          :widget wibox.container.background})})


;;;; task-widgets;;;UNUSED FOR NOW
(local taskWidgets
       {:icon
		(gears.table.join
		 [{:id "icon_role" :widget wibox.widget.imagebox}]
         {:margins 2 :widget wibox.container.margin})
        :title {:id "text_role" :widget wibox.widget.textbox}})



;;; widgets table




;;;; tag-widget (. widgets :tag)
(local widgets
	   {:tag
		{1
		 {1
		  {1 tagWidgets.index
		   2 tagWidgets.icon
		   :layout wibox.layout.fixed.horizontal}
		  :left 16
		  :right 16
		  :widget wibox.container.margin}
		 :id "background_role"
		 :widget wibox.container.background
		 :create_callback
		 (fn [self c3 index objects]
		   (self:connect_signal
			"mouse::enter"
			(fn []
			  (when (not= self.bg theme.general_color5)
				(set self.backup self.bg)
				(set self.has_backup true))
			  (set self.bg theme.general_color5)))
		   (self:connect_signal
			"mouse::leave"
			(fn []
			  (when self.has_backup
				(set self.bg self.backup)))))}})





;;; taglist

(fn tagList [screen]
  (local taglist-buttons keybinds.taglistButtons)
	 (awful.widget.taglist
	  {:screen screen
	   :filter awful.widget.taglist.filter.all
	   :style {:shape gears.shape.powerline}
	   :layout
	   {:spacing 16
		:spacing_widget
		{:color theme.general_color5
		 :border_color theme.general_color6
		 :border_width 1
		 :forced_width 5
		 :shape gears.shape.powerline
		 :widget wibox.widget.separator}
		:layout wibox.layout.fixed.horizontal}
	   :widget_template widgets.tag
	   :buttons taglist-buttons}))

;;; tasklist


;;;; :



(local tasklist
	   (gears.table.join
		[(gears.table.join
		  [(gears.table.join
			[taskWidgets.icon
			 taskWidgets.title]
			{:layout wibox.layout.fixed.horizontal})]
		  {:left 10 :right 10 :widget wibox.container.margin})]
		{:id "background_role" :widget wibox.container.background}))




;;; standard widgets


(global mykeyboardlayout (awful.widget.keyboardlayout))
(global mytextclock (wibox.widget.textclock))


;;; sections



(fn sections [s]
  (fn icon [iconName]
	(.. "/home/erik/.config/icons/" iconName ".svg"))

  (fn myButtonBuilder []
	(local
	 mybutton
	 (awful.widget.button
	  { :image (icon "bookmark") }))
	(mybutton:buttons
	 (gears.table.join
	  (mybutton:buttons)
	  (awful.button
	   {:button 1
		:on_release
		(fn []
		  ;;(local bottombar s.bottombar) (set bottombar.visible (not bottombar.visible))
		  ;;(local help (hotkeys-popup.widget.new))
		  ;;(help:show_help)
		  )})
	  )) mybutton)
  
  ;;(terminal.minimal:open {:class "batman" :title "^*^ batman ^*^" :prog (.. "batman " input)} awful.spawn.with_shell)
  (local powerwidget
		 (awful.widget.launcher
		  {:image (icon :aperture)
           ;;:menu mymainmenu
           :command "./home/erik/botsdir/bin/term --term tym --class logout --width 16 --height 8 --title 'exit' -e fzf-power"}))
  (lambda text-wgt [{:halign ?halign :valign ?valign :text ?text :markup ?markup}]
	(wibox.widget
	 {:markup (or ?markup ?text {})
	  :halign (or ?halign :center)
	  :valign (or ?valign :center)
	  :widget wibox.widget.textbox}))
  
  (local infotext (text-wgt {:markup (tcat ["<b>" "abcdefg" "</b>"])}))


  
  (local mylauncher
		 (awful.widget.launcher
		  {:image beautiful.awesome_icon
		   ;;:menu mymainmenu
		   :command "startmenu show"}))
  
  (local statbar
		 {:left
		  [mylauncher
		   ;;infotext
		   s.mode-indicator
		   s.mytaglist
		   s.mypromptbox]
		  :center s.mytasklist
		  :right
		  [;;         mykeyboardlayout
		   (wibox.widget.systray)
		   mytextclock
		   powerwidget
		   s.mylayoutbox]})
  statbar)



;;; statusbar
(fn statusbar [sections]
  (wiboxBuilder
   wibox.layout.align.horizontal
   [(wiboxBuilder
	 wibox.layout.fixed.horizontal
	 sections.left)
	sections.center
	(wiboxBuilder
	 wibox.layout.fixed.horizontal
	 sections.right)]))


;;; return
{:statusbar statusbar
 :sections sections
 :widgets widgets
 :tagList tagList
 :tasklist tasklist}
