(local gears (require :gears))
(local beautiful (require :beautiful))
(local naughty (require :naughty))
(local theme (require :mytheme))

(local wibox (require :wibox))
(local awful (require :awful))

(local scr (awful.screen.focused))

;;(local container (awful.widget.only_on_screen))

(local wgt {})
(local layout {})

