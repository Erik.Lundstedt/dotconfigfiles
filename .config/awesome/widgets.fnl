#!/usr/bin/env fennel
(local awful (require "awful"))
(local wibox (require "wibox"))
(local gears (require "gears"))
(local theme (require "mytheme"))
(local tbl (require "tableutils"))
(local naughty (require "naughty"))
(local keybinds (require "keybinds"))
(local beautiful (require "beautiful"))

(local music-ctrl "playerctl ")



(fn wiboxBuilder [layout items]
  (local retval
		 {
		  :layout layout
		  }
		 )

  (each [key value (ipairs items)]
	(table.insert retval value)
	)
  retval
  )


(fn notify [text]
  (naughty.notify
   {
	:text text
	}
   )
  )


  (fn icon [iconName]
	(.. "/home/erik/.config/icons/" iconName ".svg")
	)




 (fn music-control  [cmd]
   (.. music-ctrl " " cmd))

(local musicControl
	   {
		:playpause (.. music-ctrl "play-pause")
		:previous  (.. music-ctrl "previus")
		:next      (.. music-ctrl " next")
		:status    (.. music-ctrl " status")
		}
	   )

(local previus-widget
		 (awful.widget.launcher
		  {
		   :image (icon "skip-back")
		   ;;:menu mymainmenu
		   :command musicControl.previous
		   }
		  )
		)

(local separator-widget-play
		 (awful.widget.launcher
		  {
		   :image (icon "play")
		   ;;:menu mymainmenu
		   :command musicControl.playpause
		   }
		  )
		)




;; (separator-widget-play:connect_signal
;;  "button::release"
;;  (fn [_ _ _ _ button]
;;    (local widg button.widget)
;;    (set widg.image (icon "pause"))
;;    )
;;  )


(local separator-widget-pause
		 (awful.widget.launcher
		  {
		   :image (icon "pause")
		   ;;:menu mymainmenu
		   :command musicControl.playpause
		   }
		  )
		)
(local nextwidget
		 (awful.widget.launcher
		  {
		   :image (icon "skip-forward")
		   ;;:menu mymainmenu
		   :command musicControl.next
		   }
		  )
		)



{
 :play separator-widget-play
 :pause separator-widget-pause
 :prev previus-widget
 :next nextwidget
 :title (awful.widget.watch "bash -c \"playerctl metadata xesam:title\"" 30)
 :artist (awful.widget.watch "bash -c \"playerctl metadata xesam:artist\"" 30)
 }
