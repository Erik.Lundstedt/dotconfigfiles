(local awful (require :awful))
(local beautiful (require :beautiful))
(local wibox (require :wibox))


(fn titleBarWidgetBuilder [args]
  (local retval args.inputTable)
  (each [key value (ipairs (. args :widgets))]
	(table.insert retval value))
  retval)


(fn sections [buttons c]
  (local left
		 {:inputTable
		  {: buttons
		   :layout wibox.layout.fixed.horizontal}
		  :widgets [(awful.titlebar.widget.iconwidget c)
					{:widget (awful.titlebar.widget.titlewidget c) :halign :right}
					]})

  (fn bol->str [b t f]
  (match b
   true  "true"
   false "false"
   "unknown" "unknown"
   _ " "))

  (local infowidget (wibox.widget
					 {:text ""
					  :halign "right"
					  :widget wibox.widget.textbox
					  }))

 ;; (wibox.widget.textbox (fn [c]
 ;;							 "info"
 ;;							 ) true
  ;;                       )

  (when c
	  (let [text []]
		(if c.swallowed (table.insert text :swallowed) (table.insert text "not swallowed"))
		(if c.hungry (table.insert text :hungry) (table.insert text "not hungry"))
		;;(when c.leader_window	(table.insert text (.. "leader: " c.leader_window " ")))
		;;(when c.group_window	(table.insert text (.. "group: " c.group_window " ")))
		;;(when c.type	(table.insert text (.. "type: " c.type " ")))
		(set infowidget.text	(table.concat text "  "))))

  (local center
		 {:inputTable
		  {: buttons
		   :layout wibox.layout.flex.horizontal}
		  :widgets
		  [;;{:widget (awful.titlebar.widget.titlewidget c) :halign :center}
		   {:widget infowidget :halign :center}
		   ]})

  (local right {:inputTable {:layout (wibox.layout.fixed.horizontal)}
				:widgets
				[(awful.titlebar.widget.floatingbutton c)
				 (awful.titlebar.widget.maximizedbutton c)
				 (awful.titlebar.widget.stickybutton c)
				 (awful.titlebar.widget.ontopbutton c)
				 (awful.titlebar.widget.closebutton c)]})
{: left : center : right}
  )





{
 :widgetbuilder titleBarWidgetBuilder
 :sections sections
 }
