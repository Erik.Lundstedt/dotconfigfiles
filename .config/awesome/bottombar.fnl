#!/usr/bin/env fennel
(local awful (require "awful"))
(local wibox (require "wibox"))
(local gears (require "gears"))
(local widgets (require "widgets"))
(local theme (require "mytheme"))
(local tbl (require "tableutils"))
(local naughty (require "naughty"))
(local keybinds (require "keybinds"))
(local beautiful (require "beautiful"))
(local lay wibox.layout)
(local spawn
	   {:simple awful.spawn
		:single awful.spawn.single_instance
		:raise awful.spawn.raise_or_spawn
		:async awful.spawn.easy_async
		:shell awful.spawn.with_shell
		:once awful.spawn.once
		})

(fn findIcon [iconName] (.. "/home/erik/.config/icons/" iconName ".svg"))


;;; utility functions

(fn wiboxBuilder [layout items]
(local retval {: layout})


  (each [key value (ipairs items)]
	(table.insert retval value)
	)
  retval
  )

(fn textWidget [text]
  (wibox.widget
   {:text text
	:align :center
	:valign :center
	:widget wibox.widget.textbox}))


;;(local sections {:left [(textWidget "now playing: ") widgets.title (textWidget "by:") widgets.artist] :center [widgets.prev widgets.play widgets.next] :right [(textWidget "right 1") (textWidget "right 2") (textWidget "right 3")]})


(fn applauncher [{: prog : icon}] (awful.widget.launcher {:command prog :image (findIcon icon)}))

;;(local powerwidget (awful.widget.launcher {:image (icon :aperture)     ;;:menu mymainmenu    :command :neon-logout}))


(local sections
	   {:left
		[(wibox.widget.separator {:visible false})
		 (wibox.widget.separator {:visible false})
		 ]
		:center [
				 (applauncher {:prog "tym"		:icon "terminal"})
				 (applauncher {:prog "wezterm"	:icon "code"})
				 (applauncher {:prog "vivaldi"	:icon "aperture"})
				 (applauncher {:prog "tym --role 'float' -e 'nvim ~/.config'" :icon "file-text"})
				 (applauncher {:prog "tym --role 'float' -e 'nmtui'" :icon "wifi"})
				 (applauncher {:prog "tym --role 'float' -e 'xplr'" :icon "folder"})
				 (applauncher {:prog "/home/erik/.config/openbox/scripts/rofi-powermenu" :icon "power"})
				 ]
		:right [
				(wibox.widget.separator {:visible false})
				(wibox.widget.separator {:visible false})
				]})


;;; statusbar
(fn bottombar [sections]
  (wiboxBuilder
   lay.flex.horizontal [
	(wiboxBuilder lay.align.horizontal sections.left)	
	(wiboxBuilder lay.flex.horizontal sections.center)
	(wiboxBuilder lay.align.horizontal sections.right)
	]))


{:bottombar bottombar
 :sections  sections}
