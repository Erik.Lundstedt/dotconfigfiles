
(local awful (require :awful))
(local hotkeys-popup (require :awful.hotkeys_popup))
(require :awful.hotkeys_popup.keys)
(local menubar (require :menubar))
(local naughty (require :naughty))
(local settings (require :settings))
(local defaults settings.defaults)
(local keys settings.keys)
(local modkey keys.super)
(local super keys.super)
(local shiftkey keys.shift)
(local shift keys.shift)
(local ctrl keys.ctrl)
;;(or defaults.term "tym "))
(local editor defaults.editor)
(local append-global-mousebinds awful.mouse.append_global_mousebindings)
(local append-global-keybindings awful.keyboard.append_global_keybindings)
(local append-client-keybindings awful.keyboard.append_client_keybindings)
(local terminal (require :lib.terminal))
(local term (require :lib.term))

;;(require :modal-launch)

;; (lambda term.spawn [t {:title ?title :prog ?prog
;; 				  :class ?class :name ?name
;; 				  :width ?width :height ?height}]
;;   (let [trm
;; 		(t.create
;; 		 {:title ?title :prog ?prog
;; 		  :class ?class :name ?name
;; 		  :width ?width :height ?height})]
;; 	;;(print trm)
;; 	(awful.spawn.with_shell trm)
;; 	))


;;(awful.spawn.with_shell "st -c float -e man awesome")

(local helpful (hotkeys-popup.widget.new))
;;(help:show_help)
(fn helpful.show-help []
  (helpful:show_help))

;; (local menu awful.menu)
;; (local menuitems
;;	   [
;;		["help" (fn [] helpful.show-help)]
;;		 ])
;; (each [key value (ipairs menuitems)]
;;   (menu:add value key)
;;   )


;;(fn keygrabber [{:stop-event stop_event :stop-key stop_key : }])
(local great {});;the oposite of, yet extending, awful
(fn great.key
  [{:mods modifiers
	:kbd key ;;:key key
	:on-press on_press :function on_press1 :fun on_press2
	: desc : description : group : name} gr]
  
  ;; (awful.key
  ;;  {:modifiers (or modifiers [modkey])
  ;; 	:key (or key1 key)
  ;; 	:on_press (or on_press on_press1 on_press2)
  ;; 	:description (or description desc "not finished yet")
  ;; 	:group (or gr group "user")
  ;; 	: name
  ;; 	})
  (awful.key
   (or modifiers [modkey])
   key
   (or on_press on_press1 on_press2)
   {:description  (or description desc "not finished yet") :group (or gr group "user")}
   )
  )
  
;;; mouse-bindings
(append-global-mousebinds
 [(awful.button {} 3 (fn [] (helpful.show-help)))
  (awful.button {} 4 awful.tag.viewprev)
  (awful.button {} 5 awful.tag.viewnext)])

;;(local fennel (require :fennel))

(append-global-keybindings
 [(awful.key [modkey] :s
			 (fn [] (helpful.show-help))
			 {:description "show help"
			  :group :awesome})
  (awful.key [modkey] :b
			 (fn [] (each [scr screen] (set scr.bottombar.visible (not scr.bottombar.visible))))
			 {:description "show main menu"
			  :group :awesome})
 (awful.key [modkey] :w
			 (fn [] (each [scr screen] (screen.emit_signal "request::wallpaper" scr)))
			 {:description "reload wallpapers"
			  :group :awesome})
  (awful.key [modkey ctrl] :r
			 awesome.restart
			 {:description "reload awesome"
			  :group :awesome})
  (awful.key [modkey shift] :q
			 awesome.quit
			 {:description "quit awesome"
			  :group :awesome})
  ;; (awful.key [modkey] :x
  ;; 			 (fn []
  ;; 			   (awful.prompt.run
  ;; 				{:exe_callback awful.util.eval
  ;; 				 :history_path (.. (awful.util.get_cache_dir) :/history_eval)
  ;; 				 :prompt "Run Lua code: "
  ;; 				 :textbox (. (. (awful.screen.focused) :mypromptbox) :widget)}))
  ;; 			 {:description "lua execute prompt"
  ;; 			  :group :awesome})

  ;; (awful.key [modkey shift] :x
  ;;   		 (fn []
  ;; 			   (awful.spawn.with_shell "/home/erik/run.sh" )
  ;; 			   (local cmd
  ;; 			     	  (terminal.alacritty:open
  ;; 			     	   {:class "float"
  ;; 			     	   	:width 58
  ;; 			     	   	:height 12
  ;; 			     		:keep true
  ;; 			     	   	:prog "fydra"}))
  ;; 			   (local err (awful.spawn.with_shell cmd))
  ;; 			   (naughty.notification {:title (.. (tostring err) cmd)})
  ;; 			   )
  ;;   		 {:description "open fydra root"
  ;;   		  :group :awesome})

  
  (awful.key [modkey] :y
			 (fn []
			   (awful.prompt.run
				{:exe_callback
				 (fn [input]
				   ;;(naughty.notification {:timeout 200  :title (terminal.minimal:create {:class "batman" :title "batman" :prog "batman less"})})
					;;(.. "alacritty --class batman --title \"* 🦇 *BATMAN* 🦇 *\" -e batman " input )
					(awful.spawn.with_shell (terminal.minimal:open {:class "batman" :title "^*^ batman ^*^" :prog (.. "batman " input)}))
					)
				 :prompt "open man-page: "
				 :history_path (.. (awful.util.get_cache_dir) :/history_man)
				 :textbox (. (. (awful.screen.focused) :mypromptbox) :widget)}))
			 {:description "open a man-page"
			  :group :awesome})

  (awful.key [modkey ctrl] :o
			 (fn []
			   (awful.prompt.run
				{:exe_callback
				 (fn [input] (awful.spawn.with_shell (.. "jaro " input)))
				 :prompt "open with jaro: "
				 :history_path (.. (awful.util.get_cache_dir) :/history_opener)
				 :textbox (. (. (awful.screen.focused) :mypromptbox) :widget)}))
			 {:description "open with jaro"
			  :group :awesome})
  (awful.key [modkey shift] :o
			 (fn []
			   (awful.prompt.run
				{:exe_callback
				 (fn [input] (terminal.minimal:open {:class "float" :prog (.. "jaro " input)} awful.spawn.with_shell))
				 :prompt "open with jaro: "
				 :history_path (.. (awful.util.get_cache_dir) :/history_opener)
				 :textbox (. (. (awful.screen.focused) :mypromptbox) :widget)}))
			 {:description "open with jaro(in terminal)"
			  :group :awesome})
  
  ;; (great.key {:kbd :a					;time is in ms
  ;; 			  :on-press (fn [] (awful.spawn.with_shell "xwinclone -raisetime 2000"))
  ;; 			  :description "xwinclone a window"
  ;; 			  })
  

  
  (awful.key [modkey] :Return
			 (fn [] (awful.spawn.with_shell (terminal.default:create {})))
			 {:description "open a terminal"
			  :group :launcher})
  (awful.key [modkey shift] :Return (fn [] (awful.spawn.with_shell (terminal.minimal:create {})))
			 {:description "open a terminal(minimal)"
			  :group :launcher})
  ;;(awful.key [modkey ctrl] :Return (fn [] (awful.spawn "tym --role tmp")) {:description "open a terminal(minimal)" :group :launcher})


  
  (awful.key [modkey shift] :o
			 (fn []
			   (awful.spawn.with_shell "jaro $(xsel -b -o)"))
			 {:description "open clipboard with jaro or xdg-open"
			  :group :launcher})

  (awful.key [modkey shift] :v
			 (fn []
			   (awful.spawn.with_shell "mpv $(xsel -b -o)"))
			 {:description "open clipboard with mpv"
			  :group :launcher})
  
  (awful.key [modkey] :p
			 (fn [] (menubar.show))
			 {:description "show the menubar"
			  :group :launcher})
  ])

(append-global-keybindings
 [(awful.key [modkey] :Left
			 awful.tag.viewprev
			 {:description "view previous"
			  :group :tag})
  (awful.key [modkey] :Right
			 awful.tag.viewnext
			 {:description "view next"
			  :group :tag})
  (awful.key [modkey] :Escape
			 awful.tag.history.restore
			 {:description "go back"
			  :group :tag})
  (awful.key [modkey] :e
			 awful.tag.history.restore
			 {:description "go back"
			  :group :tag})
  ])



(append-global-keybindings
 [(awful.key [modkey] :j
			 (fn []
			   (awful.client.focus.byidx 1))
			 {:description "focus next by index"
			  :group :client})
  (awful.key [modkey] :k
			 (fn []
			   (awful.client.focus.byidx (- 1)))
			 {:description "focus previous by index"
			  :group :client})
  (awful.key [modkey] :Tab
			 (fn []
			   (awful.client.focus.history.previous)
			   (when client.focus
				 (client.focus:raise)))
			 {:description "go back"
			  :group :client})
  (awful.key [modkey ctrl] :j
			 (fn []
			   (awful.screen.focus_relative 1))
			 {:description "focus the next screen"
			  :group :screen})
  (awful.key [modkey ctrl] :k
			 (fn []
			   (awful.screen.focus_relative (- 1)))
			 {:description "focus the previous screen"
			  :group :screen})
  (awful.key [modkey ctrl] :n
			 (fn []
			   (let [c (awful.client.restore)]
				 (when c
				   (c:activate {:context :key.unminimize
								:raise true}))))
			 {:description "restore minimized"
			  :group :client})])

(append-global-keybindings
 [(awful.key [modkey shift] :j
			 (fn [] (awful.client.swap.byidx 1))
			 {:description "swap with next client by index"
			  :group :client})
  (awful.key [modkey shift] :k
			 (fn []
			   (awful.client.swap.byidx (- 1)))
			 {:description "swap with previous client by index"
			  :group :client})
  (awful.key [modkey] :u
			 awful.client.urgent.jumpto
			 {:description "jump to urgent client"
			  :group :client})
  (awful.key [modkey] :l
			 (fn [] (awful.tag.incmwfact 0.05))
			 {:description "increase master width factor"
			  :group :layout})
  (awful.key [modkey] :h
			 (fn [] (awful.tag.incmwfact (- 0.05)))
			 {:description "decrease master width factor"
			  :group :layout})
  (awful.key [modkey shift] :h
			 (fn [] (awful.tag.incnmaster 1 nil true))
			 {:description "increase the number of master clients"
			  :group :layout})
  (awful.key [modkey shift] :l
			 (fn [] (awful.tag.incnmaster (- 1) nil true))
			 {:description "decrease the number of master clients"
			  :group :layout})
  (awful.key [modkey ctrl] :h
			 (fn [] (awful.tag.incncol 1 nil true))
			 {:description "increase the number of columns"
			  :group :layout})
  (awful.key [modkey ctrl] :l
			 (fn [] (awful.tag.incncol (- 1) nil true))
			 {:description "decrease the number of columns"
			  :group :layout})
  (awful.key [modkey] :space
			 (fn [] (awful.layout.inc 1))
			 {:description "select next"
			  :group :layout})
  (awful.key [modkey shift] :space
			 (fn [] (awful.layout.inc (- 1)))
			 {:description "select previous"
			  :group :layout})])

(append-global-keybindings
 [(awful.key {:description "only view tag"
			  :group :tag
			  :keygroup :numrow
			  :modifiers [modkey]
			  :on_press
			  (fn [index]
				(local screen (awful.screen.focused))
				(local tag (. screen.tags index))
				(when tag (tag:view_only)))})
  (awful.key {:description "toggle tag"
			  :group :tag
			  :keygroup :numrow
			  :modifiers [modkey
						  ctrl]
			  :on_press (fn [index]
						  (local screen
								 (awful.screen.focused))
						  (local tag
								 (. screen.tags
									index))
						  (when tag
							(awful.tag.viewtoggle tag)))})
  (awful.key {:description "move focused client to tag"
			  :group :tag
			  :keygroup :numrow
			  :modifiers [modkey
						  shift]
			  :on_press (fn [index]
						  (when client.focus
							(local tag
								   (. client.focus.screen.tags
									  index))
							(when tag
							  (client.focus:move_to_tag tag))))})
  (awful.key {:description "toggle focused client on tag"
			  :group :tag
			  :keygroup :numrow
			  :modifiers [modkey
						  ctrl
						  shift]
			  :on_press (fn [index]
						  (when client.focus
							(local tag
								   (. client.focus.screen.tags
									  index))
							(when tag
							  (client.focus:toggle_tag tag))))})
  (awful.key {:description "select layout directly"
			  :group :layout
			  :keygroup :numpad
			  :modifiers [modkey]
			  :on_press (fn [index]
						  (local t
								 (. (awful.screen.focused)
									:selected_tag))
						  (when t
							(set t.layout
								 (or (. t.layouts
										index)
									 t.layout))))})])
;;;; client
(local mouse awful.button.names)

;;;;; client mousebindings
(client.connect_signal
 "request::default_mousebindings"
 (fn []
   (awful.mouse.append_client_mousebindings
	[(awful.button {:modifiers ["Any"]
					:button mouse.LEFT
					:on_press (fn [c] (c:activate {:context :mouse_click}))
					:description "activate window on click"
					:group "client"})
	 (awful.button
	  {:modifiers [modkey]
	   :button mouse.LEFT
	   :on_press (fn [c]
				   (c:activate
					{:action "mouse_move"
					 :context "mouse_click"}))})
	 (awful.button
	  {:modifiers [modkey]
	  :button button.RIGHT
	   :on_press (fn [c]
				   (c:activate
					{:action "mouse_resize"
					 :context "mouse_click"}))})])))


;;;;; client keybindings
(client.connect_signal "request::default_keybindings"
 (fn []
   (append-client-keybindings
	[(awful.key [modkey] :f
				(fn [c]
				  (set c.fullscreen
					   (not c.fullscreen))
				  (c:raise))
				{:description "toggle fullscreen"
				 :group :client})
	 (awful.key [modkey shift] :c
				(fn [c]
				  (c:kill))
				{:description :close
				 :group :client})
	 (awful.key [modkey
				 ctrl]
				:space
				awful.client.floating.toggle
				{:description "toggle floating"
				 :group :client})
	 (awful.key [modkey ctrl] :Return
				(fn [c]
				  (c:swap (awful.client.getmaster)))
				{:description "move to master"
				 :group :client})
	 (awful.key [modkey] :o
				(fn [c]
				  (c:move_to_screen))
				{:description "move to screen"
				 :group :client})
	 (awful.key [modkey] :t
				(fn [c]
				  (set c.ontop
					   (not c.ontop)))
				{:description "toggle keep on top"
				 :group :client})
	 (awful.key [modkey]
				:n
				(fn [c]
				  (set c.minimized
					   true))
				{:description :minimize
				 :group :client})
	 (awful.key [modkey] :m
				(fn [c] (set c.maximized (not c.maximized)) (c:raise))
				{:description "(un)maximize"
				 :group :client})
	 (awful.key [modkey ctrl] :m
				(fn [c] (set c.maximized_vertical (not c.maximized_vertical)) (c:raise))
				{:description "(un)maximize vertically"
				 :group :client})
	 (awful.key [modkey shift]	:m
				(fn [c]
				  (set c.maximized_horizontal
					   (not c.maximized_horizontal))
				  (c:raise))
				{:description "(un)maximize horizontally"
				 :group :client})
	 ])))






(fn mins->secs [mins]
  (* 60 mins))

{}
