#!/usr/bin/env fennel
;; -*- mode: rainbow; mode: fennel -*-
(local theme-assets (require :beautiful.theme_assets))
(local xresources (require :beautiful.xresources))
(local gears (require :gears))
(local dpi xresources.apply_dpi)
(local gfs (require :gears.filesystem))
(local themes-path (gfs.get_themes_dir))

(local tbl (require "tableutils"))
(local gears (require "gears"))
(local theme {})

(var general
	   {
		:background "#242837"
		:foreground "#8099ee"
		:color0 "#000000"
		:color1 "#d36265"
		:color2 "#aece91"
		:color3 "#e7e18c"
		:color4 "#7a7ab0"
		:color5 "#963c59"
		:color6 "#418179"
		:color7 "#bebebe"
		:color8 "#666666"
		:color9 "#ef8171"
		:color10 "#e5f779"
		:color11 "#fff796"
		:color12 "#4186be"
		:color13 "#ef9ebe"
		:color14 "#71bebe"
		:color15 "#ffffff"
		:color16 "#232635"
		})
;;(set general (xresources.get_current_theme))
;;; :

(local bg
	   {
		:normal    general.background
		:focus     general.foreground ;;"#82aaff"
		:urgent    general.color5
		:minimize  general.color7 ;;"#535d6c"
		:systray   general.color16 ;;"#242837"
		})

(local fg
	   {
		:normal    general.foreground
		:focus     general.color15
		:urgent    general.color11
		:minimize  general.color15
		:systray   general.color14
		})

(fn icon [iconName]
  (.. "/home/erik/.config/icons/" iconName ".svg"))
(fn titlebarIcon [iconName]
  (..  themes-path "default/titlebar/" iconName ".png")
  )

(fn titlebarLayouts [iconName]
  (..  themes-path "default/layouts/" iconName ".png")
  )

;;; general
(set theme.general_background  general.background)
(set theme.general_foreground  general.foreground)
(set theme.general_color0      general.color0    )
(set theme.general_color1      general.color1    )
(set theme.general_color2      general.color2    )
(set theme.general_color3      general.color3    )
(set theme.general_color4      general.color4    )
(set theme.general_color5      general.color5    )
(set theme.general_color6      general.color6    )
(set theme.general_color7      general.color7    )
(set theme.general_color8      general.color8    )
(set theme.general_color9      general.color9    )
(set theme.general_color10     general.color10   )
(set theme.general_color11     general.color11   )
(set theme.general_color12     general.color12   )
(set theme.general_color13     general.color13   )
(set theme.general_color14     general.color14   )
(set theme.general_color15     general.color15   )
;;; layout
(set theme.layout_fairh      (titlebarLayouts :fairhw))
(set theme.layout_fairv      (titlebarLayouts :fairvw))
(set theme.layout_floating   (titlebarLayouts :floatingw))
(set theme.layout_magnifier  (titlebarLayouts :magnifierw))
(set theme.layout_max        (titlebarLayouts :maxw))
(set theme.layout_fullscreen (titlebarLayouts :fullscreenw))
(set theme.layout_tilebottom (titlebarLayouts :tilebottomw))
(set theme.layout_tileleft   (titlebarLayouts :tileleftw))
(set theme.layout_tile       (titlebarLayouts :tilew))
(set theme.layout_tiletop    (titlebarLayouts :tiletopw))
(set theme.layout_spiral     (titlebarLayouts :spiralw))
(set theme.layout_dwindle    (titlebarLayouts :dwindlew))
(set theme.layout_cornernw   (titlebarLayouts :cornernww))
(set theme.layout_cornerne   (titlebarLayouts :cornernew))
(set theme.layout_cornersw   (titlebarLayouts :cornersww))
(set theme.layout_cornerse   (titlebarLayouts :cornersew))


;;; bg and fg
(set theme.bg_normal    bg.normal  )
(set theme.bg_focus     bg.focus   )
(set theme.bg_urgent    bg.urgent  )
(set theme.bg_minimize  bg.minimize)
(set theme.bg_systray   bg.systray )
(set theme.fg_normal    fg.normal  )
(set theme.fg_focus     fg.focus   )
(set theme.fg_urgent    fg.urgent  )
(set theme.fg_minimize  fg.minimize)
;;; notification


(set theme.notification-bg general.background)
(set theme.notification-fg general.foreground)
(set theme.notification_opacity 0.9)

(set theme.notification_shape gears.shape.rounded_rect)

;;; taglist
(set theme.taglist_bg_empty general.background)
(set theme.taglist_bg_focus general.color7)
(set theme.taglist_bg_occupied general.color14)
(set theme.taglist_fg_empty general.foreground)
(set theme.taglist_fg_focus general.color6)
(set theme.taglist_fg_occupied general.color12)
;;; titlebar
(set theme.titlebar_bg general.background)
(set theme.titlebar_fg general.foreground)
(set theme.titlebar_fg_focus general.color6)
(set theme.titlebar_bg_focus general.background)
;;; misc
(set theme.border_width 3)
(set theme.useless_gap 4);;(dpi 1));;3))

(set theme.menu_height (dpi 16))
;;       (set theme.menu_width (dpi 256))

;;; tasklist
(set theme.tasklist_fg_normal   fg.normal  );The default foreground (text) color.
(set theme.tasklist_bg_normal   bg.normal  );The default background color.
(set theme.tasklist_fg_focus    fg.focus   );The focused client foreground (text) color.
(set theme.tasklist_bg_focus    bg.focus   );The focused client background color.
(set theme.tasklist_fg_urgent   fg.urgent  );The urgent clients foreground (text) color.
(set theme.tasklist_bg_urgent   bg.urgent  );The urgent clients background color.
(set theme.tasklist_fg_minimize fg.minimize);The minimized clients foreground (text) color.
(set theme.tasklist_bg_minimize bg.minimize);The minimized clients background color.




;;       (set theme.tasklist_bg_image_normal     );The elements default background image.
;;       (set theme.tasklist_bg_image_focus      );The focused client background image.
;;       (set theme.tasklist_bg_image_urgent     );The urgent clients background image.
;;       (set theme.tasklist_bg_image_minimize   );The minimized clients background image.




;;; :

(set theme.taglist_font "UbuntuMono Nerd Font Mono 9")
(set theme.font "UbuntuMono Nerd Font Mono 10")

(set theme.border_focus general.color6)
(set theme.border_normal theme.bg_normal)
(set theme.menu_submenu_icon (.. themes-path :default/submenu.png))
(set theme.mouse_finder_color general.color14)




(set theme.titlebar_close_button_normal
	 (titlebarIcon "close_normal"))
(set theme.titlebar_close_button_focus
	 (titlebarIcon "close_focus"))
(set theme.titlebar_minimize_button_normal
	 (titlebarIcon "minimize_normal"))
(set theme.titlebar_minimize_button_focus
	 (titlebarIcon "minimize_focus"))
(set theme.titlebar_ontop_button_normal_inactive
	 (titlebarIcon "ontop_normal_inactive"))
(set theme.titlebar_ontop_button_focus_inactive
	 (titlebarIcon "ontop_focus_inactive"))
(set theme.titlebar_ontop_button_normal_active
	 (titlebarIcon "ontop_normal_active"))
(set theme.titlebar_ontop_button_focus_active
	 (titlebarIcon "ontop_focus_active"))
(set theme.titlebar_sticky_button_normal_inactive
	 (titlebarIcon "sticky_normal_inactive"))
(set theme.titlebar_sticky_button_focus_inactive
	 (titlebarIcon "sticky_focus_inactive"))
(set theme.titlebar_sticky_button_normal_active
	 (titlebarIcon "sticky_normal_active"))
(set theme.titlebar_sticky_button_focus_active
	 (titlebarIcon "sticky_focus_active"))
(set theme.titlebar_floating_button_normal_inactive
	 (titlebarIcon "floating_normal_inactive"))
(set theme.titlebar_floating_button_focus_inactive
	 (titlebarIcon "floating_focus_inactive"))
(set theme.titlebar_floating_button_normal_active
	 (titlebarIcon "floating_normal_active"))
(set theme.titlebar_floating_button_focus_active
	 (titlebarIcon "floating_focus_active"))
(set theme.titlebar_maximized_button_normal_inactive
	 (titlebarIcon "maximized_normal_inactive"))
(set theme.titlebar_maximized_button_focus_inactive
	 (titlebarIcon "maximized_focus_inactive"))
(set theme.titlebar_maximized_button_normal_active
	 (titlebarIcon "maximized_normal_active"))
(set theme.titlebar_maximized_button_focus_active
	 (titlebarIcon "maximized_focus_active"))




(set theme.awesome_icon
	 (theme-assets.awesome_icon
	  theme.menu_height
	  bg.focus
	  fg.focus)
	 )


;;(set theme.icon_theme nil)



theme
