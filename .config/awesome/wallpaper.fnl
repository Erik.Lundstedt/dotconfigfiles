#!/usr/bin/env fennel
(local home (os.getenv "HOME"))
(local wallsDir (.. home "/Desktop/wallpapers/"))
(local settings (require :settings))
(local wibox (require :wibox))
(local awful (require :awful))
(local naughty (require :naughty))

(local wallpaper {})

(fn wallpaper.set [{:screen s :image img}]
  (local wallpaperwidget
		 {:valign "center"
		  :halign "center"
		  :tiled false
		  :widget wibox.container.tile})
  (tset wallpaperwidget 1
		{:image img
		 :upscale true
		 :downscale true
		 :widget wibox.widget.imagebox})
	(awful.wallpaper
	 {:screen s
	  :widget wallpaperwidget})
  )


(fn wallpaper.auto [scr]
  (when settings.wallpapers
	(var wallpaper-image (. settings.wallpapers scr.index))
	(when (= (type wallpaper-image) :function)
	  (set wallpaper-image (wallpaper-image scr)))
	;;(naughty.notification {:timeout 200  :title (.. "screen:" scr.index "\n wallpaper: " wallpaper-image) })
	(wallpaper.set {:screen scr :image wallpaper-image})))






(screen.connect_signal "request::wallpaper" wallpaper.auto)
;;(screen.connect_signal "property::geometry" set-wallpaper)
;;(screen.connect_signal "desktop_decoration" set-wallpaper)




wallpaper
