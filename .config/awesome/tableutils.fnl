(local tbl {})


(fn tbl.combine [tables]
  (local tc {})
  (each [i tabl (ipairs tables)]
	(each [k v (pairs tabl)]
	  (tset tc k v)
	  )
	)
  tc
  )


(fn tbl.dump [o]
  (if
   (= (type o) :table)
   (do
	 (var s "{ ")
	 (each [k v (pairs o)]
	   (when (not= (type k) :number)
		 (set-forcibly! k (.. "\"" k "\"")))
	   (set s (.. s "[" k "] = " (tbl.dump v) ",\n")))
	 (.. s "}\n "))
   (tostring o)))


tbl
