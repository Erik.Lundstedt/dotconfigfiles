#!/usr/bin/env fennel
(local awful (require :awful))
(local utils
	   {:spawn
		{:simple awful.spawn
		 :single awful.spawn.single_instance
		 :raise awful.spawn.raise_or_spawn
		 :async awful.spawn.easy_async
		 :shell awful.spawn.with_shell
		 :once awful.spawn.once
		 }
		:tagOrder
		{:home        "home"
		 :games       "games"
		 :term        "terminal"
		 :fm          "filemanager"
		 :www         "browser"
		 :media       "media"
		 :editor      "editor"
		 :prog        "programming"
		 :ide         "ide"
		 }
		})





utils
