(define (kittyArgs class)
  (string-concatenate (list "kitty "  "--class " class " "))
  ;;"-1 " "--wait-for-single-instance-window-close "
  )


(define (tymArgs name)
  ;;(string-concatenate (list "-- " name " tym "  "--role " name " "))
  (string-concatenate (list "-- " " tym "  "--role " name " "))
  ;;"-1 " "--wait-for-single-instance-window-close "
  )

(define (altTymArgs name)
  ;;(string-concatenate (list "-- " name " tym "  "--role " name " "))
  (string-concatenate (list " tym "  "--role " name " "))
  ;;"-1 " "--wait-for-single-instance-window-close "
  )


(define (weztermArgs class )
  (string-concatenate (list "wezterm start "
							"--domain=" class " "
							"--class=" class " "
							"--attach"
							)))

							;;"--always-new-process "



(define (terminal class )
  ;;(kittyArgs class)
  ;;(if (equal? (gethostname) "archLaptop")
  (weztermArgs class )
  ;;(kittyArgs class)
  ;;)
  ;;(kittyArgs class)
  ;;(tymArgs class )
  ;;(string-concatenate
  ;;(list
;;	(tymArgs class )
;;	"mprocs --server 127.0.0.1:7342 zsh "
;;	)
  ;; )
  )


(define (tdropArgs number)
  (string-concatenate
   (list "tdrop "
		 "-mta "
		 "--height=60% "
		 "--width=90% "
		 "--x-offset=5% "
		 "--y-offset=23 "
		 "--number=" number " -- "))
  )



(define (tdropArgsCurrent number)
  (string-concatenate
   (list "tdrop "
		 "-mta "
		 "--height=60% "
		 "--width=90% "
		 "--x-offset=5% "
		 "--y-offset=23 "
		 "--number=" number " "
		 "current"))
  )


(define (tdropArgsClearCurrent number)
  (string-concatenate
   (list "tdrop "
		 "-mta "
		 "--height=60% "
		 "--width=90% "
		 "--x-offset=5% "
		 "--y-offset=23 "
		 "--number=" number
		 "--clear"
		 "current")))



;;(xbindkey '("mod2" XF86Eject) ())
(xbindkey '("mod4" "x" ) "~/run.fnl")




;; (xbindkey '("mod2" "mod4" "Menu")
;; 		  (string-append
;; 		   ;;(tdropArgs "200")
;; 		   (altTymArgs "tmp")
;; 		   "-e \"hanime get $(xclip -o) "
;; 		   "-o /media/erik/hentai/media/video/hentai "
;; 		   "-y\""
;; 		   )
;; 		  )

#|
(xbindkey '(Shift "mod2" "Menu" ) "tmux send-keys \"gradle-or-gradlew runClient
\"")
|#




;;(xbindkey '("mod2" XF86LaunchA) "")



;;(xbindkey '("mod2" XF86LaunchA) "awmtt restart")
;;(xbindkey '("mod2" XF86LaunchB) "")

(xbindkey '("Scroll_Lock")
		  (string-append
		   (tdropArgs "42")
		   "emacsclient -c -F '((name . \"scratch\") (title . \"scratch\"))'"))


;;(xbindkey '("mod2" "Menu")  (string-append (tdropArgs "73") (terminal "scratch")))
(xbindkey
 '("mod2" "Menu")
 (string-append
  (tdropArgs "73")
  (terminal "scratch" )
  ))
;;;(xbindkey '("mod2" XF86Launch7) "tbt.fnl")

;;(xbindkey '("mod2" "Super_L" "Menu") (tdropArgsCurrent "42"))
;;(xbindkey '("mod2" "Super_L" "Shift" "Menu") (tdropArgsClearCurrent "42"))

;;(xbindkey '("mod2" "Shift" "Menu") "tdrop -n 42 --clear current;tdrop -n 8 --clear current")



;;(xbindkey '("mod2" "Print")  (string-append (tdropArgs "73") (terminal "scratch")))
;;(xbindkey '("mod2" "Menu")  (string-append "echo " "\"" (tdropArgs "73") (terminal "scratch") "\"|xclip"))

;;(xbindkey '("mod2" XF86Eject) (tdropArgsCurrent "8"));;dont use



;; ;;(xbindkey '("mod2" XF86Explorer)  (string-append (tdropArgs "1") (terminal "scratch")))
;; (xbindkey '("mod2" XF86Explorer)
;;		  (string-append
;;		   ;;(tdropArgs "2")
;;		   (terminal "float") "xplr" " ")
;;		  )




;; (xbindkey '("mod2" XF86Launch1) (string-append (tdropArgs "2") (terminal "xplr") "xplr"))
;; (xbindkey '("mod2" XF86Launch2) wallset)
;; ;;(xbindkey '("mod2" XF86Launch3) "/home/erik/scripts/emacsTile")

;; ;;(xbindkey '("mod2" XF86Launch4) "pkill xbindkeys;abduco -A xbindkeys xbindkeys -fg /home/erik/.config/xbindkeys/xbindkeysrc.scm")

;; (xbindkey '("mod2" Pause) "killall -HUP xbindkeys;notify-send \"restarted xbindkeys\"")
;; (xbindkey '("mod2" ) "killall -HUP xbindkeys")



;; (xbindkey '("mod2" XF86Launch7) wallset)
;; (xbindkey '("mod2" XF86WWW) "luakit")

;; (xbindkey '("mod2" XF86Phone) (string-append (tdropArgs "200") "--class=scratchmacs " "emacsclient -c --frame-parameters=\"(minibuffer nil name \"scratchmacs\")\""))

;; (xbindkey '("mod2" XF86ScreenSaver) "arcolinux-logout")

;; (xbindkey '("mod2"  XF86AudioRaiseVolume)        "playerctl  5%+")
;; (xbindkey '("mod2"  XF86AudioLowerVolume)        "playerctl  5%-")
;; (xbindkey '("mod2" Shift  XF86AudioRaiseVolume)  "playerctl  10%+")
;; (xbindkey '("mod2" Shift  XF86AudioLowerVolume)  "playerctl  10%-")


;; (xbindkey '("mod2" XF86AudioPrev) "playerctl previous")
;; (xbindkey '("mod2" XF86AudioNext) "playerctl next")

;; (xbindkey '("mod2" XF86AudioPause) "playerctl play-pause")
;; (xbindkey '("mod2" XF86MyComputer) "/home/erik/scripts/tmuxSend-dmenu")
;; (xbindkey '("mod2" XF86Audio) "luakit")
;; (xbindkey '("mod2" XF86Audio) "luakit")
;; (xbindkey '("mod2" XF86Audio) "luakit")
;; (xbindkey '("mod2" XF86Audio) "luakit")
